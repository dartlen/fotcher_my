package com.balinasoft.fotcher

import android.annotation.SuppressLint
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.balinasoft.fotcher.di.ComponentManager

class FotcherApp : MultiDexApplication() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        @JvmStatic
        lateinit var componentsManager: ComponentManager
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        MultiDex.install(this)
        initComponentsTree()
        initAppComponent()
    }

    fun initAppComponent() = componentsManager.getAppComponent()
    fun initComponentsTree(): ComponentManager {
        componentsManager = ComponentManager(this)
        return componentsManager
    }
}