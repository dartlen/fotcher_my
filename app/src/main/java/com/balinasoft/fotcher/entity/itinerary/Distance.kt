package com.balinasoft.fotcher.entity.itinerary

import com.google.gson.annotations.SerializedName

data class Distance(
        @SerializedName("text") val text: String,
        @SerializedName("value") val value: Int
)