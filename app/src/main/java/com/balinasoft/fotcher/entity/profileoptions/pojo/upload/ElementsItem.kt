package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class ElementsItem(
        val name: String? = null,
        val caption: String? = null,
        val type: String? = null,
        val value: Any? = null
)
