package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class ParamsItem(
        val name: String? = null,
        val caption: String? = null,
        val type: String? = null,
        val value: Any? = null,
        val elements: List<ElementsItem?>? = null
)
