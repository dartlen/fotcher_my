package com.balinasoft.fotcher.entity.filterphotostudios

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterPhotostudiosEntity(
        val params: MutableList<ParamsEntity?>? = null
) : Parcelable