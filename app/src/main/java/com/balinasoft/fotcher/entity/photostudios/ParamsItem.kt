package com.balinasoft.fotcher.entity.photostudios

data class ParamsItem(
        val name: String? = null,
        val value: Any? = null
)
