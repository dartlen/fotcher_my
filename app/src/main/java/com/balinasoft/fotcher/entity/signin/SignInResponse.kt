package com.balinasoft.fotcher.entity.signin

data class SignInResponse(
        val needConfirm: Boolean? = null,
        val token: String? = null
)
