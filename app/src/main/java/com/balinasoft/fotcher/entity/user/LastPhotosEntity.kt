package com.balinasoft.fotcher.entity.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LastPhotosEntity(
        val small: String? = null,
        val big: String? = null,
        val id: Int? = null
) : Parcelable