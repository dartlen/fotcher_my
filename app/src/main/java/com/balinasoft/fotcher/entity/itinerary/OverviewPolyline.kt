package com.balinasoft.fotcher.entity.itinerary

import com.google.gson.annotations.SerializedName

data class OverviewPolyline(
        @SerializedName("points") val points: String
)