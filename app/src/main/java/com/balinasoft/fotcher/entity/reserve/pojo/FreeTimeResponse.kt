package com.balinasoft.fotcher.entity.reserve.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class FreeTimeResponse(

        @field:SerializedName("days")
        val days: List<DaysItem?>? = null
)