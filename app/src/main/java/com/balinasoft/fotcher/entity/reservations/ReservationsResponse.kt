package com.balinasoft.fotcher.entity.reservations

data class ReservationsResponse(
        val totalPages: Int? = null,
        val pageSize: Int? = null,
        val page: Int? = null,
        val content: List<ContentItem?>? = null,
        val totalElements: Int? = null
)
