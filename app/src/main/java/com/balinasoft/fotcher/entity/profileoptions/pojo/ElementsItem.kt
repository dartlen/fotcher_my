package com.balinasoft.fotcher.entity.profileoptions.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class ElementsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("options")
        val options: List<OptionsItem?>? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("value")
        var value: @RawValue Any? = null,

        @field:SerializedName("required")
        val required: Boolean? = null,

        @field:SerializedName("unitInt")
        val unitInt: String? = null,

        @field:SerializedName("maxInt")
        val maxInt: Int? = null,

        @field:SerializedName("minInt")
        val minInt: Int? = null,

        @field:SerializedName("minDate")
        val minDate: String? = null,

        @field:SerializedName("maxDate")
        val maxDate: String? = null,

        var border: MutableList<Int>? = null
) : Parcelable