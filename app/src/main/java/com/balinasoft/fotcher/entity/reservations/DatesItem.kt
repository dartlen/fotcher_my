package com.balinasoft.fotcher.entity.reservations

data class DatesItem(
        val date: String? = null,
        val hour: Int? = null,
        val price: Int? = null
)
