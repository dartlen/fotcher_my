package com.balinasoft.fotcher.entity.gallery.pojoimage

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class UploadGalleryResponse(

        @field:SerializedName("small")
        val small: String? = null,

        @field:SerializedName("big")
        val big: String? = null,

        @field:SerializedName("id")
        val id: Int? = null
)