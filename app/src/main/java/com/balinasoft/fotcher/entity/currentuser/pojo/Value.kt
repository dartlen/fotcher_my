package com.balinasoft.fotcher.entity.currentuser.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class Value(
        @field:SerializedName("name")
        var name: String? = null,

        @field:SerializedName("caption")
        var caption: String? = null
) : Parcelable