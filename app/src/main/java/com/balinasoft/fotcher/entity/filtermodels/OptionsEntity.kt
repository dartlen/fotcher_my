package com.balinasoft.fotcher.entity.filtermodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OptionsEntity(
        val name: String? = null,
        val caption: String? = null,
        var check: Boolean? = false
) : Parcelable