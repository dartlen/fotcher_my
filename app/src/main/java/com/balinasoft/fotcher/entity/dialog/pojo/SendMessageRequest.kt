package com.balinasoft.fotcher.entity.dialog.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class SendMessageRequest(

        @field:SerializedName("toId")
        val toId: Int? = null,

        @field:SerializedName("attachmentIds")
        val attachmentIds: List<Int?>? = null,

        @field:SerializedName("text")
        val text: String? = null
)