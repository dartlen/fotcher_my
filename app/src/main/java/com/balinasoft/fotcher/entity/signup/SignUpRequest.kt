package com.balinasoft.fotcher.entity.signup

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class SignUpRequest(

        @field:SerializedName("password")
        val password: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("email")
        val email: String? = null
)