package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class InfoColumnsItem(
        val unit: String? = null,
        val caption: String? = null,
        val value: String? = null
)
