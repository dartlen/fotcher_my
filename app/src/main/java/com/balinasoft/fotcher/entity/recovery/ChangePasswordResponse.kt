package com.balinasoft.fotcher.entity.recovery

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ChangePasswordResponse(

        @field:SerializedName("token")
        val token: String? = null
)