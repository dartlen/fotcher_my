package com.balinasoft.fotcher.entity.models.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ModelsRequest(

        @field:SerializedName("q")
        val Q: String? = null,

        @field:SerializedName("page")
        val page: Int? = null,

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null
)