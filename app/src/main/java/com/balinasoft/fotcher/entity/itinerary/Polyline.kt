package com.balinasoft.fotcher.entity.itinerary

import com.google.gson.annotations.SerializedName

data class Polyline(
        @SerializedName("points") val points: String
)