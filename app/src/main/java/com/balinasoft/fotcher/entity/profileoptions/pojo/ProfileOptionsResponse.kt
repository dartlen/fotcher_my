package com.balinasoft.fotcher.entity.profileoptions.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class ProfileOptionsResponse(

        @field:SerializedName("params")
        val params: MutableList<ParamsItem>? = null
) : Parcelable