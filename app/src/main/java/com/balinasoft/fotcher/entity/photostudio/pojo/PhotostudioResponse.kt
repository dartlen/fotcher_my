package com.balinasoft.fotcher.entity.photostudio.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class PhotostudioResponse(

        @field:SerializedName("priceMax")
        val priceMax: Int? = null,

        @field:SerializedName("address")
        val address: String? = null,

        @field:SerializedName("lng")
        val lng: Double? = null,

        @field:SerializedName("smallAvatar")
        val smallAvatar: String? = null,

        @field:SerializedName("rating")
        val rating: Any? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null,

        @field:SerializedName("priceMin")
        val priceMin: Int? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("infoColumns")
        val infoColumns: List<InfoColumnsItem?>? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("favorite")
        val favorite: Boolean? = null,

        @field:SerializedName("bigAvatar")
        val bigAvatar: String? = null,

        @field:SerializedName("lat")
        val lat: Double? = null
)