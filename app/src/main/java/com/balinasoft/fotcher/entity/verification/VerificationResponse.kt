package com.balinasoft.fotcher.entity.verification

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class VerificationResponse(

        @field:SerializedName("token")
        val token: String? = null
)