package com.balinasoft.fotcher.entity.currentuser.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ParamsItem(

        @field:SerializedName("elements")
        val elements: List<Any?>? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("value")
        val value: Any? = null
)