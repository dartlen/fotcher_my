package com.balinasoft.fotcher.entity.photostudio.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ValueItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null
)