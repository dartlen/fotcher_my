package com.balinasoft.fotcher.entity.reserve.pojo.request

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ReserveResponse(

        @field:SerializedName("terminalKey")
        val terminalKey: String? = null,

        @field:SerializedName("subTitle")
        val subTitle: String? = null,

        @field:SerializedName("orderId")
        val orderId: String? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("terminalPassword")
        val terminalPassword: String? = null,

        @field:SerializedName("publicKey")
        val publicKey: String? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("userId")
        val userId: String? = null
)