package com.balinasoft.fotcher.entity.signin

data class SignInRequest(
        val password: String? = null,
        val email: String? = null
)
