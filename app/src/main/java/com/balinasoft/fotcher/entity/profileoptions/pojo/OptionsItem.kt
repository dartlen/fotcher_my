package com.balinasoft.fotcher.entity.profileoptions.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class OptionsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null,
        var check: Boolean? = false
) : Parcelable