package com.balinasoft.fotcher.entity.reservations

data class ContentItem(
        val photostudio: Photostudio? = null,
        val dates: List<DatesItem?>? = null,
        val id: Int? = null,
        val status: String? = null
)
