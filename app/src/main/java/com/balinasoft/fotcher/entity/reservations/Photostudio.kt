package com.balinasoft.fotcher.entity.reservations

data class Photostudio(
        val priceMin: Int? = null,
        val priceMax: Int? = null,
        val address: String? = null,
        val lng: Double? = null,
        val smallAvatar: Any? = null,
        val name: String? = null,
        val rating: Any? = null,
        val id: Int? = null,
        val favorite: Boolean? = null,
        val bigAvatar: Any? = null,
        val lat: Double? = null
)
