package com.balinasoft.fotcher.entity.profileoptions.pojo.post

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ProfileOptionsResponse(

        @field:SerializedName("leftStatus")
        val leftStatus: String? = null,

        @field:SerializedName("lastPhotos")
        val lastPhotos: List<LastPhotosItem?>? = null,

        @field:SerializedName("roles")
        val roles: List<String?>? = null,

        @field:SerializedName("smallAvatar")
        val smallAvatar: String? = null,

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("rightStatus")
        val rightStatus: String? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("surname")
        val surname: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("infoColumns")
        val infoColumns: List<InfoColumnsItem?>? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("bigAvatar")
        val bigAvatar: String? = null,

        @field:SerializedName("email")
        val email: String? = null
)