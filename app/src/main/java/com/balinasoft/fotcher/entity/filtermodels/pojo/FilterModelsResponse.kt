package com.balinasoft.fotcher.entity.filtermodels.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class FilterModelsResponse(

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null
)