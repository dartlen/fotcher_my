package com.balinasoft.fotcher.entity.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValueEntity(
        val name: String? = null,
        val caption: String? = null
) : Parcelable