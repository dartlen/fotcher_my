package com.balinasoft.fotcher.entity.photostudios

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class ContentItem(

        @field:SerializedName("priceMin")
        val priceMin: Int? = null,

        @field:SerializedName("priceMax")
        val priceMax: Int? = null,

        @field:SerializedName("address")
        val address: String? = null,

        @field:SerializedName("lng")
        val lng: Double? = null,

        @field:SerializedName("smallAvatar")
        val smallAvatar: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("rating")
        val rating: @RawValue Any? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("favorite")
        var favorite: Boolean? = null,

        @field:SerializedName("bigAvatar")
        val bigAvatar: String? = null,

        @field:SerializedName("lat")
        val lat: Double? = null,
        var flag: Boolean = false
) : Parcelable