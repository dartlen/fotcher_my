package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class ValueItem(
        val name: String? = null,
        val caption: String? = null
)
