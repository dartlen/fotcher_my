package com.balinasoft.fotcher.entity.reserve.pojo.response

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class PartsItem(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("hour")
        val hour: Int? = null
)