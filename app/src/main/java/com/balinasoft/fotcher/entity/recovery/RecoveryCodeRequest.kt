package com.balinasoft.fotcher.entity.recovery

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class RecoveryCodeRequest(

        @field:SerializedName("code")
        val code: String? = null,

        @field:SerializedName("email")
        val email: String? = null
)