package com.balinasoft.fotcher.entity.itinerary

import com.google.gson.annotations.SerializedName

data class ItineraryResponse(
        @SerializedName("geocoded_waypoints") val geocodedWaypoints: List<GeocodedWaypoint>,
        @SerializedName("routes") val routes: List<Route>,
        @SerializedName("status") val status: String
)