package com.balinasoft.fotcher.entity.filtermodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ElementsEntity(
        val name: String? = null,
        val options: List<OptionsEntity?>? = null,
        val caption: String? = null,
        val type: String? = null,
        val value: String? = null,
        val required: Boolean? = null,
        val unitInt: String? = null,
        val maxInt: Int? = null,
        val minInt: Int? = null,
        var borders: MutableList<Int?>? = null
) : Parcelable