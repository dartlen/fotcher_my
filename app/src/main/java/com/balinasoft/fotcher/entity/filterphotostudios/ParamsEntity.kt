package com.balinasoft.fotcher.entity.filterphotostudios

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParamsEntity(
        val maxInt: Int? = null,
        val minInt: Int? = null,
        val caption: String? = null,
        val type: String? = null,
        val name: String? = null,
        val options: MutableList<OptionsEntity?>? = null,
        var borders: List<Int?>? = null
) : Parcelable