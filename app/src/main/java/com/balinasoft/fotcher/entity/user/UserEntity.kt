package com.balinasoft.fotcher.entity.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class UserEntity(
        val leftStatus: String? = null,
        val rightStatus: String? = null,
        val lastPhotos: List<LastPhotosEntity?>? = null,
        val phone: String? = null,
        val surname: String? = null,
        val smallAvatar: String? = null,
        val name: String? = null,
        val infoColumns: @RawValue Any? = null,
        val id: Int? = null,
        val type: String? = null,
        val params: List<ParamsEntity?>? = null,
        val bigAvatar: String? = null
) : Parcelable