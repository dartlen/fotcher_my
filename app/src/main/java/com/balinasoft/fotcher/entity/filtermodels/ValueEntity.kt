package com.balinasoft.fotcher.entity.filtermodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValueEntity(
        val any: String? = null
) : Parcelable