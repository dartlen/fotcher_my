package com.balinasoft.fotcher.entity.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Element(val name: String?, val caption: String?, val value: Int?, val type: String?) : Parcelable