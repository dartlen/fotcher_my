package com.balinasoft.fotcher.entity.comments.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class CommentRequest(

        @field:SerializedName("star")
        val star: Float? = null,

        @field:SerializedName("text")
        val text: String? = null
)