package com.balinasoft.fotcher.entity.itinerary

import com.google.gson.annotations.SerializedName

data class Southwest(
        @SerializedName("lat") val lat: Double,
        @SerializedName("lng") val lng: Double
)