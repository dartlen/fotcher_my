package com.balinasoft.fotcher.entity.filterphotographers.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ParamsItem(

        @field:SerializedName("unitInt")
        val unitInt: String? = null,

        @field:SerializedName("minDate")
        val minDate: String? = null,

        @field:SerializedName("maxInt")
        val maxInt: Int? = null,

        @field:SerializedName("minInt")
        val minInt: Int? = null,

        @field:SerializedName("maxDouble")
        val maxDouble: Int? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("minDouble")
        val minDouble: Int? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("required")
        val required: Boolean? = null,

        @field:SerializedName("elements")
        val elements: List<Any?>? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("options")
        val options: List<OptionsItem?>? = null,

        @field:SerializedName("maxDate")
        val maxDate: String? = null,

        @field:SerializedName("value")
        val value: Value? = null,

        @field:SerializedName("maxLength")
        val maxLength: Int? = null
)