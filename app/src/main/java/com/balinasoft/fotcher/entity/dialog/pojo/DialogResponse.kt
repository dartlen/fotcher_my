package com.balinasoft.fotcher.entity.dialog.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DialogResponse(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("readed")
        val readed: Boolean? = null,

        @field:SerializedName("attachments")
        val attachments: List<Any?>? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("text")
        val text: String? = null,

        @field:SerializedName("my")
        val my: Boolean? = null
)