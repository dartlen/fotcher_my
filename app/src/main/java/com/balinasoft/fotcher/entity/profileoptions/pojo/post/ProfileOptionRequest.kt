package com.balinasoft.fotcher.entity.profileoptions.pojo.post

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ProfileOptionRequest(

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null
)