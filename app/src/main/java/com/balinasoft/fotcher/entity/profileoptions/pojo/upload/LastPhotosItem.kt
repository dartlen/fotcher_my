package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class LastPhotosItem(
        val small: String? = null,
        val big: String? = null,
        val id: Int? = null
)
