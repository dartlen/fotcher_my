package com.balinasoft.fotcher.entity.recovery

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class SetNewPasswordResponse(

        @field:SerializedName("token")
        val token: String? = null
)