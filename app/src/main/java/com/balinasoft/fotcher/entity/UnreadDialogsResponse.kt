package com.balinasoft.fotcher.entity

import com.google.gson.annotations.SerializedName

data class UnreadDialogsResponse(
        @SerializedName("unreadCount") val unreadCount: Int
)