package com.balinasoft.fotcher.entity.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class ParamsEntity(
        val name: String? = null,
        val caption: String? = null,
        val type: String? = null,
        val value: MutableList<ValueEntity?>? = null,
        val elements: @RawValue MutableList<Element>? = null
) : Parcelable