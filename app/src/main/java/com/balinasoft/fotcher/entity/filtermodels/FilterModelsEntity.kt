package com.balinasoft.fotcher.entity.filtermodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterModelsEntity(
        var params: MutableList<ParamsEntity>
) : Parcelable