package com.balinasoft.fotcher.entity.reserve.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DaysItem(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("hours")
        val hours: List<HoursItem?>? = null
)