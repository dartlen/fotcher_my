package com.balinasoft.fotcher.entity.profileoptions.pojo.post

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ParamsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("value")
        val value: Any? = null
)