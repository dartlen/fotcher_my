package com.balinasoft.fotcher.entity.signup

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class SignUpResponse(

        @field:SerializedName("needConfirm")
        val needConfirm: Boolean? = null
)