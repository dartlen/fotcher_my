package com.balinasoft.fotcher.entity.filterphotographers

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterParamsEntity(
        val params: MutableList<ParamsEntity?>? = null
) : Parcelable