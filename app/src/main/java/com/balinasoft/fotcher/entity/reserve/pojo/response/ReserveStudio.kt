package com.balinasoft.fotcher.entity.reserve.pojo.response

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ReserveStudio(

        @field:SerializedName("parts")
        val parts: MutableList<PartsItem?>? = null,

        @field:SerializedName("photostudioId")
        val photostudioId: Int? = null
)