package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class UploadResponse(
        val leftStatus: String? = null,
        val lastPhotos: List<LastPhotosItem?>? = null,
        val smallAvatar: String? = null,
        val roles: List<String?>? = null,
        val type: String? = null,
        val params: List<ParamsItem?>? = null,
        val rightStatus: String? = null,
        val phone: Any? = null,
        val surname: String? = null,
        val name: String? = null,
        val infoColumns: List<InfoColumnsItem?>? = null,
        val id: Int? = null,
        val bigAvatar: String? = null,
        val email: String? = null
)
