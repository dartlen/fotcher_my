package com.balinasoft.fotcher.entity.filterphotographers

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParamsEntity(
        val caption: String? = null,
        val name: String? = null,
        val options: MutableList<OptionsEntity?>? = null
) : Parcelable