package com.balinasoft.fotcher.entity.messages

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ContentItem(

        @field:SerializedName("lastMessage")
        val lastMessage: LastMessage? = null,

        @field:SerializedName("unreadCount")
        val unreadCount: Int? = null,

        @field:SerializedName("user")
        val user: User? = null
)