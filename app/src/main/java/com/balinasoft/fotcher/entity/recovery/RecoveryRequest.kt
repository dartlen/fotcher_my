package com.balinasoft.fotcher.entity.recovery

data class RecoveryRequest(
        val email: String? = null
)
