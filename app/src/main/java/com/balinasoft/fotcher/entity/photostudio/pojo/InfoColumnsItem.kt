package com.balinasoft.fotcher.entity.photostudio.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class InfoColumnsItem(

        @field:SerializedName("unit")
        val unit: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("value")
        val value: String? = null
)