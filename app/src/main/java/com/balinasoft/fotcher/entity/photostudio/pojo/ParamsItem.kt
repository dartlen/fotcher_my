package com.balinasoft.fotcher.entity.photostudio.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
@Parcelize
data class ParamsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("value")

        val value: @RawValue Any? = null
) : Parcelable