package com.balinasoft.fotcher.entity.verification

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class VerificationRequest(

        @field:SerializedName("code")
        val code: String? = null,

        @field:SerializedName("email")
        val email: String? = null
)