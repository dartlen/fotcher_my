package com.balinasoft.fotcher.entity.profileoptions.pojo.upload

data class Value(
        val name: String? = null,
        val caption: String? = null
)
