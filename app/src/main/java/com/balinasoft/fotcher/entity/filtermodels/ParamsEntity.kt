package com.balinasoft.fotcher.entity.filtermodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParamsEntity(
        var maxInt: Int? = null,
        var minInt: Int? = null,
        var caption: String? = null,
        var type: String? = null,
        var name: String? = null,
        var options: MutableList<OptionsEntity?>? = null,
        var elements: MutableList<ElementsEntity?>
        //var elements:@Rawvarue Any? = null
) : Parcelable