package com.balinasoft.fotcher.entity.photostudios

data class PhotoStudiosRequest(
        var q: String? = null,
        var page: Int? = null,
        var params: List<ParamsItem?>? = null,
        var favorite: Boolean? = null
)
