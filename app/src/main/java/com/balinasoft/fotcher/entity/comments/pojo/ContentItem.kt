package com.balinasoft.fotcher.entity.comments.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ContentItem(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("star")
        val star: Float? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("text")
        val text: String? = null,

        //@field:SerializedName("user")
        val user: User? = null
)