package com.balinasoft.fotcher.entity.filtermodels.pojo

import com.balinasoft.fotcher.entity.filtermodels.OptionsEntity
import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ElementsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("options")
        val options: MutableList<OptionsEntity?>? = null,

        @field:SerializedName("caption")
        val caption: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("value")
        val value: Any? = null,

        @field:SerializedName("required")
        val required: Boolean? = null,

        @field:SerializedName("unitInt")
        val unitInt: String? = null,

        @field:SerializedName("maxInt")
        val maxInt: Int? = null,

        @field:SerializedName("minInt")
        val minInt: Int? = null
)