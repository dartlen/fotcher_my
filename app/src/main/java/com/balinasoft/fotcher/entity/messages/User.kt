package com.balinasoft.fotcher.entity.messages

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.annotation.Generated

@Parcelize
@Generated("com.robohorse.robopojogenerator")
data class User(

        @field:SerializedName("leftStatus")
        val leftStatus: String? = null,

        @field:SerializedName("surname")
        val surname: String? = null,

        @field:SerializedName("smallAvatar")
        val smallAvatar: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("type")
        val type: String? = null
) : Parcelable