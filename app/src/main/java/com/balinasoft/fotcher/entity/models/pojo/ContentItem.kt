package com.balinasoft.fotcher.entity.models.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class ContentItem(

        @field:SerializedName("leftStatus")
        val leftStatus: String? = null,

        @field:SerializedName("surname")
        val surname: String? = null,

        @field:SerializedName("smallAvatar")
        val smallAvatar: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("type")
        val type: String? = null
)