package com.balinasoft.fotcher.entity.filterphotographers.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class OptionsItem(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("caption")
        val caption: String? = null
)