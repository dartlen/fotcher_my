package com.balinasoft.fotcher.entity.recovery

data class SetNewPasswordRequest(
        val password: String? = null,
        val code: String? = null,
        val email: String? = null
)
