package com.balinasoft.fotcher.entity.reserve.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class HoursItem(

        @field:SerializedName("hour")
        val hour: Int? = null,

        @field:SerializedName("price")
        val price: Int? = null,

        @field:SerializedName("free")
        val free: Boolean? = null,
        var check: Boolean? = false
)