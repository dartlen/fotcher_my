package com.balinasoft.fotcher.entity.filterphotographers.pojo

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class FilterPhotographersResponse(

        @field:SerializedName("params")
        val params: List<ParamsItem?>? = null
)