package com.balinasoft.fotcher

object Constants {
    const val BASE_URL = "http://fotcher-temp.balinasoft.com"
    const val MAPS_URL = "https://maps.googleapis.com"
    const val SIGNIN = "signin"
    const val SIGNUP = "signup"
    const val REC = "recovery"
    const val PHOTOSTUDIOS = "photostudios"
    const val FAVORITES = "favorites"
    const val PHOTOGRAPHERS = "photographers"
    const val MODELS = "models"
    const val MESSAGES = "messages"
    const val RESERVATIONS = "reservations"
    const val PROFILEPHOTOGRAHPER = "profilephotographer"
    const val FILTERPHOTOSTUDIOS = "filterphotostudios"
    const val VERIFICATION = "verification"
    const val FILTERPHOTOGRAPHERS = "filterphotographers"
    const val FILTERPARAMS = "filterparams"
    const val GALLERY = "gallery"
    const val INFOPHOTOSTUDIO = "infophotostudio"
    const val INFOPHOTOGRAPHER = "infophotographer"
    const val PROFILEPHOTOSTUDIO = "profilephotostudio"
    const val COMMENTS = "comments"
    const val RESERVE = "reserve"
    const val FILTERMODELS = "filtermodels"
    const val FILTEROPTIONSMODEL = "filteroptionsmodel"
    const val FILTERPARAMSMODEL = "filterparamsmodels"
    const val PROFILEMODEL = "profilemodel"
    const val PROFILEOPTION = "profileoption"
    const val INFOPROFILEOPTION = "infoprofileoptions"
    const val OPTIONSMODEL = "optionsmodel"
    const val DIALOG = "dialog"
    const val MODELPRAMS = "modelparams"
    const val ITINERARY = "itinerary"

    const val SELECTIMAGE = 1489
    const val PERMISSION = 1488


    val USERS = mapOf("Фотограф" to "PHOTOGRAPHER", "Модель" to "PHOTOMODEL")
}
