package com.balinasoft.fotcher.mvp.dialogs.messages

import android.content.Context
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.entity.messages.ContentItem
import com.balinasoft.fotcher.entity.messages.MessagesResponse
import com.balinasoft.fotcher.mvp.dialogs.messages.adapters.MessagesAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_messages.*
import javax.inject.Inject
import javax.inject.Provider

class MessagesFragment : MvpAppCompatFragment(), MessagesContract.View {

    @InjectPresenter
    lateinit var messagesPresenter: MessagesPresenter

    internal var linearLayoutManager: LinearLayoutManager? = null

    @Inject
    lateinit var presenterProvider: Provider<MessagesPresenter>

    @ProvidePresenter
    fun providePresenter(): MessagesPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 19
    var currentPage = PAGE_START
    var TOTAL_PAGES = 1

    companion object {
        private val PAGE_START = 0
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_messages, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_messages)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_messages.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerMessages.layoutManager = linearLayoutManager
        recyclerMessages.adapter = MessagesAdapter { item: ContentItem -> messagesPresenter.onClickedDialog(item) }

        recyclerMessages.addOnScrollListener(
                object : PaginationScrollListener(linearLayoutManager!!) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {

                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        } else
                            if ((recyclerMessages.adapter as MessagesAdapter).isLoadingAdded)
                                (recyclerMessages.adapter as MessagesAdapter).removeLoadingFooter()
                    }
                })
    }

    override fun onStart() {
        super.onStart()
        if (currentPage == 0)
            messagesPresenter.onStart()
    }

    override fun showMessages(list: MutableList<ContentItem?>, t: MessagesResponse) {
        TOTAL_PAGES = t.totalPages!!
        (recyclerMessages.adapter as MessagesAdapter).list = list
        (recyclerMessages.adapter as MessagesAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
            (recyclerMessages.adapter as MessagesAdapter).addLoadingFooter()
        else
            isLast = true
    }

    override fun removeFooter() {
        (recyclerMessages.adapter as MessagesAdapter).removeLoadingFooter()
    }

    private fun loadNextPage() {
        if (currentPage != TOTAL_PAGES)
            if (!(recyclerMessages.adapter as MessagesAdapter).isLoadingAdded)
                (recyclerMessages.adapter as MessagesAdapter).addLoadingFooter()

        messagesPresenter.loadNextPage(currentPage)
    }

    override fun showNext(t: MessagesResponse) {
        (recyclerMessages.adapter as MessagesAdapter).removeLoadingFooter()
        isLoad = false

        (recyclerMessages.adapter as MessagesAdapter).addAll(t.content)

        if (currentPage != TOTAL_PAGES)
            (recyclerMessages.adapter as MessagesAdapter).addLoadingFooter()
        else
            isLast = true
    }

    override fun hideProgressLoading() {
        messagesProgress.visibility = View.GONE
        recyclerMessages.visibility = View.VISIBLE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

}