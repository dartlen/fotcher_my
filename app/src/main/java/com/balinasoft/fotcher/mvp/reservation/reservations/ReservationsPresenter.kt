package com.balinasoft.fotcher.mvp.reservation.reservations

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.model.interactor.reservations.ReservationsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ReservationsPresenter @Inject constructor(
        private val router: Router,
        private val reservationsInteractor: ReservationsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ReservationsContract.View>() {

    fun getReservations() {
        val dispose = reservationsInteractor.getReservations().subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe({ t ->
                    viewState.showReservations(t.body()!!)
                }, { t: Throwable? ->
                    viewState.showToast(t!!.message)
                })
        unsubscribeOnDestroy(dispose)
    }
}
