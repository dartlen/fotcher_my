package com.balinasoft.fotcher.mvp.activity

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface MainActivityContract {
    interface View : MvpView {
        @StateStrategyType(SkipStrategy::class)
        fun showCurrentUser(url: String, name: String)

        @StateStrategyType(SkipStrategy::class)
        fun showEmail(email: String)

        fun hideDrawerMenu() {}
        fun showUnreadedDialogs(value: Int)
    }
}