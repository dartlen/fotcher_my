package com.balinasoft.fotcher.mvp.comments

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.comments.pojo.CommentSendResponse
import com.balinasoft.fotcher.entity.comments.pojo.ContentItem

@StateStrategyType(SkipStrategy::class)
interface CommentsContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showComments(list: List<ContentItem?>?)
        fun updateView(data: CommentSendResponse)
    }
}