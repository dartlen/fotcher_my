package com.balinasoft.fotcher.mvp.profile.profilephotographer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.ParamsEntity
import com.balinasoft.fotcher.entity.user.UserEntity
import com.balinasoft.fotcher.mvp.profile.profilephotographer.adapters.ProfilePhotographerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile_photographer.*
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class ProfilePhotographerFragment : MvpAppCompatFragment(), ProfilePhotographerContract.View {
    @Suppress("UNCHECKED_CAST")
    companion object {
        fun newInstance(idUser: Any): ProfilePhotographerFragment {
            val fragment = ProfilePhotographerFragment()
            val args = Bundle()
            args.putIntegerArrayList("list", idUser as ArrayList<Int>)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var profilePhotographerPresenter: ProfilePhotographerPresenter

    @Inject
    lateinit var presenterProvider: Provider<ProfilePhotographerPresenter>

    @ProvidePresenter
    fun providePresenter(): ProfilePhotographerPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_profile_photographer, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar_profile_photographer)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_profile_photographer.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerProfilePhotographer.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerProfilePhotographer.addItemDecoration(itemDecor)
        recyclerProfilePhotographer.adapter = ProfilePhotographerAdapter(context,
                { Item: ParamsEntity -> profilePhotographerPresenter.onClickedParams(Item) })

        one.setOnClickListener {
            profilePhotographerPresenter.onClickedGallery(arguments!!.getIntegerArrayList("list"))
        }

        /*if(arguments!!.getIntegerArrayList("list")[1]==1) {

        }else{
            floatingActionButton2.visibility = View.VISIBLE
            floatingActionButton.visibility = View.VISIBLE
        }*/




        floatingActionButton2.setOnClickListener {
            profilePhotographerPresenter.onClickedDialog(arguments!!.getIntegerArrayList("list")[0])
        }

        floatingActionButton.setOnClickListener {
            profilePhotographerPresenter.onClickedCall()
        }

    }

    override fun onStart() {
        super.onStart()
        profilePhotographerPresenter.onStart(arguments!!.getIntegerArrayList("list")[0])
        photographersProgress.visibility = View.VISIBLE
        image.visibility = View.GONE
        recyclerProfilePhotographer.visibility = View.GONE
        name.visibility = View.GONE
        comment.visibility = View.GONE
        exp.visibility = View.GONE
        one.visibility = View.GONE
        two.visibility = View.GONE
        three.visibility = View.GONE
        textView6.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (arguments!!.getIntegerArrayList("list")[1] == 1)
            inflater!!.inflate(R.menu.menu_profile_photographers, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_profile_photgrapher_settings -> {
                profilePhotographerPresenter.onClickedOptions()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SetTextI18n")
    override fun showUser(userEntity: UserEntity) {
        val listforrecycler = arrayListOf<ParamsEntity>()
        for (i in userEntity.params!!) {
            listforrecycler.add(i!!)
        }
        (recyclerProfilePhotographer.adapter as ProfilePhotographerAdapter).list = listforrecycler
        (recyclerProfilePhotographer.adapter as ProfilePhotographerAdapter).notifyDataSetChanged()
        val options = RequestOptions()
        options.centerCrop()
        Glide.with(context!!)
                .load(userEntity.bigAvatar)
                .apply(options)
                .into(image)
        name.text = userEntity.name + " " + userEntity.surname
        comment.text = userEntity.leftStatus
        exp.text = userEntity.rightStatus


        if (userEntity.lastPhotos!!.isNotEmpty()) {
            two.visibility = View.VISIBLE
            val options2 = RequestOptions()
            options2.centerCrop()
            Glide.with(context!!)
                    .load(userEntity.lastPhotos[0]!!.small)
                    .apply(options2)
                    .into(two)
        } else {
            two.visibility = View.INVISIBLE
        }

        if (userEntity.lastPhotos.size >= 2) {
            three.visibility = View.VISIBLE
            val options3 = RequestOptions()
            options3.centerCrop()
            Glide.with(context!!)
                    .load(userEntity.lastPhotos[1]!!.small)
                    .apply(options3)
                    .into(three)
        } else {
            three.visibility = View.INVISIBLE
        }
    }

    var number: String? = null

    override fun call(number: String, name: String?) {
        AlertDialog.Builder(activity!!)
                .setTitle("Совершить звонок $name")
                .setPositiveButton("Позвонить",
                        { _, _ ->
                            this.number = number
                            if (Build.VERSION.SDK_INT >= 23) {
                                val read = activity!!.checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                                val listRequestPermission = ArrayList<String>()

                                if (read != PackageManager.PERMISSION_GRANTED) {
                                    listRequestPermission.add(android.Manifest.permission.CALL_PHONE)
                                }
                                if (!listRequestPermission.isEmpty()) {
                                    val strRequestPermission = listRequestPermission.toTypedArray()
                                    requestPermissions(strRequestPermission, 1400)
                                } else {
                                    val call = Uri.parse("tel:$number")
                                    val surf = Intent(Intent.ACTION_CALL, call)
                                    startActivity(surf)
                                }
                            } else {
                                val call = Uri.parse("tel:$number")
                                val surf = Intent(Intent.ACTION_CALL, call)
                                startActivity(surf)
                            }
                        }
                )
                .setNegativeButton("Отмена",
                        { dialog, _ -> dialog.dismiss() }
                )
                .create().show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1400 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    val call = Uri.parse("tel:$number")
                    val surf = Intent(Intent.ACTION_CALL, call)
                    startActivity(surf)
                } else {

                }
                return
            }

        // Add other 'when' lines to check for other
        // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun hideProgress() {
        photographersProgress.visibility = View.GONE
        image.visibility = View.VISIBLE
        recyclerProfilePhotographer.visibility = View.VISIBLE
        name.visibility = View.VISIBLE
        comment.visibility = View.VISIBLE
        exp.visibility = View.VISIBLE
        one.visibility = View.VISIBLE
        two.visibility = View.VISIBLE
        three.visibility = View.VISIBLE
        textView6.visibility = View.VISIBLE

        if (arguments!!.getIntegerArrayList("list")[1] == 1) {

        } else {
            floatingActionButton2.visibility = View.VISIBLE
            floatingActionButton.visibility = View.VISIBLE
        }
    }

    override fun showProgress() {
        photographersProgress.visibility = View.VISIBLE
    }
}