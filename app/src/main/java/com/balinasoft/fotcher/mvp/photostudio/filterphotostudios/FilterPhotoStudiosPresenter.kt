package com.balinasoft.fotcher.mvp.photostudio.filterphotostudios

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERPARAMS
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filterphotostudios.FilterPhotoStudiosInteractor

import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterPhotoStudiosPresenter @Inject constructor(
        private val router: Router,
        private val filterPhotoStudiosInteractor: FilterPhotoStudiosInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterPhotoStudiosContract.View>() {
    var data: FilterPhotostudiosEntity? = null
    fun onClickedClean() {

        for (i in 0 until data!!.params!!.size) {
            if (i == 1 || i >= 4)
                for (k in 0 until data!!.params!![i]!!.options!!.size) {
                    if (data!!.params!![i]!!.options!![k]!!.check == true)
                        data!!.params!![i]!!.options!![k]!!.check = false
                } else {
                data!!.params!![i]!!.borders = listOf(data!!.params!![i]!!.maxInt, data!!.params!![i]!!.minInt)
            }
        }
        filterPhotoStudiosInteractor.saveFilters(data!!)
        viewState.setDelault(data!!.params!![1]!!.options!!.size, data!!)

    }

    fun onStart() {
        val dispose = filterPhotoStudiosInteractor.onStart().subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    data = t
                    viewState.showFilters(t)

                }, { t ->
                })

        unsubscribeOnDestroy(dispose)
    }

    fun onItemSelectedCity(position: Int) {
        for (i in 0 until data!!.params!![1]!!.options!!.size) {
            if (i == position)
                data!!.params!![1]!!.options!![i]!!.check = true

        }
    }

    fun checkValueSpinner(position: Int): Int {
        for (i in 0 until data!!.params!![position]!!.options!!.size) {
            if (data!!.params!![position]!!.options!![i]!!.check!!) {
                return i
            }
        }
        return data!!.params!![position]!!.options!!.size
    }

    fun saveFilters(min1: String, max1: String, min2: String, max2: String, min3: String, max3: String) {
        data!!.params!![0]!!.borders = listOf(max1.toInt(), min1.toInt())
        data!!.params!![2]!!.borders = listOf(max2.toInt(), min2.toInt())
        data!!.params!![3]!!.borders = listOf(max3.toInt(), min3.toInt())
        filterPhotoStudiosInteractor.saveFilters(data!!)

        filterPhotoStudiosInteractor.setFlagForReloadPhotostudios()
        router.backTo(PHOTOSTUDIOS)
    }

    fun onClickedParams(item: ParamsEntity) {
        saveFilters()
        router.navigateTo(FILTERPARAMS, item)
    }

    private fun saveFilters() {
        filterPhotoStudiosInteractor.saveFilters(data!!)
    }

    fun changedSeedBarValue(min: String, max: String, number: Int) {
        data!!.params!![number]!!.borders = listOf(max.toInt(), min.toInt())
    }

}
