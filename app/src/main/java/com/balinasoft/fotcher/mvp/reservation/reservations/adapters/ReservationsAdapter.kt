package com.balinasoft.fotcher.mvp.reservation.reservations.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.reservations.ContentItem
import kotlinx.android.synthetic.main.item_reservations.view.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ReservationsAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list[position])
    }

    var list: ArrayList<ContentItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_reservations, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bind(item: ContentItem) {
        itemView.pricePhotostudio.text = item.dates!![0]!!.price.toString() + " руб/ч"
        itemView.namePhotostudio.text = item.photostudio!!.name
        itemView.adressPhotostudio.text = item.photostudio.address
        if (item.status == "PAID") {
            itemView.state.text = "Оплачено"
            itemView.state.setTextColor(Color.parseColor("#7ed321"))
        } else {
            itemView.state.text = "В обработке"
            itemView.state.setTextColor(Color.parseColor("#eab52f"))
        }

        val format = SimpleDateFormat("yyyy-MM-dd")
        try {
            val date = format.parse(item.dates[0]!!.date)

            val x = DateFormat.getDateInstance(SimpleDateFormat.LONG, Locale("ru")).format(date)
            itemView.timeReservation.text = item.dates[0]!!.hour.toString() + ":00, " + x.substring(0, x.length - 8)

        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
}