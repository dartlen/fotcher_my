package com.balinasoft.fotcher.mvp.model.modelparams

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.Element
import com.balinasoft.fotcher.mvp.model.modelparams.adapters.ModelParamsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_modelparams.*
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class ModelParamsFragment : MvpAppCompatFragment(), ModelParamsContract.View {
    @Suppress("UNCHECKED_CAST")
    companion object {
        fun newInstance(params: Any): ModelParamsFragment {
            return ModelParamsFragment().apply {
                arguments = Bundle().apply { putParcelableArrayList("options", params as ArrayList<out Parcelable>) }
            }
        }
    }

    @InjectPresenter
    lateinit var modelParamsPresenter: ModelParamsPresenter

    @Inject
    lateinit var presenterProvider: Provider<ModelParamsPresenter>

    @ProvidePresenter
    fun providePresenter(): ModelParamsPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_modelparams, container, false)
        setHasOptionsMenu(true)

        return view
    }
    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_modelparams.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_modelparams)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.title = "Параметры"

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        recyclerModelParams.layoutManager = linearLayoutManager

        recyclerModelParams.adapter = ModelParamsAdapter()

        (recyclerModelParams.adapter as ModelParamsAdapter).list = (arguments!!["options"] as ArrayList<Element>).filterIndexed { index, _ -> index < 5 }.toMutableList()
        (recyclerModelParams.adapter as ModelParamsAdapter).notifyDataSetChanged()

        toolbar_modelparams.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}