package com.balinasoft.fotcher.mvp.auth.recovery

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.model.interactor.recovery.RecoveryInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class RecoveryPresenter @Inject constructor(
        private val router: Router,
        private val recoveryInteractor: RecoveryInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<RecoveryContract.View>() {

    fun onClickedRecovery(email: String) {
        if (email.isEmailValid()) {
            val dispose = recoveryInteractor.recoveryAccount(email).subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe({ _ ->
                        viewState.showInputCodeRecovery()
                    }, { t -> viewState.showToast(t.message) })
            unsubscribeOnDestroy(dispose)
        } else {
            viewState.showToast("Неверный email")
        }
    }

    fun onClickedSendCode(code: String, email: String) {
        val dispose = recoveryInteractor.sendRecoveryCode(code, email)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({
                    viewState.showInputNewPassword()
                }, { t ->
                    viewState.showToast(t.message)
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedChangePassword(email: String, code: String, password: String, password2: String) {
        val dispose = recoveryInteractor.changePassword(email, code, password)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({
                    recoveryInteractor.setToken(it!!.token!!)
                    viewState.hideKeyboard()
                    router.navigateTo(PHOTOSTUDIOS, 1)
                }, { t ->
                    viewState.showToast(t.message)
                })
        unsubscribeOnDestroy(dispose)
    }
}