package com.balinasoft.fotcher.mvp.activity

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.Constants.FAVORITES
import com.balinasoft.fotcher.Constants.MESSAGES
import com.balinasoft.fotcher.Constants.MODELS
import com.balinasoft.fotcher.Constants.PHOTOGRAPHERS
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.Constants.PROFILEMODEL
import com.balinasoft.fotcher.Constants.PROFILEPHOTOGRAHPER
import com.balinasoft.fotcher.Constants.RESERVATIONS
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.model.interactor.activity.MainActivityInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.activity.adapters.NavigationAdapter
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MainActivityPresenter @Inject constructor(
        private val router: Router,
        private val mainActivityInteractor: MainActivityInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<MainActivityContract.View>() {

    fun onClickedItemMenu(menu: NavigationAdapter.Navigation) {
        when (menu.image) {
            R.drawable.ic_photostudios -> {
                router.navigateTo(PHOTOSTUDIOS, 0)
            }
            R.drawable.ic_favorites -> {
                router.navigateTo(FAVORITES, 0)
            }
            R.drawable.ic_photographers -> {
                router.navigateTo(PHOTOGRAPHERS, 0)
            }
            R.drawable.ic_models -> {
                router.navigateTo(MODELS, 0)
            }
            R.drawable.ic_messages -> {
                router.navigateTo(MESSAGES, 0)
            }
            R.drawable.ic_my_reservation -> {
                router.navigateTo(RESERVATIONS, 0)
            }
            R.drawable.ic_profile -> {
                if (mainActivityInteractor.getType() == "PHOTOGRAPHER")
                    router.navigateTo(PROFILEPHOTOGRAHPER, arrayListOf(mainActivityInteractor.getId(), 1))
                else
                    router.navigateTo(PROFILEMODEL, arrayListOf(mainActivityInteractor.getId(), 1))
            }
        }
        viewState.hideDrawerMenu()
    }

    fun onStart() {
        var dispose = mainActivityInteractor.checkToken().subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    if (t!!.bigAvatar != null && t.name != "" && t.surname != "")
                        viewState.showCurrentUser(t.bigAvatar!!, t.name + " " + t.surname)
                    else {
                        viewState.showEmail(t.email!!)
                    }
                    mainActivityInteractor.saveUserId(t.id!!)
                    mainActivityInteractor.saveType(t.type!!)
                    if (t.name != "" && t.surname != "")
                        mainActivityInteractor.setUserName(t.name + " " + t.surname)
                    else
                        mainActivityInteractor.setUserName(t.email!!)

                    if (t.bigAvatar != null)
                        mainActivityInteractor.saveImage(t.bigAvatar)
                    router.navigateTo(PHOTOSTUDIOS, 0)

                }, {
                    router.navigateTo(Constants.SIGNIN, 0)
                })
        unsubscribeOnDestroy(dispose)
    }

    fun getAvatar(): String {
        return mainActivityInteractor.getAvatar()
    }

    fun getUserName(): String {
        return mainActivityInteractor.getUserName()
    }

    fun getUnreadedDialogs() {
        val dispose = mainActivityInteractor.getUnreadedDialogs().subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui()).subscribe({ t ->
            viewState.showUnreadedDialogs(t!!.unreadCount)
        }, { _: Throwable? ->

        })
        unsubscribeOnDestroy(dispose)
    }
}
