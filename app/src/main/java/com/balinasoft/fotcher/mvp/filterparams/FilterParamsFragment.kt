package com.balinasoft.fotcher.mvp.filterparams


import android.content.Context
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filterphotographers.OptionsEntity
import com.balinasoft.fotcher.entity.filterphotographers.ParamsEntity
import com.balinasoft.fotcher.mvp.filterparams.adapters.FilterParamsAdapter
import com.balinasoft.fotcher.mvp.filterparams.adapters.ViewHolder
import com.balinasoft.fotcher.mvp.filterparams.adapters.ViewHolder2
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_filterparams.*
import javax.inject.Inject
import javax.inject.Provider

class FilterParamsFragment : MvpAppCompatFragment(), FilterParamsContract.View {
    companion object {
        fun newInstance(params: Any): FilterParamsFragment {
            return FilterParamsFragment().apply {
                if (params is ParamsEntity)
                    arguments = Bundle().apply { putParcelable("options", params) }
                else if (params is com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity)
                    arguments = Bundle().apply { putParcelable("options", params) }
            }
        }
    }

    @InjectPresenter
    lateinit var filtesParamsPresenter: FilterParamsPresenter

    @Inject
    lateinit var presenterProvider: Provider<FilterParamsPresenter>

    @ProvidePresenter
    fun providePresenter(): FilterParamsPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null
    var data: ParamsEntity? = null
    var data2: com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_filterparams, container, false)
        setHasOptionsMenu(true)

        return view
    }
    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_filterparams.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_filterparams)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        if (arguments!!["options"] is ParamsEntity)
            data = arguments!!["options"] as ParamsEntity
        else if (arguments!!["options"] is com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity)
            data2 = arguments!!["options"] as com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity

        if (data2 == null) {
            (activity as AppCompatActivity).supportActionBar?.title =
                    data!!.caption
        } else {
            (activity as AppCompatActivity).supportActionBar?.title =
                    data2!!.caption
        }

        toolbar_filterparams.setNavigationOnClickListener { _ ->
            filtesParamsPresenter.onClickedBack()
        }
        val itemDecor = DividerItemDecoration(activity, ClipDrawable.HORIZONTAL)
        recyclerFilterParams.addItemDecoration(itemDecor)
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        recyclerFilterParams.layoutManager = linearLayoutManager



        recyclerFilterParams.adapter = object : FilterParamsAdapter<Any>(
                { item ->
                    if (item is OptionsEntity)
                        filtesParamsPresenter.onClickedItem(item)
                    else if (item is com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity)
                        filtesParamsPresenter.onClickedItem(item)
                }, data != null) {

            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return if (data2 == null) {
                    ViewHolder(view)
                } else {
                    ViewHolder2(view)
                }
            }
        }

        if (data2 == null) {
            filtesParamsPresenter.setParams(data!!)
            (recyclerFilterParams.adapter as FilterParamsAdapter<OptionsEntity>).list = data!!.options
            (recyclerFilterParams.adapter as FilterParamsAdapter<OptionsEntity>).notifyDataSetChanged()
        } else {
            filtesParamsPresenter.setParams(data2!!)
            (recyclerFilterParams.adapter as FilterParamsAdapter<com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity>).list = data2!!.options
            (recyclerFilterParams.adapter as FilterParamsAdapter<com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity>).notifyDataSetChanged()
        }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

}