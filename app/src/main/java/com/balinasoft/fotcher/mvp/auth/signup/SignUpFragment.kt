package com.balinasoft.fotcher.mvp.auth.signup

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.Constants.USERS
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import kotlinx.android.synthetic.main.fragment_sign_up.*
import javax.inject.Inject
import javax.inject.Provider

class SignUpFragment : MvpAppCompatFragment(), SignUpContract.View,
        AdapterView.OnItemSelectedListener {

    @InjectPresenter
    lateinit var signUpPresenter: SignUpPresenter
    @Inject
    lateinit var presenterProvider: Provider<SignUpPresenter>

    @ProvidePresenter
    fun providePresenter(): SignUpPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_sign_up, container, false)

        return view
    }

    override fun onStart() {
        super.onStart()
        button.setOnClickListener { _ ->
            signUpPresenter.onClickedSignUp(
                    spinnerMain.selectedItem.toString(),
                    emailSignUp.text.toString(),
                    passwordSignUp1.text.toString(),
                    passwordSignUp2.text.toString()
            )
        }

        val aa = ArrayAdapter(context, R.layout.spinner_textview_main, USERS.keys.toTypedArray())
        aa.setDropDownViewResource(R.layout.spinner_textview)
        spinnerMain.adapter = aa

        (activity as AppCompatActivity).setSupportActionBar(toolbar_signup)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_signup.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })

        verification.setOnClickListener { _ -> signUpPresenter.onClickedVerefication() }

        emailSignUp.setText("")
        passwordSignUp1.setText("")
        passwordSignUp2.setText("")
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {

    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }
}