package com.balinasoft.fotcher.mvp.profile.profileoptions

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem

@StateStrategyType(SkipStrategy::class)
interface ProfileOptionsContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        @StateStrategyType(SkipStrategy::class)
        fun showParams(paramsItem: List<ParamsItem>)

        @StateStrategyType(SkipStrategy::class)
        fun showAvatar(url: String)

        @StateStrategyType(SkipStrategy::class)
        fun onBack()
    }
}