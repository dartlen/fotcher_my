package com.balinasoft.fotcher.mvp.photostudio.filterphotostudios

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity

@StateStrategyType(SkipStrategy::class)
interface FilterPhotoStudiosContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showFilters(filterPhotostudiosEntity: FilterPhotostudiosEntity)
        fun setDelault(size: Int, filterPhotostudiosEntity: FilterPhotostudiosEntity)
    }
}