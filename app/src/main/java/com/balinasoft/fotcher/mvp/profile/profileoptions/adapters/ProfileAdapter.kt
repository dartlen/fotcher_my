package com.balinasoft.fotcher.mvp.profile.profileoptions.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.inflate
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.item_edite.view.*
import kotlinx.android.synthetic.main.item_range.view.*
import kotlinx.android.synthetic.main.item_spinner.view.*

class ProfileAdapter(val context: Context?, val listener: (ParamsItem) -> Unit,
                     val listener2: (ParamsItem) -> Unit, val listener3: (ParamsItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_EDITE = 0
        const val TYPE_SPINNER = 1
        const val TYPE_IN_RANGE = 2
    }

    var list: ArrayList<ParamsItem> = arrayListOf()

    override fun getItemViewType(position: Int): Int {
        return when (list[position].type) {
            "STRING" -> TYPE_EDITE
            "SELECT" -> TYPE_SPINNER
            "INT_RANGE" -> TYPE_IN_RANGE
            else -> 100
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_EDITE -> ViewHolderEdite(parent.inflate(R.layout.item_edite))
            TYPE_SPINNER -> ViewHolderSpinner(parent.inflate(R.layout.item_spinner))
            TYPE_IN_RANGE -> ViewHolderRange(parent.inflate(R.layout.item_range))
            else -> ViewHolder(parent.inflate(R.layout.item_range))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return when (getItemViewType(position)) {
            TYPE_EDITE -> (holder as ViewHolderEdite).bind(list[position], listener)
            TYPE_SPINNER -> {
                (holder as ViewHolderSpinner).bind(list[position], context, listener2)
            }
            TYPE_IN_RANGE -> (holder as ViewHolderRange).bind(list[position], listener3)
            else -> (holder as ViewHolderRange).bind(list[position], listener3)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ViewHolderEdite(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ParamsItem?, listener: (ParamsItem) -> Unit) {
        itemView.edit.hint = item!!.caption

        if (item.name!! == "phone") {
            itemView.textInputLayout.visibility = View.INVISIBLE
            itemView.phone_input.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int,
                                           before: Int, count: Int) {
                    item.value = s.toString()
                    listener(item)
                }
            })

            val x: CharSequence = item.value as CharSequence
            if (x.indexOf("*") == -1)
                itemView.phone_input.setText(x)
        } else
            itemView.phone_input.visibility = View.INVISIBLE

        if (item.value != null)

            if (item.value != null)
                itemView.edit.setText(item.value as String)
        itemView.edit.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (s.indexOf("*") == -1) {
                    item.value = s.toString()
                    listener(item)
                }
            }
        })

    }
}

class ViewHolderSpinner(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ParamsItem?, context: Context?, listener: (ParamsItem) -> Unit) {
        val listvalue = arrayListOf<String>()

        item!!.options!!.forEach { x ->
            if (x!!.caption!! != "Яя")
                listvalue.add(x.caption!!)
        }
        listvalue.add(item.caption!!)

        val spinnerArrayAdapter = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context!!, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
        itemView.spinnerMain.adapter = spinnerArrayAdapter
        itemView.spinnerMain.setSelection(checkValueSpinner(item) - 1, false)
        itemView.spinnerMain.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                item.options!!.forEachIndexed { index, optionsItem -> optionsItem!!.check = index == position } //if(index==position) optionsItem!!.check=true else optionsItem!!.check=false
                listener(item)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }
    }

    fun checkValueSpinner(paramsEntity: ParamsItem): Int {
        if (paramsEntity.value == null) {

            return paramsEntity.options!!.size
        }
        return ((paramsEntity.value as LinkedTreeMap<*, *>)["name"] as String).toInt()
    }
}

class ViewHolderRange(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ParamsItem?, listener: (ParamsItem) -> Unit) {
        itemView.rangeSeekbar1.setOnSeekbarChangeListener { maxValue ->
            if (maxValue.toInt() != 0) {
                itemView.maxValueParam1.text = maxValue.toString()
                item!!.border = arrayListOf(maxValue.toInt())
                listener(item)
            }
        }
        if (item!!.border != null) {
            itemView.rangeSeekbar1.minStartValue = item.border!![0].toFloat()
        } else {
            itemView.rangeSeekbar1.minStartValue = item.minInt!!.toFloat()
        }
        itemView.rangeSeekbar1.maxValue = item.maxInt!!.toFloat()
        itemView.rangeSeekbar1.minValue = item.minInt!!.toFloat()
        itemView.nameSeekbar1.text = item.caption
        itemView.rangeSeekbar1.apply()
    }
}