package com.balinasoft.fotcher.mvp.photostudio.map.map

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.google.android.gms.maps.model.Marker


@StateStrategyType(OneExecutionStateStrategy::class)
interface MapContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        fun showMarker(m: Marker)
        fun showInfo(data: ContentItem)
        fun showDisableMarker(marker: Marker)
        fun disableStudioInfo()
        @StateStrategyType(SkipStrategy::class)
        fun enableStudioInfo()

        @StateStrategyType(SkipStrategy::class)
        fun clearMarkers(list: MutableList<Marker>)
    }

    interface OnSearchListener {
        fun onSearch(list: MutableList<ContentItem?>)
    }
}