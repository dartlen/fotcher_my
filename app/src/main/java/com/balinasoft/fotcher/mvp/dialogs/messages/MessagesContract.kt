package com.balinasoft.fotcher.mvp.dialogs.messages

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.messages.ContentItem
import com.balinasoft.fotcher.entity.messages.MessagesResponse

@StateStrategyType(SkipStrategy::class)
interface MessagesContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        @StateStrategyType(SkipStrategy::class)
        fun showMessages(list: MutableList<ContentItem?>, t: MessagesResponse)

        @StateStrategyType(SkipStrategy::class)
        fun loadFirstPage()

        @StateStrategyType(SkipStrategy::class)
        fun removeFooter()

        @StateStrategyType(SkipStrategy::class)
        fun hideProgressLoading()

        @StateStrategyType(SkipStrategy::class)
        fun showNext(t: MessagesResponse)
    }
}