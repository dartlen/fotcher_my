package com.balinasoft.fotcher.mvp.auth.signin

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.view.ProgressDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.nav_header_main.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar
import javax.inject.Inject
import javax.inject.Provider

class SignInFragment : MvpAppCompatFragment(), SignInContract.View {

    @InjectPresenter
    lateinit var signInPresenter: SignInPresenter

    @Inject
    lateinit var presenterProvider: Provider<SignInPresenter>

    @ProvidePresenter
    fun providePresenter(): SignInPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    lateinit var mUnregistrar: Unregistrar

    lateinit var dialog: Dialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        dialog = ProgressDialog.progressDialog(activity!!)

        val signup = view.findViewById(R.id.sigup) as TextView
        val logo = view.findViewById(R.id.currentUserImage) as ImageView
        val signin = view.findViewById(R.id.buttonLogin) as ImageView
        val recovery = view.findViewById(R.id.recovery) as TextView

        signup.setOnClickListener {
            signInPresenter.onClickedSignUp()
        }

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(activity) { isOpen ->
            run {
                val lp = signup.layoutParams as ConstraintLayout.LayoutParams
                if (isOpen) {
                    logo.visibility = View.GONE
                    lp.setMargins(8, 40, 8, 8)
                } else {
                    logo.visibility = View.VISIBLE
                    lp.setMargins(8, 59, 8, 8)
                }
            }
        }

        signin.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(context, R.anim.signin))
            signInPresenter.onClickedSignIn(emailSignIn.text.toString(),
                    passwordSignIn.text.toString())
        }

        recovery.setOnClickListener {
            signInPresenter.onClickedRecovery()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress(boolean: Boolean) {
        if (boolean) dialog.show() else dialog.dismiss()
    }

    override fun showEmail(email: String) {
        nameCurrentUser.text = email
    }

    override fun showCurrentUser(url: String, name: String) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(currentUserImg)

        nameCurrentUser.text = name
    }
}