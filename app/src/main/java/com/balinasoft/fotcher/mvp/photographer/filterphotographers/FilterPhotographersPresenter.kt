package com.balinasoft.fotcher.mvp.photographer.filterphotographers

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERPARAMS
import com.balinasoft.fotcher.Constants.PHOTOGRAPHERS
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotographers.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filterphotographers.FilterPhotographersInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterPhotoStudiosPresenter @Inject constructor(
        private val router: Router,
        private val filterPhotographersInteractor: FilterPhotographersInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterPhotographersContract.View>() {
    var data: FilterParamsEntity? = null
    fun onClickedClean() {
        viewState.setDelault(data!!.params!![0]!!.options!!.size,
                data!!.params!![1]!!.options!!.size,
                data!!.params!![2]!!.options!!.size)
        for (i in 0 until data!!.params!!.size) {
            for (k in 0 until data!!.params!![i]!!.options!!.size) {
                if (data!!.params!![i]!!.options!![k]!!.check == true)
                    data!!.params!![i]!!.options!![k]!!.check = false
            }
        }
        filterPhotographersInteractor.saveFilters(data!!)
    }

    fun onStart() {

        val x = filterPhotographersInteractor.onStart().subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe(
                        { t1: FilterParamsEntity? ->
                            data = t1
                            viewState.showFilters(t1!!)

                        },
                        { t2: Throwable? ->
                            Log.d(t2!!.toString(), "")
                        }
                )
        unsubscribeOnDestroy(x)
    }

    fun onClickedParams(item: ParamsEntity) {
        saveFilters()
        router.navigateTo(FILTERPARAMS, item)
    }

    fun onItemSelectedRate(position: Int) {
        for (i in 0 until data!!.params!![0]!!.options!!.size) {
            if (i == position)
                data!!.params!![0]!!.options!![i]!!.check = true

        }
    }

    fun onItemSelectedCity(position: Int) {
        for (i in 0 until data!!.params!![1]!!.options!!.size) {
            if (i == position)
                data!!.params!![1]!!.options!![i]!!.check = true

        }
    }

    fun onItemSelectedExp(position: Int) {
        for (i in 0 until data!!.params!![2]!!.options!!.size) {
            if (i == position)
                data!!.params!![2]!!.options!![i]!!.check = true

        }
    }

    private fun saveFilters() {
        filterPhotographersInteractor.saveFilters(data!!)
    }

    fun checkValueSpinner(number: Int): Int {
        for (i in 0 until data!!.params!![number]!!.options!!.size) {
            if (data!!.params!![number]!!.options!![i]!!.check!!) {
                return i
            }
        }
        return data!!.params!![number]!!.options!!.size
    }

    fun onClickedApply() {
        filterPhotographersInteractor.saveFilters(data!!)
        filterPhotographersInteractor.setFlagForReloadPhotographers()
        router.backTo(PHOTOGRAPHERS)
    }
}
