package com.balinasoft.fotcher.mvp.activity

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.BuildConfig
import com.balinasoft.fotcher.Constants.COMMENTS
import com.balinasoft.fotcher.Constants.DIALOG
import com.balinasoft.fotcher.Constants.FAVORITES
import com.balinasoft.fotcher.Constants.FILTERMODELS
import com.balinasoft.fotcher.Constants.FILTEROPTIONSMODEL
import com.balinasoft.fotcher.Constants.FILTERPARAMS
import com.balinasoft.fotcher.Constants.FILTERPARAMSMODEL
import com.balinasoft.fotcher.Constants.FILTERPHOTOGRAPHERS
import com.balinasoft.fotcher.Constants.FILTERPHOTOSTUDIOS
import com.balinasoft.fotcher.Constants.GALLERY
import com.balinasoft.fotcher.Constants.INFOPHOTOGRAPHER
import com.balinasoft.fotcher.Constants.INFOPHOTOSTUDIO
import com.balinasoft.fotcher.Constants.INFOPROFILEOPTION
import com.balinasoft.fotcher.Constants.ITINERARY
import com.balinasoft.fotcher.Constants.MESSAGES
import com.balinasoft.fotcher.Constants.MODELPRAMS
import com.balinasoft.fotcher.Constants.MODELS
import com.balinasoft.fotcher.Constants.OPTIONSMODEL
import com.balinasoft.fotcher.Constants.PHOTOGRAPHERS
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.Constants.PROFILEMODEL
import com.balinasoft.fotcher.Constants.PROFILEOPTION
import com.balinasoft.fotcher.Constants.PROFILEPHOTOGRAHPER
import com.balinasoft.fotcher.Constants.PROFILEPHOTOSTUDIO
import com.balinasoft.fotcher.Constants.REC
import com.balinasoft.fotcher.Constants.RESERVATIONS
import com.balinasoft.fotcher.Constants.RESERVE
import com.balinasoft.fotcher.Constants.SIGNIN
import com.balinasoft.fotcher.Constants.SIGNUP
import com.balinasoft.fotcher.Constants.VERIFICATION
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.mvp.activity.adapters.NavigationAdapter
import com.balinasoft.fotcher.mvp.comments.CommentsFragment
import com.balinasoft.fotcher.mvp.dialogs.dialog.DialogFragment
import com.balinasoft.fotcher.mvp.photostudio.favorites.FavoritesFragment
import com.balinasoft.fotcher.mvp.model.filtermodels.FilterModelsFragment
import com.balinasoft.fotcher.mvp.model.filteroptionsmodels.FilterOptionModelsFragment
import com.balinasoft.fotcher.mvp.filterparams.FilterParamsFragment
import com.balinasoft.fotcher.mvp.model.filterparamsmodel.FilterParamsModelsFragment
import com.balinasoft.fotcher.mvp.photographer.filterphotographers.FilterPhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.filterphotostudios.FilterPhotoStudiosFragment
import com.balinasoft.fotcher.mvp.gallery.GalleryFragment
import com.balinasoft.fotcher.mvp.photographer.infophotographer.InfoPhotographerFragment
import com.balinasoft.fotcher.mvp.photostudio.infophotostudio.InfoPhotostudioFragment
import com.balinasoft.fotcher.mvp.infoprofileoption.InfoProfileOptionFragment
import com.balinasoft.fotcher.mvp.photostudio.map.itinerary.ItineraryFragment
import com.balinasoft.fotcher.mvp.dialogs.messages.MessagesFragment
import com.balinasoft.fotcher.mvp.model.modelparams.ModelParamsFragment
import com.balinasoft.fotcher.mvp.model.models.ModelsFragment
import com.balinasoft.fotcher.mvp.model.optionsmodel.OptionsModelFragment
import com.balinasoft.fotcher.mvp.photographer.photographers.PhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.photostudios.PhotoStudiosFragment
import com.balinasoft.fotcher.mvp.profile.profilemodel.ProfileModelFragment
import com.balinasoft.fotcher.mvp.profile.profileoptions.ProfileOptionFragment
import com.balinasoft.fotcher.mvp.profile.profilephotographer.ProfilePhotographerFragment
import com.balinasoft.fotcher.mvp.profile.profilephotostudio.ProfilePhotostudioFragment
import com.balinasoft.fotcher.mvp.auth.recovery.RecoveryFragment
import com.balinasoft.fotcher.mvp.reservation.reservations.ReservationsFragment
import com.balinasoft.fotcher.mvp.reservation.reserve.ReserveFragment
import com.balinasoft.fotcher.mvp.auth.signin.SignInFragment
import com.balinasoft.fotcher.mvp.auth.signup.SignUpFragment
import com.balinasoft.fotcher.mvp.auth.verification.VerificationFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.nav_header_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import javax.inject.Inject
import javax.inject.Provider

class MainActivity : MvpAppCompatActivity(), MainActivityContract.View {

    init {
        FotcherApp.componentsManager.fotcher().inject(this)
    }

    @InjectPresenter
    lateinit var mainActivityPresenter: MainActivityPresenter

    @Inject
    lateinit var presenterProvider: Provider< MainActivityPresenter>

    @ProvidePresenter
    fun providePresenter():  MainActivityPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var signInFragment: SignInFragment

    @Inject
    lateinit var signUpFragment: SignUpFragment

    @Inject
    lateinit var recoveryFragment: RecoveryFragment

    @Inject
    lateinit var photoStudiosFragment: PhotoStudiosFragment

    @Inject
    lateinit var reservationsFragment: ReservationsFragment

    @Inject
    lateinit var messagesFragment: MessagesFragment

    @Inject
    lateinit var modelsFragment: ModelsFragment

    @Inject
    lateinit var photographersFragment: PhotographersFragment

    @Inject
    lateinit var filterPhotostudiosFragment: FilterPhotoStudiosFragment

    @Inject
    lateinit var filterPhotographersFragment: FilterPhotographersFragment

    @Inject
    lateinit var filterModelsFragment: FilterModelsFragment

    @Inject
    lateinit var profileOptionFragment: ProfileOptionFragment

    @Inject
    lateinit var favoritesFragment: FavoritesFragment

    private val navigator = object : SupportFragmentNavigator(supportFragmentManager,
            R.id.container_activity) {
        override fun createFragment(screenKey: String, data: Any): Fragment {
            when (screenKey) {
                SIGNIN -> return signInFragment
                SIGNUP -> return signUpFragment
                REC -> return recoveryFragment

                PHOTOSTUDIOS -> return photoStudiosFragment
                PROFILEPHOTOGRAHPER -> return ProfilePhotographerFragment.newInstance(data)
                MESSAGES -> return messagesFragment
                MODELS -> return modelsFragment
                PHOTOGRAPHERS -> return photographersFragment
                FILTERPHOTOSTUDIOS -> return filterPhotostudiosFragment
                VERIFICATION -> return VerificationFragment.newInstance(data)
                FILTERPHOTOGRAPHERS -> return filterPhotographersFragment
                FILTERPARAMS -> return FilterParamsFragment.newInstance(data)
                GALLERY -> return GalleryFragment.newInstance(data)
                INFOPHOTOGRAPHER -> return InfoPhotographerFragment.newInstance(data)
                INFOPHOTOSTUDIO -> return InfoPhotostudioFragment.newInstance(data)
                PROFILEPHOTOSTUDIO -> return ProfilePhotostudioFragment.newInstance(data)
                RESERVATIONS -> return ReservationsFragment.newInstance(data)
                COMMENTS -> return CommentsFragment.newInstance(data)
                RESERVE -> return ReserveFragment.newInstance(data)
                FILTERMODELS -> return filterModelsFragment
                FILTERPARAMSMODEL -> return FilterParamsModelsFragment.newInstance(data)
                FILTEROPTIONSMODEL -> return FilterOptionModelsFragment.newInstance(data)
                PROFILEMODEL -> return ProfileModelFragment.newInstance(data)
                PROFILEOPTION -> return profileOptionFragment

                INFOPROFILEOPTION -> return InfoProfileOptionFragment.newInstance(data)
                OPTIONSMODEL -> return OptionsModelFragment.newInstance(data)

                DIALOG -> return DialogFragment.newInstance(data)
                FAVORITES -> return favoritesFragment

                MODELPRAMS -> return ModelParamsFragment.newInstance(data)
                ITINERARY -> return ItineraryFragment.newInstance(data)
                else -> throw RuntimeException("Unknown screen key!")
            }
        }

        override fun showSystemMessage(message: String) {
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        }

        override fun exit() {
            finish()
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG) {
            val fabric = Fabric.Builder(this)
                    .kits(Crashlytics())
                    .debuggable(true)
                    .build()
            Fabric.with(fabric)
        }
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar_activity)

        val drawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
                this,
                drawer_layout,
                toolbar_activity,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
            }

            override fun onDrawerOpened(drawerView: View) {
                showAvatar()
                mainActivityPresenter.getUnreadedDialogs()
                hideKeyboard()
                super.onDrawerOpened(drawerView)
            }
        }

        drawerToggle.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        navigation_drawer_list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false)
        navigation_drawer_list.addItemDecoration(DividerItemDecoration(this, HORIZONTAL))
        navigation_drawer_list.adapter = NavigationAdapter { item -> mainActivityPresenter.onClickedItemMenu(item) }

        mainActivityPresenter.getUnreadedDialogs()
        mainActivityPresenter.onStart()
    }

    override fun hideDrawerMenu() {
        super.hideDrawerMenu()
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    fun showAvatar() {
        val url = mainActivityPresenter.getAvatar()
        if (url != "")
            Glide.with(this)
                    .load(url)
                    .apply(RequestOptions.circleCropTransform())
                    .into(currentUserImg)
        nameCurrentUser.text = mainActivityPresenter.getUserName()
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container_activity)
        if (fragment == null || (fragment is SignInFragment) || (fragment is PhotoStudiosFragment)) {
            return
        } else {
            super.onBackPressed()
        }
    }

    override fun showCurrentUser(url: String, name: String) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(currentUserImg)

        nameCurrentUser.text = name
    }

    override fun showEmail(email: String) {
        nameCurrentUser.text = email
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    fun hideKeyboard() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.currentFocus.hideKey(imm)

    }

    override fun showUnreadedDialogs(value: Int) {
        (navigation_drawer_list.adapter as NavigationAdapter).unreadedDialogs = value
        (navigation_drawer_list.adapter as NavigationAdapter).notifyItemChanged(4)
    }
}




