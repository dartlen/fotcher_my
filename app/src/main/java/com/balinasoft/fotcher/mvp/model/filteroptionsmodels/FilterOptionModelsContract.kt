package com.balinasoft.fotcher.mvp.model.filteroptionsmodels

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity

@StateStrategyType(SkipStrategy::class)
interface FilterOptionModelsContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showFilters(paramsEntity: ParamsEntity)
    }
}