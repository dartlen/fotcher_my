package com.balinasoft.fotcher.mvp.profile.profilephotographer

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.user.UserEntity

@StateStrategyType(SkipStrategy::class)
interface ProfilePhotographerContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        @StateStrategyType(SkipStrategy::class)
        fun showUser(userEntity: UserEntity)

        @StateStrategyType(SkipStrategy::class)
        fun call(number: String, name: String?)

        fun showProgress()
        @StateStrategyType(SkipStrategy::class)
        fun hideProgress()
    }
}