package com.balinasoft.fotcher.mvp.auth.signin

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface SignInContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        fun showProgress(boolean: Boolean)
        fun showEmail(email: String)
        fun showCurrentUser(url: String, name: String)
    }
}