package com.balinasoft.fotcher.mvp.comments.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.comments.pojo.ContentItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_comments.view.*
import java.text.SimpleDateFormat
import java.util.*

class CommentsAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list!![position])
    }

    var list: ArrayList<ContentItem?>? = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_comments, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    fun add(r: ContentItem) {
        list!!.reverse()
        list!!.add(r)
        list!!.reverse()
        notifyItemInserted(list!!.size - 1)
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    object DateUtils {
        @SuppressLint("SimpleDateFormat")
        @JvmStatic
        fun toSimpleString(date: Date): String {
            val format = SimpleDateFormat("dd.MM.yyy")
            return format.format(date)
        }
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bind(contentItem: ContentItem?) {
        itemView.name.text = contentItem!!.user!!.name + " " + contentItem.user!!.surname
        val date = SimpleDateFormat("yyyy-MM-dd").parse(contentItem.date)
        itemView.date.text = DateUtils.toSimpleString(date)
        itemView.comment.text = contentItem.text

        val options = RequestOptions()
        Glide.with(itemView.context)
                .load(contentItem.user.smallAvatar)
                .apply(options)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.avatarComment)
        itemView.library_tinted_wide_ratingbar.rating = contentItem.star!!

        itemView.frame.setOnClickListener { }
    }
}