package com.balinasoft.fotcher.mvp.model.filtermodels

import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.mvp.model.filtermodels.adapters.FilterModelsAdapter
import kotlinx.android.synthetic.main.fragment_filtermodels.*
import javax.inject.Inject
import javax.inject.Provider

class FilterModelsFragment : MvpAppCompatFragment(), FilterModelsContract.View {

    @InjectPresenter
    lateinit var filterModelsPresenter: FilterModelsPresenter

    @Inject
    lateinit var presenterProvider: Provider<FilterModelsPresenter>

    @ProvidePresenter
    fun providePresenter(): FilterModelsPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)

        val v = inflater.inflate(R.layout.fragment_filtermodels, container, false)
        setHasOptionsMenu(true)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerFilterModels.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerFilterModels.addItemDecoration(itemDecor)
        recyclerFilterModels.adapter = FilterModelsAdapter { Item: ParamsEntity -> filterModelsPresenter.onClickedParams(Item) }
        filterModelsPresenter.onStart()
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbar_filter_models)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_filter_models.setNavigationOnClickListener(
                { _ -> filterModelsPresenter.onBackPressed() })

    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_filter_studios, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_clean -> {
                filterModelsPresenter.onClickedClean()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun showFilters(data: FilterModelsEntity) {

        val listvalue = arrayListOf<String>()

        data.params[0].options!!.forEach { x ->
            listvalue.add(x!!.caption!!)
        }
        listvalue.add(data.params[0].caption!!)

        val spinnerArrayAdapter = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
        spinnerMain.adapter = spinnerArrayAdapter
        spinnerMain.setSelection(filterModelsPresenter.checkValueSpinner(0), false)
        spinnerMain.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                filterModelsPresenter.onItemSelectedRate(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }

        val listvalue3 = arrayListOf<String>()
        data.params[2].options!!.forEach { x ->
            listvalue3.add(x!!.caption!!)
        }
        listvalue3.add(data.params[2].caption!!)

        val spinnerArrayAdapter3 = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue3) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue3.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter3.setDropDownViewResource(R.layout.spinner_textview)
        spinner3.adapter = spinnerArrayAdapter3
        spinner3.setSelection(filterModelsPresenter.checkValueSpinner(2), false)
        spinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                filterModelsPresenter.onItemSelectedCity(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }

        val listvalue2 = arrayListOf<String>()

        data.params[1].options!!.forEach { x ->
            listvalue2.add(x!!.caption!!)
        }
        listvalue2.add(data.params[1].caption!!)

        val spinnerArrayAdapter2 = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue2) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue2.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_textview)
        spinner2.adapter = spinnerArrayAdapter2
        spinner2.setSelection(filterModelsPresenter.checkValueSpinner(1), false)
        spinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                filterModelsPresenter.onItemSelectedGender(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        }
        val listvalue4 = arrayListOf<String>()

        data.params[3].options!!.forEach { x ->
            listvalue4.add(x!!.caption!!)
        }
        listvalue4.add(data.params[3].caption!!)

        val spinnerArrayAdapter4 = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue4) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue4.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter4.setDropDownViewResource(R.layout.spinner_textview)
        spinner4.adapter = spinnerArrayAdapter4
        spinner4.setSelection(filterModelsPresenter.checkValueSpinner(3), false)
        spinner4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                filterModelsPresenter.onItemSelectedExp(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }

        val listforrecycler = arrayListOf<ParamsEntity>()
        for ((count, i) in data.params.withIndex()) {
            if (count >= 4) {
                listforrecycler.add(i)
            }
        }

        (recyclerFilterModels.adapter as FilterModelsAdapter).list = listforrecycler
        (recyclerFilterModels.adapter as FilterModelsAdapter).notifyDataSetChanged()

        buttonFilterApply.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(context, R.anim.signin))
            filterModelsPresenter.onClickedApply()
        }

        progressBarFilterModels.visibility = View.GONE
        buttonFilterApply.visibility = View.VISIBLE
        recyclerFilterModels.visibility = View.VISIBLE
        spinnerMain.visibility = View.VISIBLE
        spinner2.visibility = View.VISIBLE
        spinner3.visibility = View.VISIBLE
    }

    override fun setDelault(size: Int, size1: Int, size2: Int, size3: Int) {
        spinnerMain.setSelection(size)
        spinner2.setSelection(size1)
        spinner3.setSelection(size2)
        spinner4.setSelection(size3)
    }
}