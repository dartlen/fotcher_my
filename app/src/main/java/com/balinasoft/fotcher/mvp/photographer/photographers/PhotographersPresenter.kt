package com.balinasoft.fotcher.mvp.photographer.photographers

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERPHOTOGRAPHERS
import com.balinasoft.fotcher.Constants.PROFILEPHOTOGRAHPER
import com.balinasoft.fotcher.entity.photographers.ContentItem
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.model.interactor.photographers.PhotographersInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PhotographersPresenter @Inject constructor(
        private val router: Router,
        private val photographersInteractor: PhotographersInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<PhotographersContract.View>() {

    fun onClickedFilter() {
        viewState.hideKeyboard()
        router.navigateTo(FILTERPHOTOGRAPHERS, 0)
    }

    fun onStart(currentPage: Int) {
        if (currentPage == 0 || photographersInteractor.getFlagReload())
            photographersInteractor.onStart()!!.subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe(
                            { t: PhotographersResponse? ->
                                if (!photographersInteractor.getFlagReload()) {
                                    viewState.showPhotographers(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                } else {
                                    photographersInteractor.setFlagReload()
                                    viewState.showPhotographersClear(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                }
                            },
                            { _ ->
                                Log.d("", "")
                            })
    }

    fun onClickedPhotographer(item: ContentItem) {
        viewState.hideKeyboard()
        router.navigateTo(PROFILEPHOTOGRAHPER, arrayListOf(item.id, 0))
    }

    fun onInputedDataSearch(text: String) {
        val dispose = photographersInteractor.onStartSearch(text)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe(
                        { t: PhotographersResponse? ->
                            photographersInteractor.setFlagReload()
                            viewState.showPhotographersClear(t!!.content!!.toMutableList(), t)
                            viewState.hideProgressLoading()

                        },
                        { _ ->
                            Log.d("", "")
                        })
        unsubscribeOnDestroy(dispose)
    }

    fun loadNextPage(currentPage:Int, query:String){
        val disposable = photographersInteractor.loadNextPage(currentPage, query)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe(
                        { t: PhotographersResponse? ->
                           viewState.showPotographersNext(t)
                        },
                        { _: Throwable? ->

                        })
        unsubscribeOnDestroy(disposable)
    }

}
