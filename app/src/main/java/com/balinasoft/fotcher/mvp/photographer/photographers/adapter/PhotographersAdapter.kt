package com.balinasoft.fotcher.mvp.photographer.photographers.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.photographers.ContentItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_photographers.view.*

class PhotographersAdapter(private val listener: (ContentItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {

            ITEM -> (holder as ViewHolder).bind(list[position], listener)
            LOADING -> {
            }
        }
    }

    var list: MutableList<ContentItem?> = mutableListOf()

    var retryPageLoad = false
    var errorMsg: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> return ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_photographers, parent, false))
            LOADING -> {
                val v2 = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingVH(v2)
            }
        }
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return list.size
    }

    var isLoadingAdded = false

    val isEmpty: Boolean
        get() = itemCount == 0

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    fun add(r: ContentItem) {
        list.add(r)
        notifyItemInserted(list.size - 1)
    }

    fun addAll(moveResults: List<ContentItem?>?) {
        for (result in moveResults!!) {
            add(result!!)
        }
    }

    fun remove(r: ContentItem?) {
        val position = list.indexOf(r)
        if (position > -1) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(ContentItem())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = list.size - 1
        val result = getItem(position)

        if (result != null) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): ContentItem? {
        return list[position]
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(list.size - 1)
        if (errorMsg != null) this.errorMsg = errorMsg
    }

    /*
   View Holders
   _________________________________________________________________________________________________
    */

    class LoadingVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        private val ITEM = 0
        private val LOADING = 1
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ContentItem?, listener: (ContentItem) -> Unit) {

        val options = RequestOptions()
        Glide.with(itemView.context)
                .load(item!!.smallAvatar)
                .apply(options)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.avatarComment)

        if (item.leftStatus != null)
            itemView.comment.text = item.leftStatus
        else {
            itemView.comment.text = "Earth"
        }
        if (item.surname == "" && item.name == "") {
            itemView.name.text = "Фотограф"
        } else {
            itemView.name.text = item.surname + " " + item.name
        }
        itemView.setOnClickListener { _ -> listener(item) }
    }

}

