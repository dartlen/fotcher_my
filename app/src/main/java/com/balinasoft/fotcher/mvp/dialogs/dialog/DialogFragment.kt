package com.balinasoft.fotcher.mvp.dialogs.dialog

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.FileUtils
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse
import com.balinasoft.fotcher.entity.messages.User
import com.balinasoft.fotcher.mvp.dialogs.dialog.adapters.DialogAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_dialog.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class DialogFragment : MvpAppCompatFragment(), DialogContract.View {
    companion object {
        fun newInstance(user: Any) = DialogFragment()
                .apply {
                    arguments = Bundle()
                            .apply { putParcelable("user", user as User) }
                }
    }

    @InjectPresenter
    lateinit var dialogPresenter: DialogPresenter

    @Inject
    lateinit var presenterProvider: Provider<DialogPresenter>

    @ProvidePresenter
    fun providePresenter(): DialogPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    internal var linearLayoutManager: LinearLayoutManager? = null
    lateinit var mUnregistrar: Unregistrar
    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 3
    var currentPage = 0
    var TOTAL_PAGES = 100

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        return inflater.inflate(R.layout.fragment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_dialog)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_dialog.setNavigationOnClickListener { _ -> activity!!.onBackPressed() }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)

        recyclerDialog.itemAnimator = null
        linearLayoutManager!!.reverseLayout = true

        recyclerDialog.layoutManager = linearLayoutManager
        recyclerDialog.adapter = DialogAdapter()

        recyclerDialog.addOnScrollListener(
                object : PaginationScrollListener(linearLayoutManager!!) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {

                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        }
                    }
                })

        imageView9.setOnClickListener {
            dialogPresenter.clickedSendMessage(textInput.text.toString(), (arguments!!["user"] as User).id!!)
        }

        imageView10.setOnClickListener {
            dialogPresenter.clickedSendImage()
        }

        (recyclerDialog.adapter as DialogAdapter).setImageDialog(if ((arguments!!["user"] as User).smallAvatar == null) "" else (arguments!!["user"] as User).smallAvatar!!)

        mUnregistrar = KeyboardVisibilityEvent.registerEventListener(activity) { _ ->
            run {
                if (recyclerDialog != null)
                    recyclerDialog.scrollToPosition(0)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (currentPage < 1) {
            (recyclerDialog.adapter as DialogAdapter).list = mutableListOf()
            dialogPresenter.onStart((arguments!!["user"] as User).id!!)
        }
        accessLocationPermission()

    }

    override fun showDialog(t: List<DialogResponse>) {
        (recyclerDialog.adapter as DialogAdapter).list = t.toMutableList()
        (recyclerDialog.adapter as DialogAdapter).image = if ((arguments!!["user"] as User).smallAvatar == null) "" else (arguments!!["user"] as User).smallAvatar!!
        (recyclerDialog.adapter as DialogAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
        else
            isLast = true
    }

    private fun loadNextPage() {
        dialogPresenter.loadNextPage((arguments!!["user"]!! as User).id!!)
    }

    override fun hideProgressLoading() {
        dialogProgress.visibility = View.GONE
        recyclerDialog.visibility = View.VISIBLE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun clearOldMessage() {
        textInput.text!!.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.SELECTIMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val file = FileUtils.getFile(context, data!!.data)
                dialogPresenter.sendImage(file, (arguments!!["user"] as User).id!!)
            }
        }
    }

    private fun accessLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            val read = activity!!.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            val write = activity!!.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

            val listRequestPermission = ArrayList<String>()

            if (read != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            if (write != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            if (!listRequestPermission.isEmpty()) {
                val strRequestPermission = listRequestPermission.toTypedArray()
                requestPermissions(strRequestPermission, Constants.PERMISSION)
            }
        } else {
            // Pre-Marshmallow
        }
    }

    override fun selectImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"

        val packageManager = context!!.packageManager
        if (packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY).size>0) {
            startActivityForResult(intent, Constants.SELECTIMAGE)
        }
    }

    override fun showLoadedMessages(t: List<DialogResponse>, isLoad: Boolean) {
        this.isLoad = isLoad

        (recyclerDialog.adapter as DialogAdapter).addAll(t)
        recyclerDialog.scrollToPosition(0)
        if (currentPage != TOTAL_PAGES)
        else
            isLast = true
    }

    override fun showNewMessage(t: SendMessageResponse) {
        (recyclerDialog.adapter as DialogAdapter).addNewMessage(t)
        recyclerDialog.scrollToPosition(0)
    }
}