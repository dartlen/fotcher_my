package com.balinasoft.fotcher.mvp.photostudio.infophotostudio

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface InfoPhotostudioContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
    }
}