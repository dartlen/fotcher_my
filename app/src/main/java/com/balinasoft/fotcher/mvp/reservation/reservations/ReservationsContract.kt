package com.balinasoft.fotcher.mvp.reservation.reservations

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.reservations.ReservationsResponse

@StateStrategyType(OneExecutionStateStrategy::class)
interface ReservationsContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showReservations(data: ReservationsResponse)
    }
}