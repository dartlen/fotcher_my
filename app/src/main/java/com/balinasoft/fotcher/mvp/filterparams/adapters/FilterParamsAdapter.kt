package com.balinasoft.fotcher.mvp.filterparams.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filterphotographers.OptionsEntity
import kotlinx.android.synthetic.main.item_params.view.*

abstract class FilterParamsAdapter<T>(private val listener: (T) -> Unit, val bool: Boolean) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Binder<T>).bind(list!![position]!!, listener)

        //(holder as ViewHolder<*>).bind(list!![position]!!, listener)
    }

    var list: MutableList<T?>? = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (!bool)
            ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_params, parent, false))
        else
            ViewHolder2(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_params, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    internal interface Binder<T> {
        fun bind(data: T, listener: (T) -> Unit)
    }

    abstract fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder
}

class ViewHolder2(view: View) : RecyclerView.ViewHolder(view), FilterParamsAdapter.Binder<OptionsEntity> {

    @SuppressLint("SetTextI18n")
    override fun bind(data: OptionsEntity, listener: (OptionsEntity) -> Unit) {
        itemView.textParam.text = data.caption
        itemView.setOnClickListener {
            if (itemView.imageNull.visibility == View.VISIBLE) {
                //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
                itemView.imageNull.visibility = View.GONE
                itemView.imageCheck.visibility = View.VISIBLE
            } else {
                //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
                itemView.imageNull.visibility = View.VISIBLE
                itemView.imageCheck.visibility = View.GONE
            }
            listener(data)
        }
        if (data.check!!) {
            //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
            itemView.imageNull.visibility = View.GONE
            itemView.imageCheck.visibility = View.VISIBLE
        } else {
            //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
            itemView.imageNull.visibility = View.VISIBLE
            itemView.imageCheck.visibility = View.GONE
        }
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view), FilterParamsAdapter.Binder<com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity> {
    @SuppressLint("SetTextI18n")
    override fun bind(data: com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity, listener: (com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity) -> Unit) {
        itemView.textParam.text = data.caption
        itemView.setOnClickListener {
            if (itemView.imageNull.visibility == View.VISIBLE) {
                //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
                itemView.imageNull.visibility = View.GONE
                itemView.imageCheck.visibility = View.VISIBLE
            } else {
                //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
                itemView.imageNull.visibility = View.VISIBLE
                itemView.imageCheck.visibility = View.GONE
            }
            listener(data)
        }
        if (data.check!!) {
            //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
            itemView.imageNull.visibility = View.GONE
            itemView.imageCheck.visibility = View.VISIBLE
        } else {
            //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
            itemView.imageNull.visibility = View.VISIBLE
            itemView.imageCheck.visibility = View.GONE
        }
    }
}

