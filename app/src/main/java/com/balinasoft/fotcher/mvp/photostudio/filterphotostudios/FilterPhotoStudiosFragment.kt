package com.balinasoft.fotcher.mvp.photostudio.filterphotostudios

import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity
import com.balinasoft.fotcher.mvp.photostudio.filterphotostudios.adapters.FilterPhotostudiosAdapter
import kotlinx.android.synthetic.main.fragment_filterphotostudios.*
import javax.inject.Inject
import javax.inject.Provider

class FilterPhotoStudiosFragment : MvpAppCompatFragment(), FilterPhotoStudiosContract.View {

    @InjectPresenter
    lateinit var filterPhotoStudiosPresenter: FilterPhotoStudiosPresenter

    @Inject
    lateinit var presenterProvider: Provider<FilterPhotoStudiosPresenter>

    @ProvidePresenter
    fun providePresenter(): FilterPhotoStudiosPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_filterphotostudios, container,
                false)

        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rangeSeekbar1.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minValueParams1.text = minValue.toString()
            maxValueParam1.text = maxValue.toString()
        }

        rangeSeekbar2.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minValueParams2.text = minValue.toString()
            maxValueParam2.text = maxValue.toString()
        }

        rangeSeekbar3.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minValueParams3.text = minValue.toString()
            maxValueParam3.text = maxValue.toString()
        }

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerFilterPhotostudios.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerFilterPhotostudios.addItemDecoration(itemDecor)
        recyclerFilterPhotostudios.adapter = FilterPhotostudiosAdapter { Item: ParamsEntity -> filterPhotoStudiosPresenter.onClickedParams(Item) }

    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbar_filter_photostudios)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_filter_photostudios.setNavigationOnClickListener(
                { _ ->
                    filterPhotoStudiosPresenter.saveFilters(minValueParams1.text.toString(), maxValueParam1.text.toString(),
                            minValueParams2.text.toString(), maxValueParam2.text.toString(),
                            minValueParams3.text.toString(), maxValueParam3.text.toString())
                })
        filterPhotoStudiosPresenter.onStart()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_filter_studios, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_clean -> {
                filterPhotoStudiosPresenter.onClickedClean()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun showFilters(filterPhotostudiosEntity: FilterPhotostudiosEntity) {


        val listvalue = arrayListOf<String>()

        filterPhotostudiosEntity.params!![1]!!.options!!.forEach { x ->
            listvalue.add(x!!.caption!!)
        }
        listvalue.add(filterPhotostudiosEntity.params[1]!!.caption!!)

        val spinnerArrayAdapter = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
        spinner_photostudio.adapter = spinnerArrayAdapter
        spinner_photostudio.setSelection(filterPhotoStudiosPresenter.checkValueSpinner(1), false)
        spinner_photostudio.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                filterPhotoStudiosPresenter.onItemSelectedCity(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }

        buttonFilterPhotostudiosApply.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(context, R.anim.signin))
            filterPhotoStudiosPresenter.saveFilters(minValueParams1.text.toString(), maxValueParam1.text.toString(),
                    minValueParams2.text.toString(), maxValueParam2.text.toString(),
                    minValueParams3.text.toString(), maxValueParam3.text.toString())
        }

        val listforrecycler = arrayListOf<ParamsEntity>()
        for ((count, i) in filterPhotostudiosEntity.params.withIndex()) {
            if (count >= 4) {
                listforrecycler.add(i!!)
            }
        }

        (recyclerFilterPhotostudios.adapter as FilterPhotostudiosAdapter).list = listforrecycler
        (recyclerFilterPhotostudios.adapter as FilterPhotostudiosAdapter).notifyDataSetChanged()


        rangeSeekbar1.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
            filterPhotoStudiosPresenter.changedSeedBarValue(minValue.toString(),
                    maxValue.toString(), 0)
        }

        rangeSeekbar2.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
            filterPhotoStudiosPresenter.changedSeedBarValue(minValue.toString(),
                    maxValue.toString(), 2)
        }

        rangeSeekbar3.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
            filterPhotoStudiosPresenter.changedSeedBarValue(minValue.toString(),
                    maxValue.toString(), 3)
        }

        initseekbars(filterPhotostudiosEntity)

    }

    override fun setDelault(size: Int, filterPhotostudiosEntity: FilterPhotostudiosEntity) {
        spinner_photostudio.setSelection(size, false)
        initseekbars(filterPhotostudiosEntity)
    }

    private fun initseekbars(filterPhotostudiosEntity: FilterPhotostudiosEntity) {

        if (filterPhotostudiosEntity.params!![0]!!.borders != null) {
            rangeSeekbar1.setMaxStartValue(filterPhotostudiosEntity.params[0]!!.borders!![0]!!.toFloat())
            rangeSeekbar1.setMinStartValue(filterPhotostudiosEntity.params[0]!!.borders!![1]!!.toFloat())
        } else {
            rangeSeekbar1.setMaxStartValue(filterPhotostudiosEntity.params[0]!!.maxInt!!.toFloat())
            rangeSeekbar1.setMinStartValue(filterPhotostudiosEntity.params[0]!!.minInt!!.toFloat())
        }
        rangeSeekbar1.setMaxValue(filterPhotostudiosEntity.params[0]!!.maxInt!!.toFloat())
        rangeSeekbar1.setMinValue(filterPhotostudiosEntity.params[0]!!.minInt!!.toFloat())
        name.text = filterPhotostudiosEntity.params[0]!!.caption
        rangeSeekbar1.apply()

        if (filterPhotostudiosEntity.params[2]!!.borders != null) {
            rangeSeekbar2.setMaxStartValue(filterPhotostudiosEntity.params[2]!!.borders!![0]!!.toFloat())
            rangeSeekbar2.setMinStartValue(filterPhotostudiosEntity.params[2]!!.borders!![1]!!.toFloat())
        } else {
            rangeSeekbar2.setMaxStartValue(filterPhotostudiosEntity.params[2]!!.maxInt!!.toFloat())
            rangeSeekbar2.setMinStartValue(filterPhotostudiosEntity.params[2]!!.minInt!!.toFloat())
        }
        rangeSeekbar2.setMaxValue(filterPhotostudiosEntity.params[2]!!.maxInt!!.toFloat())
        rangeSeekbar2.setMinValue(filterPhotostudiosEntity.params[2]!!.minInt!!.toFloat())
        nameSeekbar2.text = filterPhotostudiosEntity.params[2]!!.caption
        rangeSeekbar2.apply()

        if (filterPhotostudiosEntity.params[3]!!.borders != null) {
            rangeSeekbar3.setMaxStartValue(filterPhotostudiosEntity.params[3]!!.borders!![0]!!.toFloat())
            rangeSeekbar3.setMinStartValue(filterPhotostudiosEntity.params[3]!!.borders!![1]!!.toFloat())
        } else {
            rangeSeekbar3.setMaxStartValue(filterPhotostudiosEntity.params[3]!!.maxInt!!.toFloat())
            rangeSeekbar3.setMinStartValue(filterPhotostudiosEntity.params[3]!!.minInt!!.toFloat())
        }
        rangeSeekbar3.setMaxValue(filterPhotostudiosEntity.params[3]!!.maxInt!!.toFloat())
        rangeSeekbar3.setMinValue(filterPhotostudiosEntity.params[3]!!.minInt!!.toFloat())
        nameSeekbar3.text = filterPhotostudiosEntity.params[3]!!.caption
        rangeSeekbar3.apply()
    }
}