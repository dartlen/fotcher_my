package com.balinasoft.fotcher.mvp.auth.verification

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface VerificationContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
    }
}