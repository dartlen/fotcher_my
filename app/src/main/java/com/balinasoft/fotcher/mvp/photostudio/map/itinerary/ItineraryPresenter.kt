package com.balinasoft.fotcher.mvp.photostudio.map.itinerary

import android.location.Location
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.model.interactor.itinerary.ItineraryInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import com.google.android.gms.maps.model.Marker
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class ItineraryPresenter @Inject constructor(
        private val router: Router,
        private val itineraryInteractor: ItineraryInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ItineraryContract.View>() {
    var list: MutableList<Marker> = mutableListOf()
    var listPoints: ArrayList<ContentItem> = arrayListOf()
    fun getMarker(m: Marker) {
        list.add(m)
    }

    fun onClickedMarker(m: Marker) {
        list.forEachIndexed { index, marker ->
            if (marker == m) {
                if (listPoints[index].flag) {
                    listPoints[index].flag = false
                    viewState.showDisableMarker(marker)
                    viewState.disableStudioInfo()
                } else {
                    listPoints[index].flag = true
                    viewState.showMarker(marker)
                    viewState.showInfo(listPoints[index])
                    viewState.enableStudioInfo()
                }

            } else {
                if (listPoints[index].flag) {
                    listPoints[index].flag = false
                    viewState.showDisableMarker(marker)
                }
            }
        }
    }

    fun setItinerary(/*list:ArrayList<ContentItem>*/) {
        this.list = arrayListOf()
        //listPoints = list


    }

    fun onGetLocation(location: Location?, photostudioLocation: String) {
        val dispose = itineraryInteractor.getItinerary(location, photostudioLocation).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    Log.d("", "")
                    if (t.status != "ZERO_RESULTS")
                        viewState.showItinerary(t)
                    /*else
                        router.backTo(PROFILEPHOTOSTUDIO)*/
                }, { _ ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }


}
