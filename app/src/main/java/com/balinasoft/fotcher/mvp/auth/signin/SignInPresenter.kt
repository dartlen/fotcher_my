package com.balinasoft.fotcher.mvp.auth.signin

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.Constants.REC
import com.balinasoft.fotcher.Constants.SIGNUP
import com.balinasoft.fotcher.entity.signin.SignInResponse
import com.balinasoft.fotcher.model.interactor.signin.SignInInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SignInPresenter @Inject constructor(
        private val router: Router,
        private val signInInteractor: SignInInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<SignInContract.View>() {

    fun onClickedSignUp() {
        router.navigateTo(SIGNUP, 0)
        viewState.hideKeyboard()
    }

    fun onClickedSignIn(email: String, password: String) {
        viewState.hideKeyboard()
        if (password == "" && email == "")
            viewState.showToast("Введите email и пароль!")
        else if (password == "")
            viewState.showToast("Введите пароль")
        else if (email == "")
            viewState.showToast("Введите email")
        else if (password.length >= 8)
            if (email.isEmailValid()) {
                val d = signInInteractor.signin(email, password)
                        .subscribeOn(appSchedulers.io())
                        .observeOn(appSchedulers.ui())
                        .doOnSubscribe { viewState.showProgress(true) }
                        .subscribe({ t ->
                            viewState.showProgress(false)
                            if ((t.body() as SignInResponse).token != null
                                    && (t.body() as SignInResponse).needConfirm == false) {
                                signInInteractor.saveToken((t.body() as SignInResponse).token)
                                val dispose = signInInteractor.getCurrentUser()
                                        .subscribeOn(appSchedulers.io())
                                        .observeOn(appSchedulers.ui())
                                        .subscribe({ t ->
                                            if (t!!.smallAvatar != null && t.name != null && t.surname != null)
                                                viewState.showCurrentUser(t.smallAvatar!!, t.name + " " + t.surname)
                                            else
                                                viewState.showEmail(t.email!!)
                                            signInInteractor.saveUserId(t.id!!)
                                            if (t.name != "" && t.surname != "")
                                                signInInteractor.setUserName(t.name + " " + t.surname)
                                            else
                                                signInInteractor.setUserName(t.email!!)
                                            signInInteractor.saveType(t.type!!)
                                            if (t.bigAvatar != null)
                                                signInInteractor.saveImage(t.bigAvatar)
                                        }, {

                                        })
                                unsubscribeOnDestroy(dispose)
                                router.navigateTo(PHOTOSTUDIOS, 0)

                            } else {
                                viewState.showToast("Аккаунт необходимо верифицировать")
                            }
                        }, { t ->
                            viewState.showToast(t.message)
                            viewState.showProgress(false)
                        })
                unsubscribeOnDestroy(d)
            } else
                viewState.showToast("Неверный email")
        else {
            viewState.showToast("Пароль должен быть больше 8 символов")
        }
    }

    fun onClickedRecovery() {
        router.navigateTo(REC, 0)
    }
}
