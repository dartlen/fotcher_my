package com.balinasoft.fotcher.mvp.activity.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.inflate
import kotlinx.android.synthetic.main.item_navigation.view.*

class NavigationAdapter(val listener: (Navigation) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val list = mutableListOf(
            Navigation(R.drawable.ic_photostudios, "Фотостудии"),
            Navigation(R.drawable.ic_favorites, "Избранное"),
            Navigation(R.drawable.ic_photographers, "Фотографы"),
            Navigation(R.drawable.ic_models, "Фотомодели"),
            Navigation(R.drawable.ic_messages, "Сообщения"),
            Navigation(R.drawable.ic_my_reservation, "Мои бронирования"),
            Navigation(R.drawable.ic_profile, "Профиль"))

    var unreadedDialogs = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NavigationViewHolder(parent.inflate(R.layout.item_navigation))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (list[position].name == "Сообщения")
            (holder as NavigationViewHolder).bind(list[position], listener, unreadedDialogs)
        else
            (holder as NavigationViewHolder).bind(list[position], listener, 0)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class NavigationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(navigation: Navigation, listener: (Navigation) -> Unit, unreadedDialogs: Int) {
            itemView.nameNavigationItem.text = navigation.name
            itemView.imageNavigationItem.setImageDrawable(ContextCompat.getDrawable(itemView.context, navigation.image))
            itemView.setOnClickListener {
                listener(navigation)
            }
            if (navigation.name == "Сообщения" && unreadedDialogs != 0) {
                itemView.messageBackground.visibility = View.VISIBLE
                itemView.messagesUnreaded.visibility = View.VISIBLE
                itemView.messagesUnreaded.text = unreadedDialogs.toString()
            } else {
                itemView.messageBackground.visibility = View.GONE
                itemView.messagesUnreaded.visibility = View.GONE
            }
        }
    }

    data class Navigation(val image: Int, val name: String, val unread: Int? = null)
}

