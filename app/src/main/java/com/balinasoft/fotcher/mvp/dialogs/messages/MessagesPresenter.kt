package com.balinasoft.fotcher.mvp.dialogs.messages

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.DIALOG
import com.balinasoft.fotcher.entity.messages.ContentItem
import com.balinasoft.fotcher.entity.messages.MessagesResponse
import com.balinasoft.fotcher.model.interactor.messages.MessagesInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import io.reactivex.Flowable
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class MessagesPresenter @Inject constructor(
        private val router: Router,
        private val messagesInteractor: MessagesInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<MessagesContract.View>() {

    fun onStart() {
        val dispose = messagesInteractor.getMessages(0)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .repeatWhen(
                        { t: Flowable<Any> ->
                            t.delay(10, TimeUnit.SECONDS)
                        }
                )
                .doOnError({ throwable ->
                    throwable.printStackTrace()
                })
                .subscribe({ t ->
                    viewState.showMessages(t!!.content!!.toMutableList(), t)
                    viewState.hideProgressLoading()
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedDialog(item: ContentItem) {
        router.navigateTo(DIALOG, item.user)
    }

    fun loadNextPage(currentPage: Int) {
        messagesInteractor.loadNextPage(currentPage).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe(
                        { t: MessagesResponse ->
                            viewState.showNext(t)
                        },
                        { t: Throwable? ->
                            Log.d(t!!.message.toString(), "d")
                        })
    }
}
