package com.balinasoft.fotcher.mvp.photostudio.map.itinerary

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.itinerary.ItineraryResponse
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_itinerary.*
import kotlinx.android.synthetic.main.fragment_map.*
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class ItineraryFragment : MvpAppCompatFragment(), ItineraryContract.View, OnMapReadyCallback {
    companion object {
        fun newInstance(data: Any) = ItineraryFragment().apply { arguments = Bundle().apply { putString("options", data as String) } }
        val INTERVAL = 1000.toLong() // In milliseconds
        val DISTANCE = 10.toFloat() // In meters
    }

    @InjectPresenter
    lateinit var itineraryPresenter: ItineraryPresenter

    @Inject
    lateinit var presenterProvider: Provider<ItineraryPresenter>

    @ProvidePresenter
    fun providePresenter(): ItineraryPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    lateinit var locationListeners: Array<LTRLocationListener>
    lateinit var locationManager: LocationManager
    var map: GoogleMap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_itinerary, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        itineraryPresenter.setItinerary()
        locationListeners = arrayOf(
                LTRLocationListener(LocationManager.GPS_PROVIDER, { item -> itineraryPresenter.onGetLocation(item, arguments!!["options"] as String) })
        )
        return view
    }

    class LTRLocationListener(provider: String, listener: (Location?) -> Unit) : android.location.LocationListener {
        val lastLocation = Location(provider)
        var lisener = listener
        override fun onLocationChanged(location: Location?) {
            lastLocation.set(location)
            lisener(location)
        }

        override fun onProviderDisabled(provider: String?) {
        }

        override fun onProviderEnabled(provider: String?) {
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_itinerary)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_itinerary.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).onBackPressed() }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    fun statusCheck() {
        val manager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
        }
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", { dialog, _ -> startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)) })
                .setNegativeButton("No", { dialog, _ -> dialog.cancel() })
        val alert = builder.create()
        alert.show()
    }

    override fun onMapReady(p0: GoogleMap?) {
        try {
            MapsInitializer.initialize(this.activity)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }

        map = p0
        p0!!.setOnMarkerClickListener { m: Marker ->
            itineraryPresenter.onClickedMarker(m)
            true
        }
        map!!.uiSettings.isZoomControlsEnabled = true

        val values = (arguments!!["options"] as String).split(" ")
        val endMarkerOptions = MarkerOptions()

                .position(LatLng(values[0].toDouble(), values[1].toDouble()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_blue))
        map!!.addMarker(endMarkerOptions)


        val builder = LatLngBounds.Builder()
        builder.include(endMarkerOptions.position)

        val bounds = builder.build()

        val padding = 2
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)

        map!!.animateCamera(cu)

        if (Build.VERSION.SDK_INT >= 23) {

            statusCheck()

            val read = activity!!.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)

            val listRequestPermission = ArrayList<String>()

            if (read != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION)
            }

            if (!listRequestPermission.isEmpty()) {
                val strRequestPermission = listRequestPermission.toTypedArray()
                requestPermissions(strRequestPermission, Constants.PERMISSION)
            } else {

                locationManager = activity!!.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                try {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, INTERVAL, DISTANCE, locationListeners[0])
                } catch (e: SecurityException) {
                    Log.e("", "Fail to request location update", e)
                } catch (e: IllegalArgumentException) {
                    Log.e("", "GPS provider does not exist", e)
                }
            }
        } else {
            // Pre-Marshmallow
        }
    }


    override fun showMarker(m: Marker) {
        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_blue))
    }

    override fun showDisableMarker(m: Marker) {
        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_gray))
    }

    @SuppressLint("SetTextI18n")
    override fun showInfo(data: ContentItem) {
        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))
        Glide.with(context!!)
                .load(data.smallAvatar)
                .apply(options)
                .into(imagePhotostudio)

        adressPhotostudio.text = data.name
        namePhotostudio.text = "от " + data.priceMin.toString()
        pricePhotostudio.text = data.address
    }

    override fun enableStudioInfo() {
        InfoPhotostudio.visibility = View.VISIBLE
    }

    override fun disableStudioInfo() {
        InfoPhotostudio.visibility = View.GONE

    }

    var flag = false
    override fun showItinerary(t: ItineraryResponse) {

        if (!flag) {
            flag = true
            val line = PolylineOptions()
            line.width(8f).color(context!!.resources.getColor(R.color.line))
            val latLngBuilder = LatLngBounds.Builder()
            val list = PolyUtil.decode(t.routes[0].overviewPolyline.points)

            for (i in 0 until (t.routes[0].overviewPolyline.points.length - 1)) {
                if (i == 0) {
                    val startMarkerOptions = MarkerOptions()
                            .position(list[0])
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user_label))
                    map!!.addMarker(startMarkerOptions)

                }
                if (i < list.size) {
                    line.add(list[i])
                    latLngBuilder.include(list[i])
                }
            }

            map!!.addPolyline(line)
            val size = resources.displayMetrics.widthPixels
            val latLngBounds = latLngBuilder.build()
            val track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 25)
            map!!.moveCamera(track)
        }
    }

}