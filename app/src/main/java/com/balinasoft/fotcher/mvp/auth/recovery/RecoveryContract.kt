package com.balinasoft.fotcher.mvp.auth.recovery

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


@StateStrategyType(OneExecutionStateStrategy::class)
interface RecoveryContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        fun showInputCodeRecovery()
        fun showInputNewPassword()
    }
}