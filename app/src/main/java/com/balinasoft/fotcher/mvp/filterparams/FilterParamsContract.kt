package com.balinasoft.fotcher.mvp.filterparams

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface FilterParamsContract {
    interface View : MvpView {
        fun hideKeyboard()
        //fun showPhotographers(list:MutableList<ContentItem?>, t: PhotographersResponse)
        fun showToast(text: String?)
    }
}