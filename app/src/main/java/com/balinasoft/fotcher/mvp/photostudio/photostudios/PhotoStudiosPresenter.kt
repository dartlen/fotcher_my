package com.balinasoft.fotcher.mvp.photostudio.photostudios

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERPHOTOSTUDIOS
import com.balinasoft.fotcher.Constants.PROFILEPHOTOSTUDIO
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.model.interactor.photostudios.PhotoStudiosInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class PhotoStudiosPresenter @Inject constructor(
        private val router: Router,
        private val photoStudiosInteractor: PhotoStudiosInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<PhotoStudiosContract.View>() {

    fun onClickedFilter() {
        viewState.hideKeyboard()
        router.navigateTo(FILTERPHOTOSTUDIOS, 0)
    }

    fun onStart(currentPage: Int) {
        if (currentPage == 0 || photoStudiosInteractor.getFlagReload())
            photoStudiosInteractor.onStart().subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe(
                            { t: PhotostudiosResponse? ->
                                if (!photoStudiosInteractor.getFlagReload()) {
                                    viewState.showPhotostudios(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                } else {
                                    photoStudiosInteractor.setFlagReload()
                                    viewState.showPhotostudiosClear(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                }
                            },
                            { _ ->
                                Log.d("", "")
                            })
    }

    fun onClickedLikePhotostudio(item: ContentItem) {
        val dispose = photoStudiosInteractor.likePhotostudio(item.id!!, item.favorite!!)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui()).subscribe(
                        { _ ->
                            Log.d("", "")
                        },
                        { t ->
                            viewState.showToast(t.message)
                            viewState.showValueLike(item)
                        }
                )
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedPhotostudio(item: ContentItem) {
        viewState.hideKeyboard()
        router.navigateTo(PROFILEPHOTOSTUDIO, item.id)
    }

    fun onInputedDataSearch(text: String) {

        val dispose = photoStudiosInteractor.onStartSearch(text)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe(
                        { t: PhotostudiosResponse? ->
                            photoStudiosInteractor.setFlagReload()
                            viewState.showPhotostudiosClear(t!!.content!!.toMutableList(), t)
                            viewState.hideProgressLoading()

                        },
                        { _ ->
                            Log.d("", "")
                        })
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedMap() {
        viewState.showMap()
    }

    fun loadNextPage(currentPage:Int, query:String){
        val dispose = photoStudiosInteractor.loadNextPage(currentPage, query)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe(
                        { t: PhotostudiosResponse? ->
                                viewState.showNextPage(t!!)
                        },
                        { _: Throwable? ->

                        })
        unsubscribeOnDestroy(dispose)
    }
}
