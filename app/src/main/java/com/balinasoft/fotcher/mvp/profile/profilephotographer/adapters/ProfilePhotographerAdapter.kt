package com.balinasoft.fotcher.mvp.profile.profilephotographer.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.ParamsEntity
import kotlinx.android.synthetic.main.item_filter.view.*

class ProfilePhotographerAdapter(val context: Context?, val listener: (ParamsEntity) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list[position], listener)
    }

    var list: ArrayList<ParamsEntity> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_filter, parent, false))

    }

    override fun getItemCount(): Int {
        return list.size
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ParamsEntity?, listener: (ParamsEntity) -> Unit) {
        itemView.textFilterItem.text = item!!.caption
        itemView.setOnClickListener { listener(item) }
    }
}