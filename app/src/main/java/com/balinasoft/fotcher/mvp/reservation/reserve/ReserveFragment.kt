package com.balinasoft.fotcher.mvp.reservation.reserve

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.reserve.pojo.FreeTimeResponse
import com.balinasoft.fotcher.entity.reserve.pojo.HoursItem
import com.balinasoft.fotcher.mvp.reservation.reserve.adapters.ReserveAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_reserve.*
import java.text.ParseException
import java.text.SimpleDateFormat
import javax.inject.Inject
import javax.inject.Provider

class ReserveFragment : MvpAppCompatFragment(), ReserveContract.View {
    companion object {
        fun newInstance(id: Any): ReserveFragment {
            val fragment = ReserveFragment()
            val args = Bundle()
            args.putInt("id", id as Int)
            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var reservePresenter: ReservePresenter

    @Inject
    lateinit var presenterProvider: Provider<ReservePresenter>

    @ProvidePresenter
    fun providePresenter(): ReservePresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_reserve, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_reserve)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_reserve.setNavigationOnClickListener(
                { _ ->hideKeyboard()
                    (activity as AppCompatActivity).onBackPressed()
                })
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerReserve.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerReserve.addItemDecoration(itemDecor)
        recyclerReserve.adapter = ReserveAdapter { Item: HoursItem? -> reservePresenter.onClickedTime(Item!!) }

        buttonReserve.setOnClickListener {
            reservePresenter.onClickedReserve(arguments!!.getInt("id"))
        }
    }

    override fun onStart() {
        super.onStart()

        reservePresenter.onStart(arguments!!.getInt("id"))
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SimpleDateFormat")
    override fun showTimes(t: FreeTimeResponse) {
        val listvalue3 = arrayListOf<String>()

        t.days!!.forEach { x ->
            val format = SimpleDateFormat("yyyy-MM-dd")
            val format2 = SimpleDateFormat("dd.MM.yyyy")
            try {
                val date = format.parse(x!!.date)
                listvalue3.add(format2.format(date))

            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }

        val spinnerArrayAdapter = object : ArrayAdapter<String>(
                context, R.layout.spinner_textview_main, listvalue3) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue3.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
        spinner_reserve.adapter = spinnerArrayAdapter
        spinner_reserve.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                reservePresenter.onItemSelected(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }

    }

    override fun showHours(data: List<HoursItem?>?) {
        (recyclerReserve.adapter as ReserveAdapter).list = data as ArrayList<HoursItem?>?
        (recyclerReserve.adapter as ReserveAdapter).notifyDataSetChanged()
    }
}