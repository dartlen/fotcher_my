package com.balinasoft.fotcher.mvp.model.filtermodels

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTEROPTIONSMODEL
import com.balinasoft.fotcher.Constants.FILTERPARAMSMODEL
import com.balinasoft.fotcher.Constants.MODELS
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filtermodels.FilterModelsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterModelsPresenter @Inject constructor(
        private val router: Router,
        private val filterModelsInteractor: FilterModelsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterModelsContract.View>() {
    var data: FilterModelsEntity? = null

    fun onClickedClean() {
        viewState.setDelault(data!!.params[0].options!!.size,
                data!!.params[1].options!!.size,
                data!!.params[2].options!!.size,
                data!!.params[3].options!!.size)
        for (i in 0 until data!!.params.size) {
            when {
                i < 4 -> for (k in 0 until data!!.params[i].options!!.size) {
                    if (data!!.params[i].options!![k]!!.check == true)
                        data!!.params[i].options!![k]!!.check = false
                }
                i == 4 -> for (j in 0 until (data!!.params[i].elements as ArrayList<*>).size) {
                    for (p in 0 until (data!!.params[i].elements as ArrayList<*>).size) {
                        if (data!!.params[i].elements[p]!!.borders != null)
                            data!!.params[i].elements[p]!!.borders = null
                        if (p > 5)
                            for (r in 0 until (data!!.params[i].elements[p]!!.options as ArrayList<*>).size)
                                if (data!!.params[i].elements[p]!!.options!![r]!!.check!!)
                                    data!!.params[i].elements[p]!!.options!![r]!!.check = false
                    }
                }
                else -> for (j in 0 until (data!!.params[i].options!! as ArrayList<*>).size) {
                    if (data!!.params[i].options!![j]!!.check!!)
                        data!!.params[i].options!![j]!!.check = false
                }
            }
        }
        filterModelsInteractor.saveFilters(data!!)
    }

    fun onStart() {

        val x = filterModelsInteractor.onStart().subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe(
                        { t1: FilterModelsEntity? ->
                            data = t1
                            viewState.showFilters(t1!!)

                        },
                        { t2: Throwable? ->
                            Log.d(t2!!.toString(), "")
                        }
                )
        unsubscribeOnDestroy(x)
    }

    fun onClickedParams(item: ParamsEntity) {
        saveFilters()
        if (item.caption == "Параметры")
            router.navigateTo(FILTEROPTIONSMODEL, item)
        else
            router.navigateTo(FILTERPARAMSMODEL, item)
    }

    fun onItemSelectedRate(position: Int) {
        for (i in 0 until data!!.params[0].options!!.size) {
            if (i == position)
                data!!.params[0].options!![i]!!.check = true

        }
    }

    fun onItemSelectedCity(position: Int) {
        for (i in 0 until data!!.params[2].options!!.size) {
            if (i == position)
                data!!.params[2].options!![i]!!.check = true

        }
    }

    fun onItemSelectedExp(position: Int) {
        for (i in 0 until data!!.params[3].options!!.size) {
            if (i == position)
                data!!.params[3].options!![i]!!.check = true

        }
    }

    fun onItemSelectedGender(position: Int) {
        for (i in 0 until data!!.params[1].options!!.size) {
            if (i == position)
                data!!.params[1].options!![i]!!.check = true

        }
    }


    private fun saveFilters() {
        filterModelsInteractor.saveFilters(data!!)
    }

    fun checkValueSpinner(number: Int): Int {
        for (i in 0 until data!!.params[number].options!!.size) {
            if (data!!.params[number].options!![i]!!.check!!) {
                return i
            }
        }
        return data!!.params[number].options!!.size
    }

    fun onClickedApply() {
        filterModelsInteractor.saveFilters(data!!)
        filterModelsInteractor.setFlagForReloadModels()
        router.backTo(MODELS)
    }

    fun onBackPressed() {
        router.navigateTo(MODELS, "")
    }
}
