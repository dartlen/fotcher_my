package com.balinasoft.fotcher.mvp.photostudio.favorites

import android.content.Context
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.mvp.photostudio.favorites.adapters.FavoritesAdapter
import com.balinasoft.fotcher.mvp.photostudio.photostudios.adapters.PhotostudiosAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.android.synthetic.main.fragment_photo_studios.*
import kotlinx.android.synthetic.main.nav_header_main.*
import javax.inject.Inject
import javax.inject.Provider

class FavoritesFragment : MvpAppCompatFragment(), FavoritesContract.View {

    @InjectPresenter
    lateinit var favoritesPresenter: FavoritesPresenter

    @Inject
    lateinit var presenterProvider: Provider<FavoritesPresenter>

    @ProvidePresenter
    fun providePresenter(): FavoritesPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 5
    var currentPage = PAGE_START
    var TOTAL_PAGES = 1

    var query = ""

    companion object {
        private const val PAGE_START = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        isLoad = false
        isLast = false

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_favorites)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_favorites.setNavigationOnClickListener{ _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)


        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        recyclerFavorites.layoutManager = linearLayoutManager
        recyclerFavorites.adapter = FavoritesAdapter(
                { item: ContentItem -> favoritesPresenter.onClickedPhotostudio(item) },
                { item: ContentItem -> favoritesPresenter.onClickedLikePhotostudio(item) })

        recyclerFavorites
                .addOnScrollListener(object : PaginationScrollListener(linearLayoutManager!!) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {
                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        } else
                            if ((recyclerFavorites.adapter as FavoritesAdapter).isLoadingAdded)
                                (recyclerFavorites.adapter as FavoritesAdapter).removeLoadingFooter()
                    }
                })
    }

    override fun onStart() {
        super.onStart()
        favoritesPresenter.onStart(currentPage)
    }

    override fun showPhotostudios(list: MutableList<ContentItem?>, t: PhotostudiosResponse) {
        TOTAL_PAGES = t.totalPages!!
        (recyclerFavorites.adapter as FavoritesAdapter).list = list
        (recyclerFavorites.adapter as FavoritesAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
            (recyclerPhotostudio.adapter as PhotostudiosAdapter).addLoadingFooter()
        else
            isLast = true
    }

    override fun removeFooter() {
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).removeLoadingFooter()
    }

    private fun loadNextPage() {
        if (currentPage != TOTAL_PAGES)
            if (!(recyclerFavorites.adapter as FavoritesAdapter).isLoadingAdded)
                (recyclerFavorites.adapter as FavoritesAdapter).addLoadingFooter()
        favoritesPresenter.loadNextPage(currentPage, query)
    }

    override fun hideProgressLoading() {
        favoritesProgress.visibility = View.GONE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showValueLike(item: ContentItem) {
        (recyclerFavorites.adapter as FavoritesAdapter).setStateLike(item)
    }

    override fun showPhotostudiosClear(list: MutableList<ContentItem?>, t: PhotostudiosResponse) {
        TOTAL_PAGES = t.totalPages!!
        currentPage = 0
        isLoad = false
        isLast = false
        (recyclerFavorites.adapter as FavoritesAdapter).list.clear()
        (recyclerFavorites.adapter as FavoritesAdapter).notifyDataSetChanged()
        (recyclerFavorites.adapter as FavoritesAdapter).list = list
        (recyclerFavorites.adapter as FavoritesAdapter).notifyDataSetChanged()
    }

    override fun showCurrentUser(url: String, name: String) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(currentUserImg)

        nameCurrentUser.text = name
    }

    override fun showEmail(email: String) {
        nameCurrentUser.text = email
    }

    override fun showNextPage(t: PhotostudiosResponse?) {
        (recyclerFavorites.adapter as FavoritesAdapter)
                .removeLoadingFooter()
        isLoad = false

        (recyclerFavorites.adapter as FavoritesAdapter)
                .addAll(t!!.content)

        if (currentPage != TOTAL_PAGES)
            (recyclerFavorites.adapter as FavoritesAdapter)
                    .addLoadingFooter()
        else
            isLast = true
    }
}