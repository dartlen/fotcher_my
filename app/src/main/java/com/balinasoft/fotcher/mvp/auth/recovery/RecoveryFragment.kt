package com.balinasoft.fotcher.mvp.auth.recovery

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import kotlinx.android.synthetic.main.fragment_recovery.*
import javax.inject.Inject
import javax.inject.Provider

class RecoveryFragment : MvpAppCompatFragment(), RecoveryContract.View {

    @InjectPresenter
    lateinit var recoveryPresenter: RecoveryPresenter
    @Inject
    lateinit var presenterProvider: Provider<RecoveryPresenter>

    @ProvidePresenter
    fun providePresenter(): RecoveryPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recovery, container, false)

        val toolbar = view.findViewById(R.id.my_toolbar) as Toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).onBackPressed() }

        return view
    }

    override fun onStart() {
        super.onStart()
        buttonRecovery.setOnClickListener { _ -> recoveryPresenter.onClickedRecovery(emailRecovery.text.toString()) }

        buttonSendCode.setOnClickListener { _ ->
            recoveryPresenter.onClickedSendCode(
                    codeRecovery.text.toString(), emailRecovery.text.toString())
        }

        buttonChangePassword.setOnClickListener { _ ->
            recoveryPresenter.onClickedChangePassword(
                    emailRecovery.text.toString(),
                    codeRecovery.text.toString(),
                    passwordRecovery1.text.toString(),
                    passwordRecovery2.text.toString()
            )
        }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showInputCodeRecovery() {
        textInputLayout5.visibility = View.VISIBLE
        buttonRecovery.visibility = View.GONE
        buttonSendCode.visibility = View.VISIBLE
        textView5.text = resources.getText(R.string.recoveryText)
    }

    override fun showInputNewPassword() {
        textInputLayout5.visibility = View.GONE
        textInputLayout6.visibility = View.VISIBLE
        textInputLayout7.visibility = View.VISIBLE
        buttonSendCode.visibility = View.GONE
        buttonChangePassword.visibility = View.VISIBLE
        passwordRecovery1.requestFocus()
    }


}