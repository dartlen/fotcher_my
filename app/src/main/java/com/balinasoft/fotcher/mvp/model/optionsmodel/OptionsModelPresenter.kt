package com.balinasoft.fotcher.mvp.model.optionsmodel

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.entity.profileoptions.pojo.ElementsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import com.balinasoft.fotcher.model.interactor.optionsmodel.OptionsModelInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import com.google.gson.internal.LinkedTreeMap
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class OptionsModelPresenter @Inject constructor(
        private val router: Router,
        private val optionsModelInteractor: OptionsModelInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<OptionsModelContract.View>() {
    lateinit var paramsEntity: MutableList<ElementsItem>
    @Suppress("UNCHECKED_CAST")
    fun onStart(params: MutableList<ElementsItem>) {
        params.forEach { paramsItem ->
            if (paramsItem.value != null)
                if (paramsItem.value is String)
                else if (paramsItem.value is LinkedTreeMap<*, *>)
                    (paramsItem.value as LinkedTreeMap<String, String>).forEach { t ->
                        paramsItem.options!!.forEach { t2: OptionsItem? ->
                            if (t.value == t2!!.name) {
                                t2.check = true
                            }
                        }
                    }
                else if (paramsItem.type == "INT_RANGE")
                    paramsItem.border = mutableListOf((paramsItem.value as Double).toInt())
                else
                    (paramsItem.value as ArrayList<LinkedTreeMap<String, String>>).forEach { t: LinkedTreeMap<String, String>? ->
                        paramsItem.options!!.forEach { t2: OptionsItem? ->
                            if (t!!["name"] == t2!!.name) {
                                t2.check = true
                            }
                        }
                    }
        }
        paramsEntity = params
    }

    fun onSaveEdite(paramsItem: ElementsItem) {
        //paramsEntity.forEachIndexed({count, x->if(x.name == paramsItem.name) paramsEntity[count]=paramsItem})
    }

    fun onSaveSpinner(paramsItem: ElementsItem) {
        paramsEntity.forEachIndexed({ count, x -> if (x.name == paramsItem.name) paramsEntity[count] = paramsItem })
    }

    fun onSaveSeek(paramsItem: ElementsItem) {
        paramsEntity.forEachIndexed { count, x ->
            if (x.name == paramsItem.name)
                paramsEntity[count] = paramsItem
        }

    }

    fun onSaveDate(paramsItem: ElementsItem) {
        paramsEntity.forEachIndexed({ count, x -> if (x.name == paramsItem.name) paramsEntity[count] = paramsItem })
    }

    fun onBack() {
        val d = optionsModelInteractor.saveOption(paramsEntity)
                .observeOn(appSchedulers.ui())
                .subscribe({ _ ->
                    router.backTo(Constants.PROFILEOPTION)
                }, { _ ->

                })
        unsubscribeOnDestroy(d)
    }
}