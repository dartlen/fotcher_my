package com.balinasoft.fotcher.mvp.gallery

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.entity.gallery.pojo.ContentItem
import com.balinasoft.fotcher.model.interactor.gallery.GalleryInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import java.io.File
import javax.inject.Inject

@InjectViewState
class GalleryPresenter @Inject constructor(
        private val router: Router,
        private val galleryInteractor: GalleryInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<GalleryContract.View>() {
    var state = false
    fun onStart(userId: Int) {
        galleryInteractor.onStart(id = userId, page = 0).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    viewState.showGallery(t!!)
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
    }

    fun onClickedAdd() {
        viewState.selectImage()
    }

    fun uploadImage(file: File) {
        val dispose = galleryInteractor.uploadImage(file)!!.subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe({ _ ->
                    viewState.reload()
                }, { _ ->
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedImage(item: ContentItem) {
        viewState.showImage(item)
    }

    fun setStateImage(value: Boolean) {
        state = value
    }

    fun onBack() {
        if (state) {
            state = false
            viewState.hideImage()
        } else {
            viewState.onBack()
        }
    }


}