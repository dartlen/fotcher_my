package com.balinasoft.fotcher.mvp.photostudio.favorites.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_photostudio.view.*

class FavoritesAdapter(private val listener: (ContentItem) -> Unit,
                       private val listener2: (ContentItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {

            ITEM -> (holder as ViewHolder).bind(list[position], listener, listener2)
            LOADING -> {
            }
        }
    }

    var list: MutableList<ContentItem?> = mutableListOf()

    private var retryPageLoad = false
    private var errorMsg: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ITEM -> return ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_photostudio, parent, false))
            LOADING -> {
                val v2 = inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = LoadingVH(v2)
            }
        }
        return viewHolder!!

    }

    override fun getItemCount(): Int {
        return list.size
    }

    var isLoadingAdded = false

    val isEmpty: Boolean
        get() = itemCount == 0

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size - 1 && isLoadingAdded) LOADING else ITEM
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    fun add(r: ContentItem) {
        list.add(r)
        notifyItemInserted(list.size - 1)
    }

    fun addAll(moveResults: List<ContentItem?>?) {
        for (result in moveResults!!) {
            add(result!!)
        }
    }

    fun remove(r: ContentItem?) {
        val position = list.indexOf(r)
        if (position > -1) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(ContentItem())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = list.size - 1
        val result = getItem(position)

        if (result != null) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): ContentItem? {
        return list[position]
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(list.size - 1)

        if (errorMsg != null) this.errorMsg = errorMsg
    }

    fun setStateLike(item: ContentItem) {
        list.mapIndexed { index, contentItem ->
            {
                if (item.equals(contentItem)) {
                    list[index]!!.favorite = item.favorite
                    notifyItemChanged(index)
                }
            }
        }
    }

    /*
   View Holders
   _________________________________________________________________________________________________
    */

    class LoadingVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        private const val ITEM = 0
        private const val LOADING = 1
    }

}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    @Suppress("UNCHECKED_CAST")
    fun bind(item: ContentItem?, listener: (ContentItem) -> Unit, listener2: (ContentItem) -> Unit) {

        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))
        Glide.with(itemView.context)
                .load(item!!.bigAvatar)
                .apply(options)
                .into(itemView.imageView5)

        itemView.namePhotostudio.text = item.name
        itemView.adressPhotostudio.text = item.address
        itemView.textView.text = item.priceMin.toString() + " \u20BD/час"
        if (item.favorite == true) {
            itemView.heartWithout.visibility = View.INVISIBLE
            itemView.heart.visibility = View.VISIBLE
        } else {
            itemView.heartWithout.visibility = View.VISIBLE
            itemView.heart.visibility = View.INVISIBLE
        }

        if (item.rating != null) {
            val value = (item.rating as Map<String, Double>)["value"]!!.toInt()
            val count = item.rating["count"]!!.toInt()
            if (count == 1)
                itemView.feedbak.text = count.toString() + " отзыв"
            if (count in 2..4)
                itemView.feedbak.text = count.toString() + " отзыва"
            if (count > 5)
                itemView.feedbak.text = count.toString() + " отзывов"

            itemView.library_tinted_wide_ratingbar.rating = value.toFloat()
        } else {
            itemView.library_tinted_wide_ratingbar.rating = 0.0f
            itemView.feedbak.text = "0 отзывов"
        }

        if (item.favorite != null)
            if (item.favorite!!) {
                itemView.heart.visibility = View.INVISIBLE
                itemView.heart.visibility = View.VISIBLE
            } else {
                itemView.heart.visibility = View.VISIBLE
                itemView.heart.visibility = View.INVISIBLE
            }


        itemView.heartWithout.setOnClickListener {
            item.favorite = false
            listener2(item)

            itemView.heart.visibility = View.VISIBLE
            itemView.heartWithout.visibility = View.INVISIBLE
        }
        itemView.heart.setOnClickListener {
            item.favorite = true
            listener2(item)

            itemView.heart.visibility = View.INVISIBLE
            itemView.heartWithout.visibility = View.VISIBLE
        }

        itemView.imageView5.setOnClickListener {
            listener(item)
        }
    }
}

