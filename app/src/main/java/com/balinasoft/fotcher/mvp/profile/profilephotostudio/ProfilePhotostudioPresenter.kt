package com.balinasoft.fotcher.mvp.profile.profilephotostudio

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.COMMENTS
import com.balinasoft.fotcher.Constants.INFOPHOTOSTUDIO
import com.balinasoft.fotcher.Constants.ITINERARY
import com.balinasoft.fotcher.Constants.RESERVE
import com.balinasoft.fotcher.entity.photostudio.pojo.ParamsItem
import com.balinasoft.fotcher.model.interactor.profilephotostudio.ProfilePhotostudioInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ProfilePhotostudioPresenter @Inject constructor(
        private val router: Router,
        private val profilePhotostudioInteractor: ProfilePhotostudioInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ProfilePhotostudioContract.View>() {

    var photostudioId: Int = 0
    var photostudioLocation = ""
    fun onStart(id: Int) {
        val dispose = profilePhotostudioInteractor.onStart(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    photostudioId = t!!.id!!
                    photostudioLocation = t.lat.toString() + " " + t.lng
                    viewState.showPhotostudio(t)
                    viewState.hideProgress()
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedParams(paramsItem: ParamsItem) {
        router.navigateTo(INFOPHOTOSTUDIO, paramsItem)
    }

    fun onClickedLikePhotostudio(id: Int, likeState: Boolean) {
        val dispose = profilePhotostudioInteractor.likePhotostudio(id, likeState)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui()).subscribe(
                        { _ ->
                            Log.d("", "")
                        },
                        { t ->
                            viewState.showToast(t.message)
                            //viewState.showValueLike(item)
                        }
                )
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedComments() {
        router.navigateTo(COMMENTS, photostudioId.toString())
    }

    fun onClickedReserve(id: Int) {
        router.navigateTo(RESERVE, id)
    }

    fun onClickedAdress() {
        router.navigateTo(ITINERARY, photostudioLocation)
    }
}
