package com.balinasoft.fotcher.mvp.model.optionsmodel

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.profileoptions.pojo.ElementsItem

@StateStrategyType(SkipStrategy::class)
interface OptionsModelContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showParams(paramsItem: MutableList<ElementsItem>)
    }
}