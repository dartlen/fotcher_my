package com.balinasoft.fotcher.mvp.photostudio.photostudios

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse

@StateStrategyType(SkipStrategy::class)
interface PhotoStudiosContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showPhotostudios(list: MutableList<ContentItem?>, t: PhotostudiosResponse)
        @StateStrategyType(SkipStrategy::class)
        fun hideProgressLoading()

        fun loadFirstPage()
        fun showToast(text: String?)
        fun showValueLike(item: ContentItem)
        fun removeFooter()
        fun showPhotostudiosClear(list: MutableList<ContentItem?>, t: PhotostudiosResponse)
        fun showCurrentUser(url: String, name: String)
        fun showEmail(email: String)
        fun showMap()

        fun showNextPage(t:PhotostudiosResponse)
    }
}