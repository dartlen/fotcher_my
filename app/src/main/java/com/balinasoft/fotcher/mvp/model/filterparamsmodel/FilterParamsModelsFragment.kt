package com.balinasoft.fotcher.mvp.model.filterparamsmodel

import android.content.Context
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.mvp.model.filterparamsmodel.adapters.FilterParamsModelsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_filterparamsmodels.*
import javax.inject.Inject
import javax.inject.Provider

class FilterParamsModelsFragment : MvpAppCompatFragment(), FilterParamsModelContract.View {
    companion object {
        fun newInstance(params: Any): FilterParamsModelsFragment {
            return FilterParamsModelsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("options", (params as ParamsEntity))
                }
            }
        }
    }

    @InjectPresenter
    lateinit var filtesParamsModelPresenter: FilterParamsModelsPresenter

    @Inject
    lateinit var presenterProvider: Provider<FilterParamsModelsPresenter>

    @ProvidePresenter
    fun providePresenter(): FilterParamsModelsPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_filterparamsmodels, container, false)
        setHasOptionsMenu(true)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_filterparamsmodels.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_filterparamsmodels)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)



        (activity as AppCompatActivity).supportActionBar?.title =
                (arguments!!["options"] as ParamsEntity).caption


        toolbar_filterparamsmodels.setNavigationOnClickListener { _ ->
            filtesParamsModelPresenter.onClickedBack()
        }

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        val itemDecor = DividerItemDecoration(activity, ClipDrawable.HORIZONTAL)
        recyclerFilterParamsModels.addItemDecoration(itemDecor)
        recyclerFilterParamsModels.layoutManager = linearLayoutManager
        recyclerFilterParamsModels.adapter = FilterParamsModelsAdapter { item -> filtesParamsModelPresenter.onClickedItem(item) }


        filtesParamsModelPresenter.setParams(arguments!!["options"] as ParamsEntity)
        (recyclerFilterParamsModels.adapter as FilterParamsModelsAdapter).list = (arguments!!["options"] as ParamsEntity).options
        (recyclerFilterParamsModels.adapter as FilterParamsModelsAdapter).notifyDataSetChanged()

    }

    override fun onStart() {
        super.onStart()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}