package com.balinasoft.fotcher.mvp.profile.profileoptions

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.INFOPROFILEOPTION
import com.balinasoft.fotcher.Constants.OPTIONSMODEL
import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.model.interactor.profileoptions.ProfileOptionsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import com.google.gson.internal.LinkedTreeMap
import ru.terrakok.cicerone.Router
import java.io.File
import javax.inject.Inject

@InjectViewState
class ProfileOptionsPresenter @Inject constructor(
        private val router: Router,
        private val profileOptionsInteractor: ProfileOptionsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ProfileOptionsContract.View>() {
    lateinit var paramsEntity: MutableList<ParamsItem>
    fun onStart() {
        profileOptionsInteractor.onStart()!!.subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    t!!.params!!.forEach {
                        when {
                            it.type == "STRING" -> {

                            }
                            it.type == "INT_RANGE" -> {
                                if (it.value != null) {
                                    it.border = mutableListOf((it.value as Double).toInt())
                                    it.value = null
                                }
                            }
                            it.type == "SELECT" -> {
                                if (it.value != null)
                                    it.options!!.forEach { t: OptionsItem? ->
                                        if (t!!.name == (it.value as LinkedTreeMap<*, *>)["name"] as String?)
                                            t.check = true
                                    }
                            }
                            it.type == "GROUP" -> {
                                it.elements!!.forEach {
                                    when {
                                        it!!.type == "INT_RANGE" -> {
                                            if (it.value != null)
                                                it.border = arrayListOf((it.value as Double).toInt())
                                            it.value = null
                                        }
                                        it.type == "SELECT" -> {
                                            if (it.value != null)
                                                it.options!!.forEach { t ->
                                                    if ((it.value as LinkedTreeMap<*, *>)["name"] == t!!.name) {
                                                        t.check = true
                                                    }
                                                }
                                            it.value = null
                                        }
                                        it.type == "DATE" -> {

                                        }
                                    }
                                }
                            }
                            it.type == "MULTI_SELECT" -> {
                                if (it.value != null)
                                    if ((it.value is ArrayList<*>))
                                        if ((it.value as ArrayList<*>).size > 0) {
                                            it.options!!.forEach { t ->
                                                (it.value as ArrayList<*>).forEachIndexed { count, item ->
                                                    if ((item as LinkedTreeMap<*, *>)["name"] == t!!.name) {
                                                        t.check = true
                                                    }
                                                }
                                            }
                                        }
                                it.value = null
                            }
                        }
                    }
                    paramsEntity = t.params as MutableList<ParamsItem>
                    viewState.showParams(t.params as List<ParamsItem>)
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
        viewState.showAvatar(profileOptionsInteractor.getAvatar())
    }

    fun onClickedParams(paramsItem: ParamsItem, list: ArrayList<ParamsItem>) {
        paramsEntity.forEachIndexed({ count, x ->
            if (count < list.size)
                if (x.name == list[count].name) paramsEntity[count] = list[count]
        })
        profileOptionsInteractor.saveOptions(paramsEntity)
        if (paramsItem.caption.equals("Параметры"))
            router.navigateTo(OPTIONSMODEL, paramsItem)
        else
            router.navigateTo(INFOPROFILEOPTION, paramsItem)
    }

    fun uploadImage(file: File) {
        val dispose = profileOptionsInteractor.uploadImage(file)!!.subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    profileOptionsInteractor.setAvatar(t!!.bigAvatar!!)
                    viewState.showAvatar(t.bigAvatar!!)
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onSaveEdite(paramsItem: ParamsItem) {
        paramsEntity.forEachIndexed({ count, x -> if (x.name == paramsItem.name) paramsEntity[count] = paramsItem })
    }

    fun onSaveSpinner(paramsItem: ParamsItem) {
        paramsEntity.forEachIndexed({ count, x -> if (x.name == paramsItem.name) paramsEntity[count] = paramsItem })
    }

    fun onSaveSeek(paramsItem: ParamsItem) {
        paramsEntity.forEachIndexed({ count, x -> if (x.name == paramsItem.name) paramsEntity[count] = paramsItem })
    }

    fun onBack() {
        viewState.hideKeyboard()
        val dispose = profileOptionsInteractor.onBack(paramsEntity).subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe({ _ ->
                    Log.d("", "")
                    if (paramsEntity[0].value != null && paramsEntity[1].value != null)
                        profileOptionsInteractor.setUserName(paramsEntity[0].value.toString() + " " + paramsEntity[1].value.toString())
                    viewState.onBack()
                }, { _ ->
                    Log.d("", "")
                })

        unsubscribeOnDestroy(dispose)
    }
}