package com.balinasoft.fotcher.mvp.reservation.reserve

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.RESERVATIONS
import com.balinasoft.fotcher.entity.reserve.pojo.FreeTimeResponse
import com.balinasoft.fotcher.entity.reserve.pojo.HoursItem
import com.balinasoft.fotcher.entity.reserve.pojo.response.PartsItem
import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.model.interactor.reserve.ReserveInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ReservePresenter @Inject constructor(
        private val router: Router,
        private val reserveInteractor: ReserveInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ReserveContract.View>() {
    var data: FreeTimeResponse? = null
    var index: Int? = null
    fun onStart(id: Int) {
        val dispose = reserveInteractor.onStart(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    data = t.body()
                    viewState.showTimes(t.body()!!)
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }

    fun onItemSelected(position: Int) {
        data!!.days!!.forEachIndexed { index, daysItem ->
            if (index == position) {
                viewState.showHours(daysItem!!.hours)
                this.index = index
            }
        }
    }

    fun onClickedTime(item: HoursItem) {
        data!!.days!!.forEachIndexed { index, daysItem ->
            if (this.index == index)
                daysItem!!.hours!!.forEachIndexed { _, hoursItem ->
                    if (item.hour == hoursItem!!.hour && item.price == hoursItem.price) {
                        hoursItem.check = !hoursItem.check!!
                        return@forEachIndexed
                    }
                }
        }
    }

    fun onClickedReserve(id: Int) {
        val request = ReserveStudio(mutableListOf(), id)
        data!!.days!!.forEachIndexed { index, daysItem ->
            if (this.index == index)
                daysItem!!.hours!!.forEachIndexed { _, hoursItem ->
                    if (hoursItem!!.check == true) {
                        request.parts!!.add(PartsItem(date = daysItem.date, hour = hoursItem.hour))
                        return@forEachIndexed
                    }
                }
        }
        val dispose = reserveInteractor.onReserve(request).subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe({
                    router.navigateTo(RESERVATIONS, id)
                }, {

                })
        unsubscribeOnDestroy(dispose)
    }
}
