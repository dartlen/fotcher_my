package com.balinasoft.fotcher.mvp.photographer.infophotographer

import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.ParamsEntity
import com.balinasoft.fotcher.mvp.photographer.infophotographer.adapters.InfoPhotographerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_info.*
import javax.inject.Inject
import javax.inject.Provider

class InfoPhotographerFragment : MvpAppCompatFragment(), InfoPhotographerContract.View {
    companion object {
        fun newInstance(params: Any): InfoPhotographerFragment {
            return InfoPhotographerFragment().apply {
                arguments = Bundle().apply { putParcelable("options", params as ParamsEntity) }
            }
        }
    }

    @InjectPresenter
    lateinit var infoPhotographerPresenter: InfoPhotographerPresenter

    @Inject
    lateinit var presenterProvider: Provider<InfoPhotographerPresenter>

    @ProvidePresenter
    fun providePresenter(): InfoPhotographerPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_info, container, false)
        setHasOptionsMenu(true)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_info.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_info)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        (activity as AppCompatActivity).supportActionBar?.title =
                (arguments!!["options"] as ParamsEntity).caption

        toolbar_info.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerInfo.addItemDecoration(itemDecor)
        recyclerInfo.layoutManager = linearLayoutManager

        recyclerInfo.adapter = InfoPhotographerAdapter()
        (recyclerInfo.adapter as InfoPhotographerAdapter).list = (arguments!!["options"] as ParamsEntity).value
        (recyclerInfo.adapter as InfoPhotographerAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}