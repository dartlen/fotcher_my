package com.balinasoft.fotcher.mvp.model.optionsmodel

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.profileoptions.pojo.ElementsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.mvp.model.optionsmodel.adapters.OptionsModelAdapter
import kotlinx.android.synthetic.main.fragment_optionsmodel.*
import javax.inject.Inject
import javax.inject.Provider

class OptionsModelFragment : MvpAppCompatFragment(), OptionsModelContract.View {
    companion object {
        fun newInstance(params: Any) = OptionsModelFragment()
                .apply {
                    arguments = Bundle()
                            .apply { putParcelable("options", params as ParamsItem) }
                }
    }

    @InjectPresenter
    lateinit var optionsModelPresenter: OptionsModelPresenter

    @Inject
    lateinit var presenterProvider: Provider<OptionsModelPresenter>

    @ProvidePresenter
    fun providePresenter(): OptionsModelPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        return inflater.inflate(R.layout.fragment_optionsmodel, container,
                false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager2 = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerOptionsModel.layoutManager = linearLayoutManager2
        recyclerOptionsModel.adapter = OptionsModelAdapter({ item -> optionsModelPresenter.onSaveEdite(item) },
                { item -> optionsModelPresenter.onSaveSpinner(item) },
                { item -> optionsModelPresenter.onSaveSeek(item) },
                { item -> optionsModelPresenter.onSaveDate(item) })
    }
    @Suppress("UNCHECKED_CAST")
    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbarOptionsModel)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbarOptionsModel.setNavigationOnClickListener(
                { _ -> optionsModelPresenter.onBack() })

        optionsModelPresenter.onStart((arguments!!["options"] as ParamsItem).elements as MutableList<ElementsItem>)
        showParams((arguments!!["options"] as ParamsItem).elements as MutableList<ElementsItem>)
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showParams(paramsItem: MutableList<ElementsItem>) {
        (recyclerOptionsModel.adapter as OptionsModelAdapter).list = ArrayList(paramsItem.filter { t -> t.type != "MULTI_SELECT" }.filter { t -> t.type != "GROUP" })
        (recyclerOptionsModel.adapter as OptionsModelAdapter).notifyDataSetChanged()
    }

}