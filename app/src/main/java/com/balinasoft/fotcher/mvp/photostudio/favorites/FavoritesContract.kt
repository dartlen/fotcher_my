package com.balinasoft.fotcher.mvp.photostudio.favorites

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse

@StateStrategyType(SkipStrategy::class)
interface FavoritesContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showPhotostudios(list: MutableList<ContentItem?>, t: PhotostudiosResponse)
        @StateStrategyType(SkipStrategy::class)
        fun hideProgressLoading()
        @StateStrategyType(SkipStrategy::class)
        fun loadFirstPage()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)
        @StateStrategyType(SkipStrategy::class)
        fun showValueLike(item: ContentItem)
        @StateStrategyType(SkipStrategy::class)
        fun removeFooter()
        @StateStrategyType(SkipStrategy::class)
        fun showPhotostudiosClear(list: MutableList<ContentItem?>, t: PhotostudiosResponse)
        @StateStrategyType(SkipStrategy::class)
        fun showCurrentUser(url: String, name: String)
        @StateStrategyType(SkipStrategy::class)
        fun showEmail(email: String)
        @StateStrategyType(SkipStrategy::class)
        fun showNextPage( t: PhotostudiosResponse?)
    }
}