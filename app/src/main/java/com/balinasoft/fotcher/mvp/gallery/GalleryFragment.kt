package com.balinasoft.fotcher.mvp.gallery

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.Constants.SELECTIMAGE
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.FileUtils
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.commons.view.ItemOffsetDecoration
import com.balinasoft.fotcher.entity.gallery.pojo.ContentItem
import com.balinasoft.fotcher.entity.gallery.pojo.GalleryResponse
import com.balinasoft.fotcher.mvp.gallery.adapter.GalleryAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_gallery.*
import javax.inject.Inject
import javax.inject.Provider
import android.content.pm.PackageManager



class GalleryFragment : MvpAppCompatFragment(), GalleryContract.View {
    @Suppress("UNCHECKED_CAST")
    companion object {
        fun newInstance(idUser: Any): GalleryFragment {
            val fragment = GalleryFragment()
            val args = Bundle()
            args.putIntegerArrayList("idUser", idUser as ArrayList<Int>)
            fragment.arguments = args

            return fragment
        }

        private const val PAGE_START = 0
    }

    @InjectPresenter
    lateinit var galleryPresenter: GalleryPresenter

    @Inject
    lateinit var presenterProvider: Provider<GalleryPresenter>

    @ProvidePresenter
    fun providePresenter(): GalleryPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    lateinit var gridLayoutManager: GridLayoutManager

    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 5
    var currentPage = PAGE_START
    var TOTAL_PAGES = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_gallery, container, false)
        setHasOptionsMenu(true)
        isLoad = false
        isLast = false

        return view
    }

    override fun onBack() {
        (activity as AppCompatActivity).onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_gallery)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar_gallery.setNavigationOnClickListener(
                { _ -> galleryPresenter.onBack() })
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        gridLayoutManager = GridLayoutManager(context, 3)
        recyclerGallery.layoutManager = gridLayoutManager
        recyclerGallery.adapter = GalleryAdapter { Item: ContentItem -> galleryPresenter.onClickedImage(Item) }

        val itemDecoration = ItemOffsetDecoration(context!!, R.dimen.item_offset)
        recyclerGallery.addItemDecoration(itemDecoration)

        recyclerGallery
                .addOnScrollListener(object : PaginationScrollListener(gridLayoutManager) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {
                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        }
                    }
                })
    }

    override fun onStart() {
        super.onStart()
        galleryPresenter.onStart(arguments!!.getIntegerArrayList("idUser")[0])
        galleryPresenter.setStateImage(false)
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val x = arguments!!.getIntegerArrayList("idUser")[1]
        if (x == 1) {
            inflater!!.inflate(R.menu.menu_gallery, menu)
            super.onCreateOptionsMenu(menu, inflater)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_add -> {
                galleryPresenter.onClickedAdd()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
            ;
        else
            isLast = true
    }

    override fun removeFooter() {
        //(recyclerGallery.adapter as GalleryAdapter).removeLoadingFooter()
    }

    private fun loadNextPage() {
        //if (currentPage != TOTAL_PAGES)
        /* if(!(recyclerGallery.adapter as GalleryAdapter).isLoadingAdded)
             (recyclerGallery.adapter as GalleryAdapter).addLoadingFooter()*/

        /*galleryInteractor.loadNextPage(currentPage)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe(
                        {t: PhotographersResponse? ->
                            (recyclerGallery.adapter as GalleryAdapter)
                                    .removeLoadingFooter()
                            isLoad = false

                            (recyclerGallery.adapter as GalleryAdapter)
                                    .addAll(t!!.content)

                            if (currentPage != TOTAL_PAGES)
                                (recyclerPGallery.adapter as GalleryAdapter)
                                        .addLoadingFooter()
                            else
                                isLast = true
                        },
                        {t: Throwable? ->

                        })*/
    }

    override fun hideProgressLoading() {
        galleryProgress.visibility = View.GONE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNCHECKED_CAST")
    override fun showGallery(gallry: GalleryResponse) {
        galleryProgress.visibility = View.GONE
        TOTAL_PAGES = gallry.totalPages!!
        (recyclerGallery.adapter as GalleryAdapter).list = gallry.content as ArrayList<ContentItem>
        (recyclerGallery.adapter as GalleryAdapter).notifyDataSetChanged()
    }

    override fun selectImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"

        val packageManager = context!!.packageManager
        if (packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY).size>0) {
            startActivityForResult(intent, SELECTIMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == SELECTIMAGE) {

            if (resultCode == Activity.RESULT_OK) {
                val file = FileUtils.getFile(context, data!!.data)
                galleryPresenter.uploadImage(file)
            }
        }
    }

    override fun reload() {
        onStart()
    }

    override fun showImage(item: ContentItem) {
        Thread {
            val theBitmap: Bitmap? = Glide.with(activity!!)
                    .asBitmap()
                    .load(item.big)
                    .submit()
                    .get()

            val d = BitmapDrawable(resources, theBitmap)
            galleryPresenter.setStateImage(true)
            activity!!.runOnUiThread {
                iv_photo.visibility = View.VISIBLE
                back.visibility = View.VISIBLE
                iv_photo.setImageDrawable(d)
            }

        }.start()
    }

    override fun hideImage() {
        back.visibility = View.GONE
        iv_photo.visibility = View.GONE
    }
}