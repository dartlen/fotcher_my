package com.balinasoft.fotcher.mvp.reservation.reservations

import android.content.Context
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.reservations.ContentItem
import com.balinasoft.fotcher.entity.reservations.ReservationsResponse
import com.balinasoft.fotcher.mvp.reservation.reservations.adapters.ReservationsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_reservations.*
import javax.inject.Inject
import javax.inject.Provider


class ReservationsFragment : MvpAppCompatFragment(), ReservationsContract.View {
    companion object {
        fun newInstance(id: Any): ReservationsFragment {
            val fragment = ReservationsFragment()
            val args = Bundle()
            args.putInt("id", id as Int)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var reservationsPresenter: ReservationsPresenter

    @Inject
    lateinit var presenterProvider: Provider<ReservationsPresenter>

    @ProvidePresenter
    fun providePresenter(): ReservationsPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_reservations, container, false)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_reservations)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_reservations.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val itemDecor = DividerItemDecoration(activity, ClipDrawable.HORIZONTAL)
        recyclerReservations.addItemDecoration(itemDecor)
        recyclerReservations.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerReservations.adapter = ReservationsAdapter()

        swipeRefreshLayout.setOnRefreshListener({
            reservationsPresenter.getReservations()
        })
    }

    override fun onStart() {
        super.onStart()
        reservationsPresenter.getReservations()
    }


    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNCHECKED_CAST")
    override fun showReservations(data: ReservationsResponse) {
        swipeRefreshLayout.isRefreshing = false
        (recyclerReservations.adapter as ReservationsAdapter).list = data.content as ArrayList<ContentItem>
        (recyclerReservations.adapter as ReservationsAdapter).notifyDataSetChanged()
        reservationsProgress.visibility = View.GONE
        recyclerReservations.visibility = View.VISIBLE
    }
}