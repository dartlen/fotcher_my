package com.balinasoft.fotcher.mvp.profile.profilephotostudio

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.MetricAffectingSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.photostudio.pojo.ParamsItem
import com.balinasoft.fotcher.entity.photostudio.pojo.PhotostudioResponse
import com.balinasoft.fotcher.mvp.profile.profilephotostudio.adapters.ProfilePhotostudioAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile_photostudio.*
import javax.inject.Inject
import javax.inject.Provider

class ProfilePhotostudioFragment : MvpAppCompatFragment(), ProfilePhotostudioContract.View {
    companion object {
        fun newInstance(id: Any): ProfilePhotostudioFragment {
            val fragment = ProfilePhotostudioFragment()
            val args = Bundle()
            args.putInt("id", id as Int)
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var presenterProvider: Provider<ProfilePhotostudioPresenter>

    @InjectPresenter
    lateinit var profilePhotostudioPresenter: ProfilePhotostudioPresenter

    @ProvidePresenter
    fun providePresenter():ProfilePhotostudioPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_profile_photostudio, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_profile_photostudio)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_profile_photostudio.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).onBackPressed() }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerProfilePhotostudio.layoutManager = linearLayoutManager
        recyclerProfilePhotostudio.adapter = ProfilePhotostudioAdapter(context
        ) { Item: ParamsItem -> profilePhotostudioPresenter.onClickedParams(Item) }

        feedback.setOnClickListener {
            profilePhotostudioPresenter.onClickedComments()
        }

        buttonReserve.setOnClickListener {
            profilePhotostudioPresenter.onClickedReserve(arguments!!.getInt("id"))
        }

        address.setOnClickListener {
            profilePhotostudioPresenter.onClickedAdress()
        }

        frame.setOnClickListener {

        }

    }

    override fun onStart() {
        super.onStart()
        profilePhotostudioPresenter.onStart(arguments!!.getInt("id"))
        heartFavorite.setOnClickListener {
            heartFavorite.visibility = View.GONE
            heartNull.visibility = View.VISIBLE
            profilePhotostudioPresenter.onClickedLikePhotostudio(arguments!!.getInt("id"), false)
        }
        heartNull.setOnClickListener {
            heartFavorite.visibility = View.VISIBLE
            heartNull.visibility = View.GONE
            profilePhotostudioPresenter.onClickedLikePhotostudio(arguments!!.getInt("id"), true)
        }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SetTextI18n")
    @Suppress("UNCHECKED_CAST")
    override fun showPhotostudio(photostudio: PhotostudioResponse) {
        try {
            val listforrecycler = arrayListOf<ParamsItem>()
            for ((count, i) in photostudio.params!!.withIndex()) {
                if (count >= 4)
                    listforrecycler.add(i!!)
            }
            (recyclerProfilePhotostudio.adapter as ProfilePhotostudioAdapter).list = listforrecycler
            (recyclerProfilePhotostudio.adapter as ProfilePhotostudioAdapter).notifyDataSetChanged()
            val options = RequestOptions()
            options.centerCrop()
            Glide.with(context!!)
                    .load(photostudio.bigAvatar)
                    .apply(options)
                    .into(image)
            address.text = photostudio.address
            name.text = photostudio.name
            price.text = "от " + photostudio.priceMin.toString()

            one.text = photostudio.params[0]!!.value.toString().substring(0, photostudio.params[0]!!.value.toString().length - 2) + " м"


            val string = SpannableString(photostudio.params[0]!!.value.toString().substring(0, photostudio.params[0]!!.value.toString().length - 2) + " м" + "2")
            string.setSpan(AntiRelativeSizeSpan((0.5f)), string.length - 1, string.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            one.text = string

            val string2 = SpannableString(photostudio.params[1]!!.value.toString().substring(0, photostudio.params[0]!!.value.toString().length - 2) + " м" + "2")
            string2.setSpan(AntiRelativeSizeSpan((0.5f)), string2.length - 1, string2.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            two.text = string2

            three.text = photostudio.params[2]!!.value.toString().substring(0, photostudio.params[0]!!.value.toString().length - 2) + " cм"



            if (photostudio.rating != null) {
                val value = (photostudio.rating as Map<String, Double>)["value"]!!
                val count = photostudio.rating["count"]!!.toInt()
                if (count == 1)
                    feedback.text = count.toString() + " отзыв"
                if (count in 2..4)
                    feedback.text = count.toString() + " отзыва"
                if (count >= 5)
                    feedback.text = count.toString() + " отзывов"


                library_tinted_wide_ratingbar.rating = value.toFloat()
            } else {
                library_tinted_wide_ratingbar.rating = 0.0f
                feedback.text = "0 отзывов"
            }

            expand_text_view.text = photostudio.description.toString()
            expand_collapse.setImageDrawable(resources.getDrawable(R.drawable.ic_ar_down))
            expand_text_view.setOnExpandStateChangeListener { _, isExpanded ->
                expand_collapse.setImageDrawable(if (isExpanded) resources.getDrawable(R.drawable.ic_ar_up) else resources.getDrawable(R.drawable.ic_ar_down))
            }
        } catch (e: Exception) {

        }

        if (photostudio.favorite!!) {
            heartFavorite.visibility = View.VISIBLE
            heartNull.visibility = View.GONE
        } else {
            heartFavorite.visibility = View.GONE
            heartNull.visibility = View.VISIBLE
        }
    }

    override fun hideProgress() {
        view_profile_photostudio.visibility = View.VISIBLE
        photostudioProfileProgress.visibility = View.INVISIBLE
    }

}

class AntiRelativeSizeSpan(val sizeChange: Float) : MetricAffectingSpan() {

    override fun updateDrawState(ds: TextPaint) {
        updateAnyState(ds)
    }

    override fun updateMeasureState(ds: TextPaint) {
        updateAnyState(ds)
    }

    private fun updateAnyState(ds: TextPaint) {
        val bounds = Rect()
        ds.getTextBounds("1A", 0, 2, bounds)
        var shift = bounds.top - bounds.bottom
        ds.textSize = ds.textSize * sizeChange
        ds.getTextBounds("1A", 0, 2, bounds)
        shift += bounds.bottom - bounds.top
        ds.baselineShift += shift
    }
}