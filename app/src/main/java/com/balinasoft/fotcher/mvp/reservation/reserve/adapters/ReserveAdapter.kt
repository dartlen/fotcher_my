package com.balinasoft.fotcher.mvp.reservation.reserve.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.reserve.pojo.HoursItem
import kotlinx.android.synthetic.main.item_reserve.view.*

class ReserveAdapter(val listener: (HoursItem?) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list!![position], listener)
    }

    var list: ArrayList<HoursItem?>? = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_reserve, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: HoursItem?, listener: (HoursItem) -> Unit) {
        itemView.setOnClickListener {
            if (itemView.iconNavigate.visibility == View.VISIBLE) {
                itemView.iconNavigate.visibility = View.INVISIBLE
                itemView.itemGallery.visibility = View.VISIBLE
            } else {
                itemView.iconNavigate.visibility = View.VISIBLE
                itemView.itemGallery.visibility = View.INVISIBLE
            }
            listener(item!!)
        }
        if (item!!.check!!) {
            itemView.iconNavigate.visibility = View.VISIBLE
            itemView.itemGallery.visibility = View.INVISIBLE
        } else {
            itemView.iconNavigate.visibility = View.INVISIBLE
            itemView.itemGallery.visibility = View.VISIBLE

        }

        itemView.time.text = item.hour.toString() + ":00"
        itemView.price.text = item.price.toString()
    }
}