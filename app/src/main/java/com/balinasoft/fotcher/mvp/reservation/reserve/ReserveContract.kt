package com.balinasoft.fotcher.mvp.reservation.reserve

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.reserve.pojo.FreeTimeResponse
import com.balinasoft.fotcher.entity.reserve.pojo.HoursItem

@StateStrategyType(SkipStrategy::class)
interface ReserveContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        @StateStrategyType(SkipStrategy::class)
        fun showTimes(t: FreeTimeResponse)

        @StateStrategyType(SkipStrategy::class)
        fun showHours(data: List<HoursItem?>?)
    }
}