package com.balinasoft.fotcher.mvp.auth.signup

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


@StateStrategyType(SkipStrategy::class)
interface SignUpContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)

    }
}