package com.balinasoft.fotcher.mvp.profile.profilemodel

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.user.UserEntity

@StateStrategyType(SkipStrategy::class)
interface ProfileModelContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        @StateStrategyType(SkipStrategy::class)
        fun showUser(userEntity: UserEntity)

        @StateStrategyType(SkipStrategy::class)
        fun hideProgress()

    }
}
