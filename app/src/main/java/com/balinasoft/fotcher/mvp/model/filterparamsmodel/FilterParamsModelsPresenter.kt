package com.balinasoft.fotcher.mvp.model.filterparamsmodel

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERMODELS
import com.balinasoft.fotcher.entity.filtermodels.OptionsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filterparamsmodels.FilterParamsModelsInteractor


import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterParamsModelsPresenter @Inject constructor(
        private val router: Router,
        private val filtersParamsModelsInteractor: FilterParamsModelsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterParamsModelContract.View>() {

    private var paramsEntity: ParamsEntity? = null

    fun onClickedBack() {

        val d = filtersParamsModelsInteractor.saveOptionsPhotostudiosEntity(paramsEntity!!).observeOn(appSchedulers.ui()).subscribe({ _ ->
            router.backTo(FILTERMODELS)
        }, { _ ->

        })
        unsubscribeOnDestroy(d)

    }

    fun onClickedItem(item: OptionsEntity) {
        for (i in 0 until paramsEntity!!.options!!.size) {
            if (this.paramsEntity!!.options!![i]!! == item)
                paramsEntity!!.options!![i]!!.check = !paramsEntity!!.options!![i]!!.check!!
        }
    }

    fun setParams(paramsEntity: ParamsEntity) {
        this.paramsEntity = paramsEntity
    }
}