package com.balinasoft.fotcher.mvp.infoprofileoption

import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.mvp.infoprofileoption.adapters.InfoProfileOptionAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_info.*
import javax.inject.Inject
import javax.inject.Provider

class InfoProfileOptionFragment : MvpAppCompatFragment(), InfoProfileOptionContract.View {
    companion object {
        fun newInstance(params: Any): InfoProfileOptionFragment {
            return InfoProfileOptionFragment().apply {
                arguments = Bundle().apply { putParcelable("options", params as ParamsItem) }
            }
        }
    }


    @InjectPresenter
    lateinit var infoProfileOptionPresenter: InfoProfileOptionPresenter

    @Inject
    lateinit var presenterProvider: Provider<InfoProfileOptionPresenter>

    @ProvidePresenter
    fun providePresenter(): InfoProfileOptionPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_info, container, false)
        setHasOptionsMenu(true)

        return view
    }
    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_info.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_info)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title =
                (arguments!!["options"] as ParamsItem).caption


        toolbar_info.setNavigationOnClickListener(
                { _ -> infoProfileOptionPresenter.onBack() })

        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerInfo.addItemDecoration(itemDecor)
        recyclerInfo.layoutManager = linearLayoutManager

        recyclerInfo.adapter = InfoProfileOptionAdapter { item -> infoProfileOptionPresenter.onClickedItem(item) }

        val l = ((arguments!!["options"] as ParamsItem).options as MutableList<OptionsItem>)

        (recyclerInfo.adapter as InfoProfileOptionAdapter).list = l
        (recyclerInfo.adapter as InfoProfileOptionAdapter).notifyDataSetChanged()

        infoProfileOptionPresenter.set((arguments!!["options"] as ParamsItem))
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}