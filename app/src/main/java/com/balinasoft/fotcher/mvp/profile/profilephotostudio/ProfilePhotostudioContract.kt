package com.balinasoft.fotcher.mvp.profile.profilephotostudio

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.photostudio.pojo.PhotostudioResponse

@StateStrategyType(SkipStrategy::class)
interface ProfilePhotostudioContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        @StateStrategyType(SkipStrategy::class)
        fun showPhotostudio(photostudio: PhotostudioResponse)

        @StateStrategyType(SkipStrategy::class)
        fun hideProgress()
    }
}