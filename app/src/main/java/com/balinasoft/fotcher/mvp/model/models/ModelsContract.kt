package com.balinasoft.fotcher.mvp.model.models

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.models.pojo.ContentItem
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse

@StateStrategyType(SkipStrategy::class)
interface ModelsContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showModels(list: MutableList<ContentItem?>, t: ModelsResponse)
        fun hideProgressLoading()
        fun loadFirstPage()
        fun showToast(text: String?)
        fun removeFooter()
        fun showModelsClear(list: MutableList<ContentItem?>, t: ModelsResponse)

        fun showModels(t:ModelsResponse)
    }
}