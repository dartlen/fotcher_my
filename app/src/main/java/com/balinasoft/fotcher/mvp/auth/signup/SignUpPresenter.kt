package com.balinasoft.fotcher.mvp.auth.signup

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.USERS
import com.balinasoft.fotcher.Constants.VERIFICATION
import com.balinasoft.fotcher.model.interactor.signup.SignUpInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SignUpPresenter @Inject constructor(
        private val router: Router,
        private val signUpInteractor: SignUpInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<SignUpContract.View>() {

    fun onClickedSignUp(userType: String, email: String, password: String, password2: String) {
        if (email.isEmailValid() && password.equals(password2)) {
            signUpInteractor.signup(email, password, USERS.getValue(userType))
                    .subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe({ _ ->
                        router.navigateTo(VERIFICATION, email)

                        viewState.hideKeyboard()
                    }, { t ->
                        viewState.showToast(t.message)
                    })
        }
    }

    fun onClickedVerefication() {
        viewState.hideKeyboard()
        router.navigateTo(VERIFICATION, 0)
    }
}