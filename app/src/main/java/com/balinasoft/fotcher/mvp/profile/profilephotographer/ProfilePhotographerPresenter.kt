package com.balinasoft.fotcher.mvp.profile.profilephotographer

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.DIALOG
import com.balinasoft.fotcher.Constants.GALLERY
import com.balinasoft.fotcher.Constants.INFOPHOTOGRAPHER
import com.balinasoft.fotcher.Constants.PROFILEOPTION
import com.balinasoft.fotcher.entity.user.ParamsEntity
import com.balinasoft.fotcher.entity.user.UserEntity
import com.balinasoft.fotcher.model.interactor.profilephotographer.ProfilePhotographerInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ProfilePhotographerPresenter @Inject constructor(
        private val router: Router,
        private val profilePhotgrapherInteractor: ProfilePhotographerInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ProfilePhotographerContract.View>() {

    lateinit var user: UserEntity
    fun onStart(userId: Int) {
        viewState.showProgress()
        profilePhotgrapherInteractor.onStart(userId)!!.subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    user = t
                    viewState.showUser(t)
                    viewState.hideProgress()
                    Log.d("", "")
                }, { t ->
                    Log.d("", "")
                })
    }

    fun onClickedParams(paramsEntity: ParamsEntity) {
        router.navigateTo(INFOPHOTOGRAPHER, paramsEntity)
    }

    fun onClickedGallery(data: ArrayList<Int>) {
        router.navigateTo(GALLERY, data)
    }

    fun onClickedOptions() {
        router.navigateTo(PROFILEOPTION, "")
    }

    fun onClickedDialog(id: Int) {
        val dispose = profilePhotgrapherInteractor.getUser(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t -> router.navigateTo(DIALOG, t) }, {})
        unsubscribeOnDestroy(dispose)

    }

    fun onClickedCall() {
        if (user.phone != null)
            viewState.call(user.phone!!, user.name)
        else {

        }
    }
}
