package com.balinasoft.fotcher.mvp.comments

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.model.interactor.comments.CommentsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class CommentsPresenter @Inject constructor(
        private val router: Router,
        private val commentsInteractor: CommentsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<CommentsContract.View>() {

    fun onStart(id: String) {
        val dispose = commentsInteractor.onStart(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    viewState.showComments(t!!.content)
                }, { _ ->
                    Log.d("", "")
                })

        unsubscribeOnDestroy(dispose)
    }

    fun postComment(id: String, stars: Float, comment: String) {
        /*if(comment.length<5)
            viewState.showToast("Слишком короткий комментарий")
        else {*/
        val dispose = commentsInteractor.postComment(id, stars, comment).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe({ t ->
                    viewState.updateView(t!!)
                    viewState.hideKeyboard()
                }, { _ ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
        // }
    }
}