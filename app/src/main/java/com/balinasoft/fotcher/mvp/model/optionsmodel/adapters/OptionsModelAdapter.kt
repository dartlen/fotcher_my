package com.balinasoft.fotcher.mvp.model.optionsmodel.adapters

import android.annotation.SuppressLint
import android.app.DatePickerDialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.inflate
import com.balinasoft.fotcher.entity.profileoptions.pojo.ElementsItem
import kotlinx.android.synthetic.main.item_date.view.*
import kotlinx.android.synthetic.main.item_range.view.*
import kotlinx.android.synthetic.main.item_spinner.view.*
import java.util.*

class OptionsModelAdapter(val listener: (ElementsItem) -> Unit,
                          private val listener2: (ElementsItem) -> Unit,
                          private val listener3: (ElementsItem) -> Unit,
                          private val listener4: (ElementsItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_EDITE = 0
        const val TYPE_SPINNER = 1
        const val TYPE_IN_RANGE = 2
        const val TYPE_DATE = 3
    }

    var list: ArrayList<ElementsItem> = arrayListOf()

    override fun getItemViewType(position: Int): Int {
        return when (list[position].type) {
            "STRING" -> TYPE_EDITE
            "SELECT" -> TYPE_SPINNER
            "INT_RANGE" -> TYPE_IN_RANGE
            "DATE" -> TYPE_DATE
            else -> 100
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_EDITE -> ViewHolderEdite(parent.inflate(R.layout.item_edite))
            TYPE_SPINNER -> ViewHolderSpinner(parent.inflate(R.layout.item_spinner))
            TYPE_IN_RANGE -> ViewHolderRange(parent.inflate(R.layout.item_range))
            TYPE_DATE -> ViewHolderDate(parent.inflate(R.layout.item_date))
            else -> ViewHolderRange(parent.inflate(R.layout.item_range))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return when (getItemViewType(position)) {
            TYPE_EDITE -> (holder as ViewHolderEdite).bind(list[position], listener)
            TYPE_SPINNER -> (holder as ViewHolderSpinner).bind(list[position], listener2)
            TYPE_IN_RANGE -> (holder as ViewHolderRange).bind(list[position], listener3)
            TYPE_DATE -> (holder as ViewHolderDate).bind(list[position], listener4)
            else -> (holder as ViewHolderRange).bind(list[position], listener3)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}


class ViewHolderDate(view: View) : RecyclerView.ViewHolder(view) {
    private var mDateSetListener: DatePickerDialog.OnDateSetListener? = null
    fun bind(item: ElementsItem?, listener: (ElementsItem) -> Unit) {
        if (item!!.value != null)
            itemView.birthday.text = item.value.toString()
        itemView.birthdayBackground.setOnClickListener({
            val cal = Calendar.getInstance()
            val year = cal.get(Calendar.YEAR)
            val month = cal.get(Calendar.MONTH)
            val day = cal.get(Calendar.DAY_OF_MONTH)

            val dialog = DatePickerDialog(
                    itemView.context,
                    android.R.style.Theme_Holo_Dialog_MinWidth,
                    mDateSetListener,
                    year, month, day)
            dialog.datePicker.maxDate = Date().time
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        })

        mDateSetListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
            var month = month
            month += 1
            val date = day.toString() + "." + month + "." + year
            itemView.birthday.text = date
            item.value = year.toString() + "-" + month.toString() + "-" + day
            listener(item)
        }
    }
}

class ViewHolderEdite(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ElementsItem?, listener: (ElementsItem) -> Unit) {
        /*itemView.edit.hint = item!!.caption
        if(item.value!=null || item.value!="")
            itemView.edit.setText(item.value as String)
        itemView.edit.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                item.value=s.toString()
                listener(item)
            }
        })*/

    }
}

class ViewHolderSpinner(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ElementsItem?, listener: (ElementsItem) -> Unit) {
        val listvalue = arrayListOf<String>()

        item!!.options!!.forEach { x ->
            listvalue.add(x!!.caption!!)
        }
        listvalue.add(item.caption!!)

        val spinnerArrayAdapter = object : ArrayAdapter<String>(
                itemView.context, R.layout.spinner_textview_main, listvalue) {

            override fun getCount(): Int {
                val count = super.getCount()
                return if (count > 0) count - 1 else count
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val v = super.getView(position, convertView, parent)
                if (position == listvalue.size - 1) {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context!!, R.color.gray))
                } else {
                    val mytextview = v as TextView
                    mytextview.setTextColor(ContextCompat.getColor(context!!, R.color.text))
                }
                return v
            }
        }

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
        itemView.spinnerMain.adapter = spinnerArrayAdapter
        itemView.spinnerMain.setSelection(checkValueSpinner(item), false)
        itemView.spinnerMain.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                item.options!!.forEachIndexed { index, optionsItem ->
                    optionsItem!!.check = index == position
                }
                listener(item)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }
        }
    }

    fun checkValueSpinner(paramsEntity: ElementsItem): Int {
        for (i in 0 until paramsEntity.options!!.size) {
            if (paramsEntity.options[i]!!.check!!) {
                return i
            }
        }
        return paramsEntity.options.size
    }
}

class ViewHolderRange(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ElementsItem?, listener: (ElementsItem) -> Unit) {
        itemView.rangeSeekbar1.setOnSeekbarChangeListener { maxValue ->
            if (maxValue.toInt() != 0) {
                itemView.maxValueParam1.text = maxValue.toString()
                val x = arrayListOf(maxValue.toInt())
                item!!.border = x
                listener(item)
            }
        }
        if (item!!.border != null) {
            itemView.rangeSeekbar1.minStartValue = item.border!![0].toFloat()
        } else {
            itemView.rangeSeekbar1.minStartValue = item.minInt!!.toFloat()
        }
        itemView.rangeSeekbar1.maxValue = item.maxInt!!.toFloat()
        itemView.rangeSeekbar1.minValue = item.minInt!!.toFloat()
        itemView.nameSeekbar1.text = item.caption
        itemView.rangeSeekbar1.apply()
    }
}