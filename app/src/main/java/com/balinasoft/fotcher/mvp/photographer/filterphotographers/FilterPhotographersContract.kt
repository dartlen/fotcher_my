package com.balinasoft.fotcher.mvp.photographer.filterphotographers

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity

@StateStrategyType(SkipStrategy::class)
interface FilterPhotographersContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showFilters(data: FilterParamsEntity)
        fun setDelault(size: Int, size1: Int, size2: Int)
    }
}