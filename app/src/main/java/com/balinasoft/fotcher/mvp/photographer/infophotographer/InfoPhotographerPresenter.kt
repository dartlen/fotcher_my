package com.balinasoft.fotcher.mvp.photographer.infophotographer

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.model.interactor.infophotographer.InfoPhotographerInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class InfoPhotographerPresenter @Inject constructor(
        private val router: Router,
        private val infoInteractor: InfoPhotographerInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<InfoPhotographerContract.View>() {

    //var paramsEntity: ParamsEntity? = null

    /*fun onClickedBack(){
        val d = filtersParamsInteractor.saveOptionsEntity(paramsEntity!!).observeOn(appSchedulers.ui()).subscribe({
            _ ->
            router.backTo(FILTERPHOTOGRAPHERS)
        },{t->

        })
        unsubscribeOnDestroy(d)
    }*/

    /*fun onClickedItem(item: OptionsEntity){
        for (i in 0 until paramsEntity!!.options!!.size) {
            if(this.paramsEntity!!.options!![i]!! == item)
                paramsEntity!!.options!![i]!!.check = !paramsEntity!!.options!![i]!!.check!!
        }
    }

    fun setParams(paramsEntity: ParamsEntity){
        this.paramsEntity = paramsEntity
    }*/

}