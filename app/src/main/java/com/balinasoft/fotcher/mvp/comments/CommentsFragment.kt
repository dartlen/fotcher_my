package com.balinasoft.fotcher.mvp.comments

import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.comments.pojo.CommentSendResponse
import com.balinasoft.fotcher.entity.comments.pojo.ContentItem
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.comments.adapters.CommentsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.framgnet_comments.*
import javax.inject.Inject
import javax.inject.Provider

class CommentsFragment : MvpAppCompatFragment(), CommentsContract.View {
    companion object {
        fun newInstance(id: Any): CommentsFragment {
            return CommentsFragment().apply {
                arguments = Bundle().apply {
                    putString("id", id as String)
                }
            }
        }
    }

    @InjectPresenter
    lateinit var commentsPresenter: CommentsPresenter

    @Inject
    lateinit var presenterProvider: Provider<CommentsPresenter>

    @ProvidePresenter
    fun providePresenter(): CommentsPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    @Inject
    lateinit var appSchedulers: AppSchedulers

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.framgnet_comments, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_comments)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_comments.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerComments.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerComments.addItemDecoration(itemDecor)
        recyclerComments.adapter = CommentsAdapter()

        buttonSendComment.setOnClickListener {

            commentsPresenter.postComment(arguments!!.getString("id"), library_tinted_wide_ratingbar.rating, comment.text.toString())
        }

        library_tinted_wide_ratingbar.setOnRatingBarChangeListener({ ratingBar, rating, fromUser ->
            if (fromUser) {
                ratingBar.rating = Math.ceil(rating.toDouble()).toFloat()
            }
        })

    }

    override fun onStart() {
        super.onStart()
        commentsPresenter.onStart(arguments!!.getString("id"))
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showComments(list: List<ContentItem?>?) {
        (recyclerComments.adapter as CommentsAdapter).list = list as ArrayList<ContentItem?>
        (recyclerComments.adapter as CommentsAdapter).notifyDataSetChanged()
    }

    override fun updateView(data: CommentSendResponse) {

        (recyclerComments.adapter as CommentsAdapter).add(ContentItem(date = data.date,
                id = data.id,
                user = data.user,
                text = data.text,
                star = data.star
        ))
        (recyclerComments.adapter as CommentsAdapter).notifyDataSetChanged()
        comment.visibility = View.GONE
        textComment.visibility = View.GONE
        library_tinted_wide_ratingbar.visibility = View.GONE

        library_tinted_wide_ratingbar.rating = 0.0F

        buttonSendComment.visibility = View.GONE
    }
}