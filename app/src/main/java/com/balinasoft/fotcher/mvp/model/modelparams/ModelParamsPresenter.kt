package com.balinasoft.fotcher.mvp.model.modelparams

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.model.interactor.modelparams.ModelParamsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ModelParamsPresenter @Inject constructor(
        private val router: Router,
        private val modelsParamsInteractor: ModelParamsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ModelParamsContract.View>() {

}