package com.balinasoft.fotcher.mvp.photographer.infophotographer.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.ValueEntity
import kotlinx.android.synthetic.main.item_info.view.*

class InfoPhotographerAdapter :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list!![position])
    }

    var list: MutableList<ValueEntity?>? = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_info, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ValueEntity?) {
        itemView.textInfo.text = item!!.caption
    }
}
