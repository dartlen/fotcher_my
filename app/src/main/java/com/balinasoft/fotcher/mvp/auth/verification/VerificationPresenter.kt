package com.balinasoft.fotcher.mvp.auth.verification

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PHOTOSTUDIOS
import com.balinasoft.fotcher.entity.verification.VerificationResponse
import com.balinasoft.fotcher.model.interactor.verification.VerificationInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class VerificationPresenter @Inject constructor(
        private val router: Router,
        private val verificationInteractor: VerificationInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<VerificationContract.View>() {

    fun onClickedVerification(code: String, email: String) {
        if (email.isEmailValid() && code.length == 6) {
            verificationInteractor.setUserName(email)
            verificationInteractor.verificateAccount(code, email).subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui()).subscribe({ t ->
                        if ((t.body() as VerificationResponse).token != null) {
                            viewState.hideKeyboard()
                            verificationInteractor.saveToken((t.body() as VerificationResponse).token)

                            val dispose = verificationInteractor.getCurrentUser()
                                    .subscribeOn(appSchedulers.io())
                                    .observeOn(appSchedulers.ui())
                                    .subscribe({ t2 ->

                                        verificationInteractor.saveUserId(t2!!.id!!)

                                    }, {

                                    })

                            unsubscribeOnDestroy(dispose)
                            router.navigateTo(PHOTOSTUDIOS, 0)
                        }
                    }, { t ->
                        viewState.showToast(t.message)
                    })
        } else
            viewState.showToast("Некоретные данные")
    }

}