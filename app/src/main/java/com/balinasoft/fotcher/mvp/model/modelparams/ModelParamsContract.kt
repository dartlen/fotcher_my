package com.balinasoft.fotcher.mvp.model.modelparams

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface ModelParamsContract {
    interface View : MvpView {
        fun hideKeyboard()
        //fun showPhotographers(list:MutableList<ContentItem?>, t: PhotographersResponse)
        fun showToast(text: String?)
    }
}