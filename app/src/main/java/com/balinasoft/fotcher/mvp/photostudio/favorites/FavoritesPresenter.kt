package com.balinasoft.fotcher.mvp.photostudio.favorites

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PROFILEPHOTOSTUDIO
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.model.interactor.favorites.FavoritesInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FavoritesPresenter @Inject constructor(
        private val router: Router,
        private val favoritesInteractor: FavoritesInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FavoritesContract.View>() {

    fun onStart(currentPage: Int) {
        if (currentPage == 0 || favoritesInteractor.getFlagReload())
            favoritesInteractor.onStart().subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe(
                            { t: PhotostudiosResponse? ->
                                if (!favoritesInteractor.getFlagReload()) {
                                    viewState.showPhotostudios(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                } else {
                                    favoritesInteractor.setFlagReload()
                                    viewState.showPhotostudiosClear(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                }

                            },
                            { _ ->
                                Log.d("", "")
                            })
    }

    fun onClickedLikePhotostudio(item: ContentItem) {
        val dispose = favoritesInteractor.likePhotostudio(item.id!!, item.favorite!!)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui()).subscribe(
                        { _ ->
                            Log.d("", "")
                        },
                        { t ->
                            viewState.showToast(t.message)
                            viewState.showValueLike(item)
                        }
                )
        unsubscribeOnDestroy(dispose)
    }

    fun onClickedPhotostudio(item: ContentItem) {
        router.navigateTo(PROFILEPHOTOSTUDIO, item.id)
    }

    fun loadNextPage(currentPage:Int, query:String){
        val dispose = favoritesInteractor.loadNextPage(currentPage, query)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe(
                        { t: PhotostudiosResponse? ->
                           viewState.showNextPage(t)
                        },
                        { _: Throwable? ->
                        })
        unsubscribeOnDestroy(dispose)
    }
}
