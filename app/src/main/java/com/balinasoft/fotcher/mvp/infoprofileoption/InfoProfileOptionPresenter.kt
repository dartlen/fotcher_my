package com.balinasoft.fotcher.mvp.infoprofileoption

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PROFILEOPTION
import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.model.interactor.infoprofileoption.InfoProfileOptionInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class InfoProfileOptionPresenter @Inject constructor(
        private val router: Router,
        private val infoInteractor: InfoProfileOptionInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<InfoProfileOptionContract.View>() {

    lateinit var params: ParamsItem

    fun onClickedItem(item: OptionsItem) {
        for (i in 0 until params.options!!.size) {
            if (params.options!![i]!! == item)
                params.options!![i]!!.check = !params.options!![i]!!.check!!
        }
    }

    fun set(p: ParamsItem) {
        params = p
    }

    fun onBack() {
        val d = infoInteractor.saveOption(params)
                .observeOn(appSchedulers.ui())
                .subscribe({ _ ->
                    router.backTo(PROFILEOPTION)
                }, { _ ->

                })
        unsubscribeOnDestroy(d)
    }
}