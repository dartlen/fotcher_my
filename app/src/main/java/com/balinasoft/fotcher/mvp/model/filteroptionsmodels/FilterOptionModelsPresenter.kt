package com.balinasoft.fotcher.mvp.model.filteroptionsmodels

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERMODELS
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filteroptionsmodels.FilterOptionsModelsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterOptionModelsPresenter @Inject constructor(
        private val router: Router,
        private val filterOptionsModelsInteractor: FilterOptionsModelsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterOptionModelsContract.View>() {
    lateinit var paramsEntity: ParamsEntity

    fun onStart(paramsEntity: ParamsEntity) {
        this.paramsEntity = paramsEntity
        viewState.showFilters(paramsEntity)
    }

    fun onItemSelected(position: Int, id: Int) {
        for (i in 0 until paramsEntity.elements[id + 6]!!.options!!.size) {
            if (i == position) {
                paramsEntity.elements[id + 6]!!.options!![i]!!.check = true
                break
            }

        }
    }

    fun checkValueSpinner(position: Int): Int {
        for (i in 0 until paramsEntity.elements[position]!!.options!!.size) {
            if (paramsEntity.elements[position]!!.options!![i]!!.check!!) {
                return i
            }
        }
        return paramsEntity.elements[position]!!.options!!.size
    }

    fun onChangeValueSeekBar(min: String, max: String, seekBar: Int) {
        paramsEntity.elements[seekBar]!!.borders = mutableListOf(min.toInt(), max.toInt())
    }

    fun onBack() {
        val dispode = filterOptionsModelsInteractor.saveFilters(paramsEntity)!!
                .doOnComplete { router.navigateTo(FILTERMODELS, 1) }.subscribe()
        unsubscribeOnDestroy(dispode)
    }

}
