package com.balinasoft.fotcher.mvp.gallery.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.gallery.pojo.ContentItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_gallery.view.*

class GalleryAdapter(private val listener: (ContentItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list[position], listener)
    }

    var list: ArrayList<ContentItem> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_gallery, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: ContentItem, listener: (ContentItem) -> Unit) {
        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))
        Glide.with(itemView.context)
                .load(item.small)
                .apply(options)
                .into(itemView.itemGallery)
        itemView.setOnClickListener { listener(item) }

    }
}