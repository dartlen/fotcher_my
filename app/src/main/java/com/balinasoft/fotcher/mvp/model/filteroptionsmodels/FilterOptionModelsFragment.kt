package com.balinasoft.fotcher.mvp.model.filteroptionsmodels

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import kotlinx.android.synthetic.main.fragment_filteroptionsmodel.*
import javax.inject.Inject
import javax.inject.Provider

class FilterOptionModelsFragment : MvpAppCompatFragment(), FilterOptionModelsContract.View {

    companion object {
        fun newInstance(params: Any): FilterOptionModelsFragment {
            return FilterOptionModelsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("options", (params as ParamsEntity))
                }
            }
        }
    }

    lateinit var listSeek: MutableList<CrystalRangeSeekbar>
    lateinit var listName: MutableList<TextView>
    lateinit var listSpinner: MutableList<Spinner>

    @InjectPresenter
    lateinit var filterOptionModelsPresenter: FilterOptionModelsPresenter

    @Inject
    lateinit var presenterProvider: Provider<FilterOptionModelsPresenter>

    @ProvidePresenter
    fun providePresenter(): FilterOptionModelsPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        return inflater.inflate(R.layout.fragment_filteroptionsmodel, container,
                false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        filterOptionModelsPresenter.onStart((arguments!!["options"] as ParamsEntity))
        rangeSeekbar1.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams1.text = minValue.toString()
                maxValueParam1.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 0)
            }
        }

        rangeSeekbar2.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams2.text = minValue.toString()
                maxValueParam2.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 1)
            }
        }

        rangeSeekbar3.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams3.text = minValue.toString()
                maxValueParam3.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 2)
            }
        }

        rangeSeekbar4.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams4.text = minValue.toString()
                maxValueParam4.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 3)
            }
        }

        rangeSeekbar5.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams5.text = minValue.toString()
                maxValueParam5.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 4)
            }
        }

        rangeSeekbar6.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (minValue.toInt() != 0) {
                minValueParams6.text = minValue.toString()
                maxValueParam6.text = maxValue.toString()
                filterOptionModelsPresenter.onChangeValueSeekBar(minValue.toString(), maxValue.toString(), 5)
            }
        }

        listSeek = mutableListOf(rangeSeekbar1, rangeSeekbar2, rangeSeekbar3, rangeSeekbar4,
                rangeSeekbar5, rangeSeekbar6)

        listName = mutableListOf(nameSeekbar1, nameSeekbar2, nameSeekbar3, nameSeekbar4, nameSeekbar5,
                nameSeekbar6)
        listSpinner = mutableListOf(spinner1, spinner2, spinner3, spinner4, spinner5, spinner6)
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbarFilterOptionsModels)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbarFilterOptionsModels.setNavigationOnClickListener(
                { _ -> filterOptionModelsPresenter.onBack() })

    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showFilters(paramsEntity: ParamsEntity) {
        initseekbars((arguments!!["options"] as ParamsEntity))
        initspinners(arguments!!["options"] as ParamsEntity)
    }

    private fun initseekbars(paramsEntity: ParamsEntity) {
        for (i in 0..5) {
            if (paramsEntity.elements[i]!!.borders != null) {
                listSeek[i].setMaxStartValue(paramsEntity.elements[i]!!.borders!![1]!!.toFloat())
                listSeek[i].setMinStartValue(paramsEntity.elements[i]!!.borders!![0]!!.toFloat())
            } else {
                listSeek[i].setMaxStartValue(paramsEntity.elements[i]!!.maxInt!!.toFloat())
                listSeek[i].setMinStartValue(paramsEntity.elements[i]!!.minInt!!.toFloat())
            }
            listSeek[i].setMaxValue(paramsEntity.elements[i]!!.maxInt!!.toFloat())
            listSeek[i].setMinValue(paramsEntity.elements[i]!!.minInt!!.toFloat())
            listName[i].text = paramsEntity.elements[i]!!.caption
            listSeek[i].apply()
        }
    }

    private fun initspinners(paramsEntity: ParamsEntity) {
        var countSpinner = 0
        for (i in 6..11) {
            val listvalue = arrayListOf<String>()

            paramsEntity.elements[i]!!.options!!.forEach { x ->
                listvalue.add(x!!.caption!!)
            }
            listvalue.add(paramsEntity.elements[i]!!.caption!!)

            val spinnerArrayAdapter = object : ArrayAdapter<String>(
                    context, R.layout.spinner_textview_main, listvalue) {

                override fun getCount(): Int {
                    val count = super.getCount()
                    return if (count > 0) count - 1 else count
                }

                override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                    val v = super.getView(position, convertView, parent)
                    if (position == listvalue.size - 1) {
                        val mytextview = v as TextView
                        mytextview.setTextColor(ContextCompat.getColor(context, R.color.gray))
                    } else {
                        val mytextview = v as TextView
                        mytextview.setTextColor(ContextCompat.getColor(context, R.color.text))
                    }
                    return v
                }
            }

            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_textview)
            listSpinner[countSpinner].adapter = spinnerArrayAdapter
            listSpinner[countSpinner].setSelection(filterOptionModelsPresenter.checkValueSpinner(i), false)
            listSpinner[countSpinner].onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {

                    listSpinner.forEachIndexed { index, t ->
                        if (t == parentView)
                            filterOptionModelsPresenter.onItemSelected(position, index)
                    }
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    // your code here
                }
            }
            countSpinner++
        }
    }
}