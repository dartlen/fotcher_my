package com.balinasoft.fotcher.mvp.gallery

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.gallery.pojo.ContentItem
import com.balinasoft.fotcher.entity.gallery.pojo.GalleryResponse

@StateStrategyType(SkipStrategy::class)
interface GalleryContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showGallery(gallry: GalleryResponse)
        fun hideProgressLoading()
        fun loadFirstPage()
        fun showToast(text: String?)
        fun removeFooter()
        @StateStrategyType(SkipStrategy::class)
        fun selectImage()

        fun reload()
        @StateStrategyType(SkipStrategy::class)
        fun showImage(item: ContentItem)

        @StateStrategyType(SkipStrategy::class)
        fun onBack()

        @StateStrategyType(SkipStrategy::class)
        fun hideImage()
    }
}