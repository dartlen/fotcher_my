package com.balinasoft.fotcher.mvp.photostudio.map.map

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.PROFILEPHOTOSTUDIO
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.model.interactor.map.MapInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import com.google.android.gms.maps.model.Marker
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class MapPresenter @Inject constructor(
        private val router: Router,
        private val mapInteractor: MapInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<MapContract.View>() {
    var list: MutableList<Marker> = mutableListOf()
    var listPoints: ArrayList<ContentItem> = arrayListOf()
    var item: ContentItem? = null

    fun getMarker(m: Marker) {
        list.add(m)
    }

    fun onClickedMarker(m: Marker) {
        list.forEachIndexed { index, marker ->
            if (marker == m) {
                if (listPoints[index].flag) {
                    listPoints[index].flag = false
                    viewState.showDisableMarker(marker)
                    viewState.disableStudioInfo()
                } else {
                    listPoints[index].flag = true
                    viewState.showMarker(marker)
                    viewState.showInfo(listPoints[index])
                    item = listPoints[index]
                    viewState.enableStudioInfo()
                }

            } else {
                if (listPoints[index].flag) {
                    listPoints[index].flag = false
                    viewState.showDisableMarker(marker)
                }
            }
        }
    }

    fun setPoints(list: ArrayList<ContentItem>) {
        this.list = arrayListOf()
        listPoints = list

    }

    fun onClickedStudio() {
        item!!.flag = false
        viewState.disableStudioInfo()
        router.navigateTo(PROFILEPHOTOSTUDIO, item!!.id)
    }

    fun clearMarkers() {
        viewState.clearMarkers(list)
    }
}
