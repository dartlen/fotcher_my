package com.balinasoft.fotcher.mvp.photographer.photographers

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.photographers.ContentItem
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse

@StateStrategyType(OneExecutionStateStrategy::class)
interface PhotographersContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showPhotographers(list: MutableList<ContentItem?>, t: PhotographersResponse)
        fun hideProgressLoading()
        fun loadFirstPage()
        fun showToast(text: String?)
        fun removeFooter()
        fun showPhotographersClear(list: MutableList<ContentItem?>, t: PhotographersResponse)
        fun showPotographersNext(t: PhotographersResponse?)
    }
}