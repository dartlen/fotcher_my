package com.balinasoft.fotcher.mvp.photostudio.map.map

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.mvp.photostudio.photostudios.PhotoStudiosFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_map.*
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class MapFragment : MvpAppCompatFragment(), MapContract.View, OnMapReadyCallback, MapContract.OnSearchListener {
    @Suppress("UNCHECKED_CAST")
    companion object {
        fun newInstance(data: Any) = MapFragment().apply { arguments = Bundle().apply { putParcelableArrayList("data", data as ArrayList<ContentItem>) } }
    }

    @InjectPresenter
    lateinit var mapPresenter: MapPresenter

    @Inject
    lateinit var presenterProvider: Provider<MapPresenter>

    @ProvidePresenter
    fun providePresenter(): MapPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    lateinit var map: GoogleMap
    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_map, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        mapPresenter.setPoints((arguments!!["data"] as ArrayList<ContentItem>))

        return view
    }

    override fun onStart() {
        super.onStart()

        if (parentFragment != null)
            (parentFragment as PhotoStudiosFragment).setMap(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)


        InfoPhotostudio.setOnClickListener { mapPresenter.onClickedStudio() }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
    @Suppress("UNCHECKED_CAST")
    override fun onMapReady(p0: GoogleMap?) {
        try {
            MapsInitializer.initialize(this.activity)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }
        (arguments!!["data"] as ArrayList<ContentItem>).forEach {
            mapPresenter.getMarker(p0!!.addMarker(MarkerOptions().position(LatLng(it.lat!!.toDouble(), it.lng!!.toDouble()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_gray))))
        }

        p0!!.setOnMarkerClickListener { m: Marker ->
            mapPresenter.onClickedMarker(m)
            true
        }

        map = p0
    }

    override fun showMarker(m: Marker) {
        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_blue))
    }

    override fun showDisableMarker(m: Marker) {
        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_gray))
    }

    @SuppressLint("SetTextI18n")
    override fun showInfo(data: ContentItem) {
        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))
        Glide.with(context!!)
                .load(data.smallAvatar)
                .apply(options)
                .into(imagePhotostudio)

        adressPhotostudio.text = data.name
        namePhotostudio.text = "от " + data.priceMin.toString()
        pricePhotostudio.text = data.address
    }

    override fun enableStudioInfo() {
        InfoPhotostudio.visibility = View.VISIBLE
    }

    override fun disableStudioInfo() {
        InfoPhotostudio.visibility = View.GONE
    }
    @Suppress("UNCHECKED_CAST")
    override fun onSearch(list: MutableList<ContentItem?>) {
        mapPresenter.clearMarkers()
        mapPresenter.setPoints(list as ArrayList<ContentItem>)
        list.forEach {
            mapPresenter.getMarker(map.addMarker(MarkerOptions().position(LatLng(it!!.lat!!.toDouble(), it.lng!!.toDouble()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_label_gray))))
        }
    }

    override fun clearMarkers(list: MutableList<Marker>) {
        disableStudioInfo()
        list.forEach {
            it.remove()
        }
    }

}