package com.balinasoft.fotcher.mvp.photographer.photographers

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.entity.photographers.ContentItem
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.mvp.photographer.photographers.adapter.PhotographersAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_photographers.*
import javax.inject.Inject
import javax.inject.Provider

class PhotographersFragment : MvpAppCompatFragment(), PhotographersContract.View {

    @InjectPresenter
    lateinit var photographersPresenter: PhotographersPresenter

    @Inject
    lateinit var presenterProvider: Provider<PhotographersPresenter>

    @ProvidePresenter
    fun providePresenter(): PhotographersPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 5
    var currentPage = PAGE_START
    var TOTAL_PAGES = 1

    var query = ""

    companion object {
        private val PAGE_START = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_photographers, container, false)
        setHasOptionsMenu(true)
        isLoad = false
        isLast = false

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_photographers)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_photographers.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)


        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        recyclerPhotographers.layoutManager = linearLayoutManager
        recyclerPhotographers.adapter = PhotographersAdapter { Item: ContentItem -> photographersPresenter.onClickedPhotographer(Item) }

        recyclerPhotographers
                .addOnScrollListener(object : PaginationScrollListener(linearLayoutManager!!) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {
                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        } else
                            if ((recyclerPhotographers.adapter as PhotographersAdapter).isLoadingAdded)
                                (recyclerPhotographers.adapter as PhotographersAdapter).removeLoadingFooter()
                    }
                })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener, android.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                query = newText
                photographersPresenter.onInputedDataSearch(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })

        val searchIconId = searchView.context.resources.getIdentifier("android:id/search_button", null, null)
        val searchIcon = searchView.findViewById(searchIconId) as ImageView
        searchIcon.setImageResource(R.drawable.ic_search)

        searchView.queryHint = "Поиск"

        val searchPlateId = searchView.context.resources.getIdentifier("android:id/search_plate", null, null)
        val viewGroup = searchView.findViewById(searchPlateId) as ViewGroup
        viewGroup.setBackgroundColor(Color.parseColor("#1f1f1f"))

        searchView.isFocusable = false
    }

    override fun onStart() {
        super.onStart()
        photographersPresenter.onStart(currentPage)
    }

    override fun showPhotographers(list: MutableList<ContentItem?>, t: PhotographersResponse) {
        TOTAL_PAGES = t.totalPages!!
        (recyclerPhotographers.adapter as PhotographersAdapter).list = list
        (recyclerPhotographers.adapter as PhotographersAdapter).notifyDataSetChanged()
    }

    override fun showPhotographersClear(list: MutableList<ContentItem?>, t: PhotographersResponse) {
        TOTAL_PAGES = t.totalPages!!
        currentPage = 0
        isLoad = false
        isLast = false
        (recyclerPhotographers.adapter as PhotographersAdapter).list.clear()
        (recyclerPhotographers.adapter as PhotographersAdapter).notifyDataSetChanged()
        (recyclerPhotographers.adapter as PhotographersAdapter).list = list
        (recyclerPhotographers.adapter as PhotographersAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_photographers, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_filter -> {
                photographersPresenter.onClickedFilter()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
            (recyclerPhotographers.adapter as PhotographersAdapter).addLoadingFooter()
        else
            isLast = true
    }

    override fun removeFooter() {
        (recyclerPhotographers.adapter as PhotographersAdapter).removeLoadingFooter()
    }

    private fun loadNextPage() {
        if (currentPage != TOTAL_PAGES)
            if (!(recyclerPhotographers.adapter as PhotographersAdapter).isLoadingAdded)
                (recyclerPhotographers.adapter as PhotographersAdapter).addLoadingFooter()
        photographersPresenter.loadNextPage(currentPage, query)

    }

    override fun hideProgressLoading() {
        photographersProgress.visibility = View.GONE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showPotographersNext(t: PhotographersResponse?) {
        (recyclerPhotographers.adapter as PhotographersAdapter)
                .removeLoadingFooter()
        isLoad = false

        (recyclerPhotographers.adapter as PhotographersAdapter)
                .addAll(t!!.content)

        if (currentPage != TOTAL_PAGES)
            (recyclerPhotographers.adapter as PhotographersAdapter)
                    .addLoadingFooter()
        else
            isLast = true
    }
}