package com.balinasoft.fotcher.mvp.dialogs.dialog

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.model.interactor.dialog.DialogInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import java.io.File
import javax.inject.Inject

@InjectViewState
class DialogPresenter @Inject constructor(
        private val router: Router,
        private val dialogInteractor: DialogInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<DialogContract.View>() {

    private var lastMessage: Int? = null
    private var flag: Int? = null
    fun onStart(id: Int) {

        val dispose = dialogInteractor.getDialog(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t: List<DialogResponse>? ->
                    if (t!!.isNotEmpty()) {
                        lastMessage = t[t.size - 1].id
                        viewState.showDialog(t)
                    }
                    viewState.hideProgressLoading()
                }, {
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }

    fun clickedSendMessage(message: String, id: Int) {

        val dispose = dialogInteractor.sendMessage(message, id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe(
                        { t ->
                            viewState.showNewMessage(t!!)
                            Log.d("", "")
                        }, { t: Throwable? ->
                    Log.d("", "")
                }
                )


        viewState.clearOldMessage()
        unsubscribeOnDestroy(dispose)
    }

    fun clickedSendImage() {
        viewState.selectImage()
    }

    fun sendImage(file: File, id: Int) {
        val dispose = dialogInteractor.sendImage(file, id)!!.subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui()).subscribe({ t ->
                    viewState.showNewMessage(t!!)
                    Log.d("", "")
                }, { t: Throwable? ->
                    Log.d("", "")
                })
        unsubscribeOnDestroy(dispose)
    }

    fun loadNextPage(id: Int) {
        if (flag != lastMessage) {
            flag = lastMessage
            val dispose = dialogInteractor.loadNextPage(id, lastMessage!!).subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())

                    .subscribe(
                            { t ->
                                if (t!!.isNotEmpty()) {
                                    lastMessage = t[t.size - 1].id
                                    viewState.showLoadedMessages(t, false)
                                }
                            }, { t ->
                        Log.d("", "")
                    })

            unsubscribeOnDestroy(dispose)
        }
    }
}
