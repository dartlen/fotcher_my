package com.balinasoft.fotcher.mvp.model.filtermodels

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity

@StateStrategyType(SkipStrategy::class)
interface FilterModelsContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        fun showFilters(data: FilterModelsEntity)
        fun setDelault(size: Int, size1: Int, size2: Int, size3: Int)
    }
}