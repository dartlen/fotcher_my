package com.balinasoft.fotcher.mvp.model.modelparams.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.Element
import kotlinx.android.synthetic.main.item_modelparams.view.*

class ModelParamsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list!![position])
    }

    var list: MutableList<Element>? = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_modelparams, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun bind(data: Element) {
        var tmp = " см"
        if (data.name == "weight")
            tmp = " кг"
        else if (data.name == "age")
            tmp = " возвраст"
        itemView.textParam.text = data.caption
        itemView.value.text = data.value.toString() + tmp
    }
}