package com.balinasoft.fotcher.mvp.auth.verification

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.isEmailValid
import kotlinx.android.synthetic.main.fragment_verification.*
import javax.inject.Inject
import javax.inject.Provider


class VerificationFragment : MvpAppCompatFragment(), VerificationContract.View {
    companion object {
        fun newInstance(email: Any): VerificationFragment {
            val fragment = VerificationFragment()
            if (email is String)
                if (email.isEmailValid()) {
                    val args = Bundle()
                    args.putString("email", email)
                    fragment.setArguments(args)
                }
            return fragment
        }
    }

    @InjectPresenter
    lateinit var verificationPresenter: VerificationPresenter

    @Inject
    lateinit var presenterProvider: Provider<VerificationPresenter>

    @ProvidePresenter
    fun providePresenter(): VerificationPresenter{
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_verification, container, false)

        return view
    }

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).setSupportActionBar(toolbar_verification)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar_verification.setNavigationOnClickListener({ _ -> (activity as AppCompatActivity).onBackPressed() })

        button.setOnClickListener { _ -> verificationPresenter.onClickedVerification(verificationCode.text.toString(), emailVerification.text.toString()) }

        if (arguments != null) {
            val email = arguments!!.getString("email")
            if (email != null)
                emailVerification.text = Editable.Factory.getInstance().newEditable(email)
        }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }
}