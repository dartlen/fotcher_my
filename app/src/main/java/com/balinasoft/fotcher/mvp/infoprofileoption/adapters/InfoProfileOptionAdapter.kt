package com.balinasoft.fotcher.mvp.infoprofileoption.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import kotlinx.android.synthetic.main.item_params.view.*

class InfoProfileOptionAdapter(val listener: (OptionsItem) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(list[position], listener)
    }

    var list: MutableList<OptionsItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_params, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n")
    fun bind(item: OptionsItem?, listener: (OptionsItem) -> Unit) {
        itemView.textParam.text = item!!.caption
        itemView.setOnClickListener {
            if (itemView.imageNull.visibility == View.VISIBLE) {
                //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
                itemView.imageNull.visibility = View.GONE
                itemView.imageCheck.visibility = View.VISIBLE
            } else {
                //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
                itemView.imageNull.visibility = View.VISIBLE
                itemView.imageCheck.visibility = View.GONE
            }
            listener(item)
        }
        if (item.check!!) {
            //itemView.border.setBackgroundColor(Color.parseColor("#ffffff"))
            itemView.imageNull.visibility = View.GONE
            itemView.imageCheck.visibility = View.VISIBLE
        } else {
            //itemView.border.setBackgroundColor(Color.parseColor("#898989"))
            itemView.imageNull.visibility = View.VISIBLE
            itemView.imageCheck.visibility = View.GONE
        }
    }
}
