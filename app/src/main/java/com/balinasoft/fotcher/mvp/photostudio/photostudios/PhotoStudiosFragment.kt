package com.balinasoft.fotcher.mvp.photostudio.photostudios

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.PaginationScrollListener
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.mvp.photostudio.map.map.MapContract
import com.balinasoft.fotcher.mvp.photostudio.map.map.MapFragment
import com.balinasoft.fotcher.mvp.photostudio.photostudios.adapters.PhotostudiosAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_photo_studios.*
import kotlinx.android.synthetic.main.nav_header_main.*
import javax.inject.Inject
import javax.inject.Provider

class PhotoStudiosFragment : MvpAppCompatFragment(), PhotoStudiosContract.View {

    @InjectPresenter
    lateinit var photoStudiosPresenter: PhotoStudiosPresenter

    @Inject
    lateinit var presenterProvider: Provider<PhotoStudiosPresenter>

    @ProvidePresenter
    fun providePresenter(): PhotoStudiosPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    var linearLayoutManager: LinearLayoutManager? = null

    var isLoad = false
    var isLast = false
    var TOTAL_ITEMS = 5
    var currentPage = PAGE_START
    var TOTAL_PAGES = 1

    var query = ""

    lateinit var listener: MapContract.OnSearchListener

    companion object {
        private const val PAGE_START = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_photo_studios, container, false)
        setHasOptionsMenu(true)
        isLoad = false
        isLast = false

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_photostudios)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbar_photostudios.setNavigationOnClickListener { _ -> (activity as AppCompatActivity).drawer_layout.openDrawer(GravityCompat.START) }
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)


        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        recyclerPhotostudio.layoutManager = linearLayoutManager
        recyclerPhotostudio.adapter = PhotostudiosAdapter(context,
                { item: ContentItem -> photoStudiosPresenter.onClickedPhotostudio(item) },
                { item: ContentItem -> photoStudiosPresenter.onClickedLikePhotostudio(item) })

        recyclerPhotostudio
                .addOnScrollListener(object : PaginationScrollListener(linearLayoutManager!!) {
                    override val isLastPage: Boolean
                        get() = isLast
                    override var isLoading: Boolean = false
                        get() = isLoad

                    override val totalPageCount: Int
                        get() = TOTAL_ITEMS

                    override fun loadMoreItems() {
                        if (TOTAL_PAGES > currentPage + 1) {
                            isLoading = true
                            currentPage += 1
                            loadNextPage()
                        } else
                            if ((recyclerPhotostudio.adapter as PhotostudiosAdapter).isLoadingAdded)
                                (recyclerPhotostudio.adapter as PhotostudiosAdapter).removeLoadingFooter()
                    }
                })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener, android.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                query = newText
                photoStudiosPresenter.onInputedDataSearch(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })

        imageButton.setOnClickListener {
            photoStudiosPresenter.onClickedMap()
        }

        val searchIconId = searchView.context.resources.getIdentifier("android:id/search_button", null, null)
        val searchIcon = searchView.findViewById(searchIconId) as ImageView
        searchIcon.setImageResource(R.drawable.ic_search)

        searchView.queryHint = "Поиск"

        val searchPlateId = searchView.context.resources.getIdentifier("android:id/search_plate", null, null)
        val viewGroup = searchView.findViewById(searchPlateId) as ViewGroup
        viewGroup.setBackgroundColor(Color.parseColor("#1f1f1f"))
        searchView.isFocusable = false
        recyclerPhotostudio.visibility = View.INVISIBLE
        photostudioProgress.visibility = View.VISIBLE


    }

    lateinit var mapFragment:MapFragment

    override fun showMap() {

        mapFragment = MapFragment.newInstance(ArrayList((recyclerPhotostudio.adapter as PhotostudiosAdapter).list))
        if (mapContainer.visibility == View.GONE) {
            recyclerPhotostudio.visibility = View.GONE
            recyclerPhotostudio.scrollToPosition(0)
            mapContainer.visibility = View.VISIBLE
            imageView15.setImageDrawable(resources.getDrawable(R.drawable.ic_list))
            childFragmentManager.beginTransaction().add(R.id.mapContainer, mapFragment).commit()
        } else {
            recyclerPhotostudio.visibility = View.VISIBLE
            mapContainer.visibility = View.GONE
            imageView15.setImageDrawable(resources.getDrawable(R.drawable.ic_map_label))
            childFragmentManager.beginTransaction().remove(mapFragment).commitNow()
        }
    }

    override fun onStart() {
        super.onStart()
        photoStudiosPresenter.onStart(currentPage)


    }

    override fun showPhotostudios(list: MutableList<ContentItem?>, t: PhotostudiosResponse) {
        TOTAL_PAGES = t.totalPages!!
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).list = list
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).notifyDataSetChanged()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_studios, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_filter -> {
                photoStudiosPresenter.onClickedFilter()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun loadFirstPage() {
        if (currentPage <= TOTAL_ITEMS)
            (recyclerPhotostudio.adapter as PhotostudiosAdapter).addLoadingFooter()
        else
            isLast = true
    }

    override fun removeFooter() {
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).removeLoadingFooter()
    }

    private fun loadNextPage() {
        if (currentPage != TOTAL_PAGES)
            if (!(recyclerPhotostudio.adapter as PhotostudiosAdapter).isLoadingAdded)
                (recyclerPhotostudio.adapter as PhotostudiosAdapter).addLoadingFooter()
        photoStudiosPresenter.loadNextPage(currentPage, query)
    }

    override fun hideProgressLoading() {
        photostudioProgress.visibility = View.GONE
        recyclerPhotostudio.visibility = View.VISIBLE
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showValueLike(item: ContentItem) {
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).setStateLike(item)
    }

    override fun showPhotostudiosClear(list: MutableList<ContentItem?>, t: PhotostudiosResponse) {
        TOTAL_PAGES = t.totalPages!!
        currentPage = 0
        isLoad = false
        isLast = false
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).list.clear()
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).notifyDataSetChanged()
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).list = list
        (recyclerPhotostudio.adapter as PhotostudiosAdapter).notifyDataSetChanged()

        try {
            listener.onSearch(list)
        }catch (ex:Exception){

        }
    }

    override fun showCurrentUser(url: String, name: String) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(currentUserImg)

        nameCurrentUser.text = name
    }

    override fun showEmail(email: String) {
        nameCurrentUser.text = email
    }

    fun setMap(listener: MapContract.OnSearchListener) {
        this.listener = listener
    }

    override fun showNextPage(t: PhotostudiosResponse) {
        (recyclerPhotostudio.adapter as PhotostudiosAdapter)
                .removeLoadingFooter()
        isLoad = false

        (recyclerPhotostudio.adapter as PhotostudiosAdapter)
                .addAll(t.content)

        if (currentPage != TOTAL_PAGES)
            (recyclerPhotostudio.adapter as PhotostudiosAdapter)
                    .addLoadingFooter()
        else
            isLast = true
    }
}