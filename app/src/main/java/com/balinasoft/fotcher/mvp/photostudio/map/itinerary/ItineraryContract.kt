package com.balinasoft.fotcher.mvp.photostudio.map.itinerary

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.itinerary.ItineraryResponse
import com.balinasoft.fotcher.entity.photostudios.ContentItem
import com.google.android.gms.maps.model.Marker


@StateStrategyType(OneExecutionStateStrategy::class)
interface ItineraryContract {
    interface View : MvpView {
        fun hideKeyboard()
        @StateStrategyType(SkipStrategy::class)
        fun showToast(text: String?)

        fun showMarker(m: Marker)
        fun showInfo(data: ContentItem)
        fun showDisableMarker(marker: Marker)
        fun disableStudioInfo()
        fun enableStudioInfo()
        @StateStrategyType(SkipStrategy::class)
        fun showItinerary(t: ItineraryResponse)
    }
}