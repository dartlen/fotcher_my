package com.balinasoft.fotcher.mvp.dialogs.dialog.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.inflate
import com.balinasoft.fotcher.entity.dialog.pojo.AttachmentsItem
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.item_image.view.*
import kotlinx.android.synthetic.main.item_image_my.view.*
import kotlinx.android.synthetic.main.item_message.view.*
import kotlinx.android.synthetic.main.item_message_my.view.*
import java.text.ParseException
import java.text.SimpleDateFormat

class DialogAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {

            MESSAGE -> (holder as ViewHolderMessage).bind(list[position]!!, image!!)
            MESSAGE_MY -> (holder as ViewHolderMessageMy).bind(list[position]!!)
            IMAGE -> (holder as ViewHolderImage).bind(list[position]!!, image!!)
            IMAGE_MY -> (holder as ViewHolderImageMy).bind(list[position]!!)
        }
    }

    var list: MutableList<DialogResponse?> = mutableListOf()
    var image: String? = null
    var retryPageLoad = false
    var errorMsg: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            MESSAGE -> ViewHolderMessage(parent.inflate(R.layout.item_message))
            MESSAGE_MY -> ViewHolderMessageMy(parent.inflate(R.layout.item_message_my))
            IMAGE -> ViewHolderImage(parent.inflate(R.layout.item_image))
            IMAGE_MY -> ViewHolderImageMy(parent.inflate(R.layout.item_image_my))
            else -> LoadingVH(parent.inflate(R.layout.item_message))
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    var isLoadingAdded = false

    val isEmpty: Boolean
        get() = itemCount == 0

    override fun getItemViewType(position: Int): Int {
        if (list[position]!!.my!!) {
            if (list[position]!!.attachments!!.isNotEmpty() && list[position]!!.text == "")
                return IMAGE_MY
            else if (list[position]!!.attachments!!.isEmpty()) {
                return MESSAGE_MY
            } else {

            }
        } else {
            if (list[position]!!.attachments!!.isNotEmpty() && list[position]!!.text == "")
                return IMAGE
            else if (list[position]!!.attachments!!.isEmpty()) {
                return MESSAGE
            } else {

            }
        }
        return 4
    }
    /*
   Helpers
   _________________________________________________________________________________________________
    */

    fun add(r: DialogResponse) {
        list.add(r)
        notifyItemInserted(list.size - 1)
    }

    fun addAll(moveResults: List<DialogResponse?>?) {
        for (result in moveResults!!) {
            add(result!!)
        }
    }

    fun remove(r: DialogResponse?) {
        val position = list.indexOf(r)
        if (position > -1) {
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = list.size - 1
        val result = getItem(position)

        if (result != null) {
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): DialogResponse? {
        return list[position]
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(list.size - 1)

        if (errorMsg != null) this.errorMsg = errorMsg
    }

    fun setImageDialog(url: String) {
        this.image = url
    }

    fun addNewMessage(t: SendMessageResponse) {
        list.reverse()
        list.add(DialogResponse(date = t.date, readed = t.readed,
                attachments = t.attachments ?: arrayListOf<AttachmentsItem>(), id = t.id,
                text = t.text, my = t.my))
        list.reverse()
        notifyDataSetChanged()
    }

    fun showBottom(listener: (Int) -> Unit) {
        listener(list.size)
    }
    /*
   View Holders
   _________________________________________________________________________________________________
    */

    class LoadingVH(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        private val MESSAGE = 0
        private val MESSAGE_MY = 1
        private val IMAGE = 2
        private val IMAGE_MY = 3
    }
}

class ViewHolderMessage(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n", "NewApi", "SimpleDateFormat")
    fun bind(item: DialogResponse, url: String) {
        itemView.textMessage.text = item.text

        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val format2 = SimpleDateFormat("HH:mm")
        try {
            val date = format.parse(item.date)
            val date2 = format2.format(date)
            itemView.timeMessage.text = date2
            Log.d(date2.toString(), "")

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val options = RequestOptions()

        Glide.with(itemView.context)
                .load(url)
                .apply(options)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.messageAvatar)

        if (item.text!!.length >= 4)
        else
            itemView.textMessage.setPadding(36, 27, 100, 64)
    }
}

class ViewHolderMessageMy(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n", "NewApi", "SimpleDateFormat")
    fun bind(item: DialogResponse) {
        itemView.textViewMessageMy.text = item.text

        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val format2 = SimpleDateFormat("HH:mm")
        try {
            val date = format.parse(item.date)
            val date2 = format2.format(date)
            itemView.textViewMessageMyTime.text = date2
            Log.d(date2.toString(), "")

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        if (item.text!!.length >= 4)
        else
            itemView.textViewMessageMy.setPadding(36, 27, 100, 64)
    }
}

class ViewHolderImage(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n", "NewApi")
    fun bind(item: DialogResponse, url: String) {
        Glide.with(itemView.context)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(itemView.avatarMessage)

        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))

        if (item.attachments!![0] is LinkedTreeMap<*, *>) {
            val x = (item.attachments[0] as LinkedTreeMap<*, *>)["url"]
            Glide.with(itemView.context)
                    .load(x)
                    .apply(options)
                    .into(itemView.imageMessage)
        } else {
            val x2 = (item.attachments[0] as AttachmentsItem).url
            Glide.with(itemView.context)
                    .load(x2)
                    .apply(options)
                    .into(itemView.imageMessage)
        }
    }
}

class ViewHolderImageMy(view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SetTextI18n", "NewApi")
    fun bind(item: DialogResponse) {
        val options = RequestOptions()
        options.transforms(CenterCrop(), RoundedCorners(6))
        if (item.attachments!![0] is LinkedTreeMap<*, *>) {
            Glide.with(itemView.context)
                    .load((item.attachments[0] as LinkedTreeMap<*, *>)["url"])
                    .apply(options)
                    .into(itemView.imageMyMessage)
        } else {
            Glide.with(itemView.context)
                    .load((item.attachments[0] as AttachmentsItem).url)
                    .apply(options)
                    .into(itemView.imageMyMessage)
        }
    }
}