package com.balinasoft.fotcher.mvp.model.models


import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERMODELS
import com.balinasoft.fotcher.Constants.PROFILEMODEL
import com.balinasoft.fotcher.entity.models.pojo.ContentItem
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse
import com.balinasoft.fotcher.model.interactor.models.ModelsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ModelsPresenter @Inject constructor(
        private val router: Router,
        private val modelsInteractor: ModelsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ModelsContract.View>() {

    fun onClickedFilter() {
        viewState.hideKeyboard()
        router.navigateTo(FILTERMODELS, 0)
    }

    fun onStart(currentPage: Int) {
        if (currentPage == 0 || modelsInteractor.getFlagReload())
            modelsInteractor.onStart()!!.subscribeOn(appSchedulers.io())
                    .observeOn(appSchedulers.ui())
                    .subscribe(
                            { t: ModelsResponse? ->
                                if (!modelsInteractor.getFlagReload()) {
                                    viewState.showModels(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                } else {
                                    modelsInteractor.setFlagReload()
                                    viewState.showModelsClear(t!!.content!!.toMutableList(), t)
                                    viewState.hideProgressLoading()
                                }
                            },
                            { _ ->
                                Log.d("", "")
                            })
    }

    fun onClickedModel(item: ContentItem) {
        viewState.hideKeyboard()
        router.navigateTo(PROFILEMODEL, arrayListOf(item.id, 0))
    }

    fun onInputedDataSearch(text: String) {

        val dispose = modelsInteractor.onStartSearch(text)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe(
                        { t: ModelsResponse? ->
                            modelsInteractor.setFlagReload()
                            viewState.showModelsClear(t!!.content!!.toMutableList(), t)
                            viewState.hideProgressLoading()

                        },
                        { _ ->
                            Log.d("", "")
                        })
        unsubscribeOnDestroy(dispose)
    }

    fun loadNextPage(currentPage:Int, query:String){
        modelsInteractor.loadNextPage(currentPage, query)
                .subscribeOn(appSchedulers.io()).observeOn(appSchedulers.ui())
                .subscribe(
                        { t: ModelsResponse? ->
                            viewState.showModels(t!!)
                        },
                        { _: Throwable? ->

                        })
    }

}
