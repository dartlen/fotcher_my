package com.balinasoft.fotcher.mvp.dialogs.dialog

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse

@StateStrategyType(SkipStrategy::class)
interface DialogContract {
    interface View : MvpView {
        fun hideKeyboard()
        fun showToast(text: String?)
        @StateStrategyType(SkipStrategy::class)
        fun showDialog(t: List<DialogResponse>)

        fun hideProgressLoading()
        fun clearOldMessage()
        @StateStrategyType(SkipStrategy::class)
        fun selectImage()

        @StateStrategyType(SkipStrategy::class)
        fun loadFirstPage()

        fun showLoadedMessages(t: List<DialogResponse>, isLoad: Boolean)
        fun showNewMessage(t: SendMessageResponse)
    }
}