package com.balinasoft.fotcher.mvp.filterparams

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants.FILTERPHOTOGRAPHERS
import com.balinasoft.fotcher.Constants.FILTERPHOTOSTUDIOS
import com.balinasoft.fotcher.entity.filterphotographers.OptionsEntity
import com.balinasoft.fotcher.entity.filterphotographers.ParamsEntity
import com.balinasoft.fotcher.model.interactor.filterparams.FilterParamsInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FilterParamsPresenter @Inject constructor(
        private val router: Router,
        private val filtersParamsInteractor: FilterParamsInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<FilterParamsContract.View>() {

    private var paramsEntity: ParamsEntity? = null
    private var paramsEntity2: com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity? = null

    fun onClickedBack() {
        if (paramsEntity != null) {
            val d = filtersParamsInteractor.saveOptionsEntity(paramsEntity!!).observeOn(appSchedulers.ui()).subscribe({ _ ->
                router.backTo(FILTERPHOTOGRAPHERS)
            }, { _ ->

            })
            unsubscribeOnDestroy(d)
        } else {
            val d = filtersParamsInteractor.saveOptionsPhotostudiosEntity(paramsEntity2!!).observeOn(appSchedulers.ui()).subscribe({ _ ->
                router.backTo(FILTERPHOTOSTUDIOS)
            }, { _ ->

            })
            unsubscribeOnDestroy(d)
        }
    }

    fun onClickedItem(item: OptionsEntity) {
        for (i in 0 until paramsEntity!!.options!!.size) {
            if (this.paramsEntity!!.options!![i]!! == item)
                paramsEntity!!.options!![i]!!.check = !paramsEntity!!.options!![i]!!.check!!
        }
    }

    fun onClickedItem(item: com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity) {
        for (i in 0 until paramsEntity2!!.options!!.size) {
            if (this.paramsEntity2!!.options!![i]!! == item)
                paramsEntity2!!.options!![i]!!.check = !paramsEntity2!!.options!![i]!!.check!!
        }
    }

    fun setParams(paramsEntity: ParamsEntity) {
        this.paramsEntity = paramsEntity
    }

    fun setParams(paramsEntity: com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity) {
        this.paramsEntity2 = paramsEntity
    }

}