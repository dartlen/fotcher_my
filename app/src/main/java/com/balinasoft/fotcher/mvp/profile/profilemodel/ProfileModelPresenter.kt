package com.balinasoft.fotcher.mvp.profile.profilemodel

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.Constants.GALLERY
import com.balinasoft.fotcher.Constants.INFOPHOTOGRAPHER
import com.balinasoft.fotcher.Constants.MODELPRAMS
import com.balinasoft.fotcher.entity.user.Element
import com.balinasoft.fotcher.entity.user.ParamsEntity
import com.balinasoft.fotcher.model.interactor.profilemodel.ProfileModelInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ProfileModelPresenter @Inject constructor(
        private val router: Router,
        private val profilePhotgrapherInteractor: ProfileModelInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<ProfileModelContract.View>() {
    var tmpEments: MutableList<Element>? = null
    fun onStart(userId: Int) {
        profilePhotgrapherInteractor.onStart(userId)!!.subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t ->
                    t.params!!.forEach {
                        if (it!!.type == "GROUP") {
                            tmpEments = it.elements
                        }
                    }
                    viewState.showUser(t)
                    viewState.hideProgress()
                    Log.d("", "")
                }, { _ ->
                    Log.d("", "")
                })
    }

    fun onClickedParams(paramsEntity: ParamsEntity) {
        if (paramsEntity.caption == "Параметры")
            router.navigateTo(MODELPRAMS, ArrayList(tmpEments))
        else
            router.navigateTo(INFOPHOTOGRAPHER, paramsEntity)
    }

    fun onClickedGallery(data: ArrayList<Int>) {
        router.navigateTo(GALLERY, data)
    }

    fun onClickedOptions() {
        router.navigateTo(Constants.PROFILEOPTION, "")
    }

    fun onClickedDialog(id: Int) {
        val dispose = profilePhotgrapherInteractor.getUser(id).subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .subscribe({ t -> router.navigateTo(Constants.DIALOG, t) }, {})
        unsubscribeOnDestroy(dispose)
    }
}