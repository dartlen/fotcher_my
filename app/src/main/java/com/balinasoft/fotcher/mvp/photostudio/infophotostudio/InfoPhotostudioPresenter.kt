package com.balinasoft.fotcher.mvp.photostudio.infophotostudio

import com.arellomobile.mvp.InjectViewState
import com.balinasoft.fotcher.model.interactor.infophotostudio.InfoPhotostudioInteractor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.base.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class InfoPhotostudioPresenter @Inject constructor(
        private val router: Router,
        private val infoInteractor: InfoPhotostudioInteractor,
        private val appSchedulers: AppSchedulers
) : BasePresenter<InfoPhotostudioContract.View>() {

}