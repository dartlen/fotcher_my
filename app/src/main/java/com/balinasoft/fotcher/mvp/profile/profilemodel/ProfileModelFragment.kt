package com.balinasoft.fotcher.mvp.profile.profilemodel

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.entity.user.ParamsEntity
import com.balinasoft.fotcher.entity.user.UserEntity
import com.balinasoft.fotcher.mvp.profile.profilemodel.adapters.ProfileModelAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.internal.LinkedTreeMap
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile_model.*
import javax.inject.Inject
import javax.inject.Provider

class ProfileModelFragment : MvpAppCompatFragment(), ProfileModelContract.View {
    @Suppress("UNCHECKED_CAST")
    companion object {
        fun newInstance(idUser: Any): ProfileModelFragment {
            val fragment = ProfileModelFragment()
            val args = Bundle()
            args.putIntegerArrayList("list", idUser as ArrayList<Int>)
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    lateinit var presenterProvider: Provider<ProfileModelPresenter>

    @InjectPresenter
    lateinit var profileModelPresenter: ProfileModelPresenter

    @ProvidePresenter
    fun providePresenter(): ProfileModelPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        val view = inflater.inflate(R.layout.fragment_profile_model, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_profile_model)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)
        toolbar_profile_model.setNavigationOnClickListener(
                { _ -> (activity as AppCompatActivity).onBackPressed() })
        (activity as AppCompatActivity).drawer_layout
                .setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerProfilePhotographer.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerProfilePhotographer.addItemDecoration(itemDecor)
        recyclerProfilePhotographer.adapter = ProfileModelAdapter(context,
                { Item: ParamsEntity -> profileModelPresenter.onClickedParams(Item) })

        oneImage.setOnClickListener {
            profileModelPresenter.onClickedGallery(arguments!!.getIntegerArrayList("list"))
        }

        if (arguments!!.getIntegerArrayList("list")[1] == 1) {
            floatingActionButton2.visibility = View.GONE
        }

        floatingActionButton2.setOnClickListener {
            profileModelPresenter.onClickedDialog(arguments!!.getIntegerArrayList("list")[0])
        }

    }

    override fun onStart() {
        super.onStart()
        profileModelPresenter.onStart(arguments!!.getIntegerArrayList("list")[0])
        scrollView.isFocusableInTouchMode = true
        scrollView.descendantFocusability = ViewGroup.FOCUS_BEFORE_DESCENDANTS
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (arguments!!.getIntegerArrayList("list")[1] == 1)
            inflater!!.inflate(R.menu.menu_profile_photographers, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.menu_profile_photgrapher_settings -> {
                profileModelPresenter.onClickedOptions()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SetTextI18n")
    @Suppress("UNCHECKED_CAST")
    override fun showUser(userEntity: UserEntity) {
        val listforrecycler = arrayListOf<ParamsEntity>()
        for (i in userEntity.params!!) {
            if (i!!.name != "gender")
                listforrecycler.add(i)
        }
        (recyclerProfilePhotographer.adapter as ProfileModelAdapter).list = listforrecycler
        (recyclerProfilePhotographer.adapter as ProfileModelAdapter).notifyDataSetChanged()
        val options = RequestOptions()
        options.centerCrop()
        Glide.with(context!!)
                .load(userEntity.bigAvatar)
                .apply(options)
                .into(image)
        name.text = userEntity.name + " " + userEntity.surname
        comment.text = userEntity.leftStatus
        exp.text = userEntity.rightStatus

        if (userEntity.infoColumns != null) {
            if (userEntity.name != "") {
                one.text = (userEntity.infoColumns as ArrayList<LinkedTreeMap<*, *>>)[0]["value"] as String
                two.text = (userEntity.infoColumns)[1]["value"] as String
                three.text = (userEntity.infoColumns)[2]["value"] as String
            }
        }

        if (userEntity.lastPhotos!!.isNotEmpty()) {
            twoImage.visibility = View.VISIBLE
            val options2 = RequestOptions()
            options2.centerCrop()
            Glide.with(context!!)
                    .load(userEntity.lastPhotos[0]!!.small)
                    .apply(options2)
                    .into(twoImage)
        } else {
            twoImage.visibility = View.INVISIBLE
        }

        if (userEntity.lastPhotos.size >= 2) {
            threeImage.visibility = View.VISIBLE
            val options3 = RequestOptions()
            options3.centerCrop()
            Glide.with(context!!)
                    .load(userEntity.lastPhotos[1]!!.small)
                    .apply(options3)
                    .into(threeImage)
        } else {
            threeImage.visibility = View.INVISIBLE
        }
    }

    override fun hideProgress() {
        profileModelProgress.visibility = View.GONE
        image.visibility = View.VISIBLE
        recyclerProfilePhotographer.visibility = View.VISIBLE
        name.visibility = View.VISIBLE
        comment.visibility = View.VISIBLE
        exp.visibility = View.VISIBLE
        view3.visibility = View.VISIBLE
        one.visibility = View.VISIBLE
        textView13.visibility = View.VISIBLE
        two.visibility = View.VISIBLE
        textView15.visibility = View.VISIBLE
        three.visibility = View.VISIBLE
        threeImage.visibility = View.VISIBLE
        textView17.visibility = View.VISIBLE
        if (arguments!!.getIntegerArrayList("list")[1] != 1)
            floatingActionButton2.visibility = View.VISIBLE
        oneImage.visibility = View.VISIBLE
        twoImage.visibility = View.VISIBLE
        threeImage.visibility = View.VISIBLE
        textView6.visibility = View.VISIBLE
    }
}