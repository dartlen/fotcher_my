package com.balinasoft.fotcher.mvp.profile.profileoptions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.Constants.PERMISSION
import com.balinasoft.fotcher.FotcherApp
import com.balinasoft.fotcher.R
import com.balinasoft.fotcher.commons.FileUtils
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.mvp.profile.profileoptions.adapters.ProfileAdapter
import com.balinasoft.fotcher.mvp.profile.profileoptions.adapters.ProfileOptionAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_profileoptions.*
import javax.inject.Inject
import javax.inject.Provider

class ProfileOptionFragment : MvpAppCompatFragment(), ProfileOptionsContract.View {

    @InjectPresenter
    lateinit var profileOptionsPresenter: ProfileOptionsPresenter

    @Inject
    lateinit var presenterProvider: Provider<ProfileOptionsPresenter>

    @ProvidePresenter
    fun providePresenter(): ProfileOptionsPresenter {
        FotcherApp.componentsManager.fotcher().inject(this)
        return presenterProvider.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        FotcherApp.componentsManager.fotcher().inject(this)
        return inflater.inflate(R.layout.fragment_profileoptions, container,
                false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerProfileOptions.layoutManager = linearLayoutManager
        val itemDecor = DividerItemDecoration(activity, HORIZONTAL)
        recyclerProfileOptions.addItemDecoration(itemDecor)
        recyclerProfileOptions.adapter = ProfileOptionAdapter(context,
                { Item: ParamsItem ->
                    profileOptionsPresenter.onClickedParams(Item,
                            (recyclerProfile.adapter as ProfileAdapter).list
                    )
                })

        val linearLayoutManager2 = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerProfile.layoutManager = linearLayoutManager2
        recyclerProfile.adapter = ProfileAdapter(context,
                { item -> profileOptionsPresenter.onSaveEdite(item) },
                { item -> profileOptionsPresenter.onSaveSpinner(item) },
                { item -> profileOptionsPresenter.onSaveSeek(item) })

        floatingActionButton2.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"

            val packageManager = context!!.packageManager
            if (packageManager.queryIntentActivities(intent,
                            PackageManager.MATCH_DEFAULT_ONLY).size>0) {
                startActivityForResult(intent, Constants.SELECTIMAGE)
            }
        }
        accessLocationPermission()
    }

    private fun accessLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            val read = activity!!.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            val write = activity!!.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

            val listRequestPermission = java.util.ArrayList<String>()

            if (read != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            if (write != PackageManager.PERMISSION_GRANTED) {
                listRequestPermission.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }

            if (!listRequestPermission.isEmpty()) {
                val strRequestPermission = listRequestPermission.toTypedArray()
                requestPermissions(strRequestPermission, PERMISSION)
            }
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"

            val packageManager = context!!.packageManager
            if (packageManager.queryIntentActivities(intent,
                            PackageManager.MATCH_DEFAULT_ONLY).size>0) {
                startActivityForResult(intent, Constants.SELECTIMAGE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.SELECTIMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val file = FileUtils.getFile(context, data!!.data)
                profileOptionsPresenter.uploadImage(file)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbarProfileOptions)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        (activity as AppCompatActivity).supportActionBar?.title = resources.getText(R.string.nothing)

        toolbarProfileOptions.setNavigationOnClickListener(
                { _ ->
                    profileOptionsPresenter.onBack()
                })
        profileOptionsPresenter.onStart()

    }

    override fun onBack() {
        (activity as AppCompatActivity).onBackPressed()
    }

    fun View.hideKey(inputMethodManager: InputMethodManager) {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        view?.hideKey(imm)
    }

    override fun showToast(text: String?) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    override fun showParams(paramsItem: List<ParamsItem>) {
        (recyclerProfileOptions.adapter as ProfileOptionAdapter).list = ArrayList(paramsItem
                .filter { t -> t.type != "SELECT" }
                .filter { t -> t.type != "INT_RANGE" }
                .filter { t -> t.type != "STRING" })
        (recyclerProfileOptions.adapter as ProfileOptionAdapter).notifyDataSetChanged()

        (recyclerProfile.adapter as ProfileAdapter).list = ArrayList(paramsItem.filter { t -> t.type != "MULTI_SELECT" }.filter { t -> t.type != "GROUP" })
        (recyclerProfile.adapter as ProfileAdapter).notifyDataSetChanged()
    }

    override fun showAvatar(url: String) {
        val options = RequestOptions()
        options.centerCrop()
        Glide.with(context!!)
                .load(url)
                .apply(options)
                .into(imageView)
    }
}