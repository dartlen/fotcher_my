package com.balinasoft.fotcher.di.module.profile

import com.balinasoft.fotcher.model.interactor.profileoptions.ProfileOptionsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.profile.profileoptions.ProfileOptionsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ProfileOptionsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ProfileOptionsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, profileOptionsInteractor:ProfileOptionsInteractor, appSchedulers: AppSchedulers) = ProfileOptionsPresenter(router, profileOptionsInteractor, appSchedulers)
}