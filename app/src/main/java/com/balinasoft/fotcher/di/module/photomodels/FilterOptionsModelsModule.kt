package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.filteroptionsmodels.FilterOptionsModelsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.filteroptionsmodels.FilterOptionModelsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterOptionsModelsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FilterOptionsModelsInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router: Router, filterOptionsModelsInteractor:FilterOptionsModelsInteractor, appSchedulers: AppSchedulers) = FilterOptionModelsPresenter(router, filterOptionsModelsInteractor, appSchedulers)
}