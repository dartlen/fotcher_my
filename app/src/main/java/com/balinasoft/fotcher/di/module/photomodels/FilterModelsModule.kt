package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.filtermodels.FilterModelsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.filtermodels.FilterModelsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterModelsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FilterModelsInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router: Router, filterModelsInteractor:FilterModelsInteractor, appSchedulers: AppSchedulers) = FilterModelsPresenter(router, filterModelsInteractor, appSchedulers)
}