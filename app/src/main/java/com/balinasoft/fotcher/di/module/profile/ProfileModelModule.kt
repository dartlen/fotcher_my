package com.balinasoft.fotcher.di.module.profile

import com.balinasoft.fotcher.model.interactor.profilemodel.ProfileModelInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.profile.profilemodel.ProfileModelPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ProfileModelModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ProfileModelInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, profileModelInteractor: ProfileModelInteractor, appSchedulers: AppSchedulers) = ProfileModelPresenter(router, profileModelInteractor, appSchedulers)
}