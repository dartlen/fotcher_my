package com.balinasoft.fotcher.di.module.auth

import com.balinasoft.fotcher.model.interactor.signup.SignUpInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.auth.signup.SignUpPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class SignUpModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = SignUpInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, signUpInteractor:SignUpInteractor, appSchedulers: AppSchedulers) = SignUpPresenter(router, signUpInteractor, appSchedulers)
}