package com.balinasoft.fotcher.di.module.gallery

import com.balinasoft.fotcher.model.interactor.gallery.GalleryInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.gallery.GalleryPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class GalleryModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = GalleryInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, galleryInteractor:GalleryInteractor, appSchedulers: AppSchedulers) = GalleryPresenter(router, galleryInteractor, appSchedulers)
}