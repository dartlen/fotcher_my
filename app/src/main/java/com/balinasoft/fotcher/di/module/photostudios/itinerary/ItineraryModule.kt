package com.balinasoft.fotcher.di.module.photostudios.itinerary

import com.balinasoft.fotcher.model.interactor.itinerary.ItineraryInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.map.itinerary.ItineraryPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ItineraryModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ItineraryInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, itineraryInteractor:ItineraryInteractor, appSchedulers: AppSchedulers) = ItineraryPresenter(router, itineraryInteractor, appSchedulers)
}