package com.balinasoft.fotcher.di.module.photographers


import com.balinasoft.fotcher.model.interactor.infophotographer.InfoPhotographerInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photographer.infophotographer.InfoPhotographerPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class InfoPhotographerModule {
    @Provides
    fun providerPhotographerInteractor(repository: Repository, appSchedulers: AppSchedulers) = InfoPhotographerInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, infoPhotographerInteractor:InfoPhotographerInteractor, appSchedulers: AppSchedulers) = InfoPhotographerPresenter(router, infoPhotographerInteractor, appSchedulers)
}