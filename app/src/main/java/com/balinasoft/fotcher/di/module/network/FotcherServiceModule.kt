package com.balinasoft.fotcher.di.module.network

import com.balinasoft.fotcher.di.qualifier.Fotcher
import com.balinasoft.fotcher.model.data.server.FotcherService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class FotcherServiceModule {
    @Provides
    @Singleton
    @Fotcher
    fun provideApiInterface(@Fotcher retrofit: Retrofit): FotcherService {
        return retrofit.create(FotcherService::class.java)
    }
}