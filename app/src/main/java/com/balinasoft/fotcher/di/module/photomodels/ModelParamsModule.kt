package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.modelparams.ModelParamsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.modelparams.ModelParamsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ModelParamsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ModelParamsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, modelParamsInteractor:ModelParamsInteractor, appSchedulers: AppSchedulers) = ModelParamsPresenter(router, modelParamsInteractor, appSchedulers)
}