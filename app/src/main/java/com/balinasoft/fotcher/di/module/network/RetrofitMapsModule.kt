package com.balinasoft.fotcher.di.module.network

import com.balinasoft.fotcher.Constants
import com.balinasoft.fotcher.di.qualifier.Maps
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitMapsModule {
    @Provides
    @Singleton
    @Maps
    fun providesRetrofitMaps(client: OkHttpClient): Retrofit = Retrofit.Builder().baseUrl(Constants.MAPS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
}