package com.balinasoft.fotcher.di.component

import com.balinasoft.fotcher.di.module.*
import com.balinasoft.fotcher.di.module.app.AppModule
import com.balinasoft.fotcher.di.module.app.NavigationModule
import com.balinasoft.fotcher.di.module.network.*
import com.balinasoft.fotcher.di.module.repository.RepositoryModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class,
        RetrofitModule::class,
        OkHttpModule::class,
        NavigationModule::class,
        RepositoryModule::class,
        FotcherServiceModule::class,
        MapsServiceModule::class,
        RetrofitMapsModule::class))
interface AppComponent {
    fun plusFotcherComponent(fotcherModule: FotcherModule): FotcherComponent
}