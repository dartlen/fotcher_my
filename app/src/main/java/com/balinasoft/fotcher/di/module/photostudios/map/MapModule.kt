package com.balinasoft.fotcher.di.module.photostudios.map

import com.balinasoft.fotcher.model.interactor.map.MapInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.map.map.MapPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class MapModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = MapInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, mapInteractor:MapInteractor, appSchedulers: AppSchedulers) = MapPresenter(router, mapInteractor, appSchedulers)
}