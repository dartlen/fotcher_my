package com.balinasoft.fotcher.di.component

import com.balinasoft.fotcher.di.module.*
import com.balinasoft.fotcher.di.module.auth.RecoveryModule
import com.balinasoft.fotcher.di.module.auth.SignInModule
import com.balinasoft.fotcher.di.module.auth.SignUpModule
import com.balinasoft.fotcher.di.module.auth.VerificationModule
import com.balinasoft.fotcher.di.module.dialogs.DialogModule
import com.balinasoft.fotcher.di.module.dialogs.MessagesModule
import com.balinasoft.fotcher.di.module.photostudios.favorites.FavoritesModule
import com.balinasoft.fotcher.di.module.gallery.GalleryModule
import com.balinasoft.fotcher.di.module.glide.MyAppGlideModule
import com.balinasoft.fotcher.di.module.params.FilterParamsModule
import com.balinasoft.fotcher.di.module.photographers.FilterPhotographersModule
import com.balinasoft.fotcher.di.module.photographers.InfoPhotographerModule
import com.balinasoft.fotcher.di.module.photographers.PhotographersModule
import com.balinasoft.fotcher.di.module.photomodels.*
import com.balinasoft.fotcher.di.module.photostudios.*
import com.balinasoft.fotcher.di.module.photostudios.itinerary.ItineraryModule
import com.balinasoft.fotcher.di.module.photostudios.map.MapModule
import com.balinasoft.fotcher.di.module.profile.*
import com.balinasoft.fotcher.di.module.reservations.ReservationsModule
import com.balinasoft.fotcher.di.module.reservations.ReserveModule
import com.balinasoft.fotcher.mvp.activity.MainActivity
import com.balinasoft.fotcher.mvp.comments.CommentsFragment
import com.balinasoft.fotcher.mvp.dialogs.dialog.DialogFragment
import com.balinasoft.fotcher.mvp.photostudio.favorites.FavoritesFragment
import com.balinasoft.fotcher.mvp.model.filtermodels.FilterModelsFragment
import com.balinasoft.fotcher.mvp.model.filteroptionsmodels.FilterOptionModelsFragment
import com.balinasoft.fotcher.mvp.filterparams.FilterParamsFragment
import com.balinasoft.fotcher.mvp.model.filterparamsmodel.FilterParamsModelsFragment
import com.balinasoft.fotcher.mvp.photographer.filterphotographers.FilterPhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.filterphotostudios.FilterPhotoStudiosFragment
import com.balinasoft.fotcher.mvp.gallery.GalleryFragment
import com.balinasoft.fotcher.mvp.photographer.infophotographer.InfoPhotographerFragment
import com.balinasoft.fotcher.mvp.photostudio.infophotostudio.InfoPhotostudioFragment
import com.balinasoft.fotcher.mvp.infoprofileoption.InfoProfileOptionFragment
import com.balinasoft.fotcher.mvp.photostudio.map.itinerary.ItineraryFragment
import com.balinasoft.fotcher.mvp.photostudio.map.map.MapFragment
import com.balinasoft.fotcher.mvp.dialogs.messages.MessagesFragment
import com.balinasoft.fotcher.mvp.model.modelparams.ModelParamsFragment
import com.balinasoft.fotcher.mvp.model.models.ModelsFragment
import com.balinasoft.fotcher.mvp.model.optionsmodel.OptionsModelFragment
import com.balinasoft.fotcher.mvp.photographer.photographers.PhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.photostudios.PhotoStudiosFragment
import com.balinasoft.fotcher.mvp.profile.profilemodel.ProfileModelFragment
import com.balinasoft.fotcher.mvp.profile.profileoptions.ProfileOptionFragment
import com.balinasoft.fotcher.mvp.profile.profilephotographer.ProfilePhotographerFragment
import com.balinasoft.fotcher.mvp.profile.profilephotostudio.ProfilePhotostudioFragment
import com.balinasoft.fotcher.mvp.auth.recovery.RecoveryFragment
import com.balinasoft.fotcher.mvp.reservation.reservations.ReservationsFragment
import com.balinasoft.fotcher.mvp.reservation.reserve.ReserveFragment
import com.balinasoft.fotcher.mvp.auth.signin.SignInFragment
import com.balinasoft.fotcher.mvp.auth.signup.SignUpFragment
import com.balinasoft.fotcher.mvp.auth.verification.VerificationFragment
import dagger.Subcomponent

@Subcomponent(modules = [(FotcherModule::class), (SignInModule::class), (SignUpModule::class),
    (RecoveryModule::class), (PhotoStudiosModule::class), (ReservationsModule::class),
    (ProfilePhotographerModule::class), (MessagesModule::class), (ModelsModule::class),
    (PhotographersModule::class), (FilterPhotoStudiosModule::class), (VerificationModule::class),
    (FilterPhotographersModule::class), (FilterParamsModule::class), (GalleryModule::class),
    (InfoPhotographerModule::class), (InfoPhotostudioModule::class), (ProfilePhotostudioModule::class),
    (ReserveModule::class),
    (CommentsModule::class), (FilterModelsModule::class), (FilterParamsModelsModule::class),
    (FilterOptionsModelsModule::class), (ProfileModelModule::class), (ProfileOptionsModule::class),
    (InfoProfileOptionModule::class), (OptionsModelModule::class), (DialogModule::class),
    (MapModule::class), (FavoritesModule::class), (ModelParamsModule::class), (ItineraryModule::class)])
interface FotcherComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(signInFragment: SignInFragment)
    fun inject(signUpFragment: SignUpFragment)
    fun inject(recoveryFragment: RecoveryFragment)
    fun inject(photoStudiosFragment: PhotoStudiosFragment)
    fun inject(reservationsFragment: ReservationsFragment)
    fun inject(profilePhotographerFragment: ProfilePhotographerFragment)
    fun inject(messagesFragment: MessagesFragment)
    fun inject(modelsFragment: ModelsFragment)
    fun inject(photographersFragment: PhotographersFragment)
    fun inject(filterPhotoStudiosFragment: FilterPhotoStudiosFragment)
    fun inject(verificationFragment: VerificationFragment)
    fun inject(filterPhotographersFragment: FilterPhotographersFragment)
    fun inject(filterParamsFragment: FilterParamsFragment)
    fun inject(galleryFragment: GalleryFragment)
    fun inject(infoPhotographerFragment: InfoPhotographerFragment)
    fun inject(profilePhotostudioFragment: ProfilePhotostudioFragment)
    fun inject(reserveFragment: ReserveFragment)
    fun inject(commentsFragment: CommentsFragment)
    fun inject(infoPhotostudioFragment: InfoPhotostudioFragment)
    fun inject(filterModelsFragment: FilterModelsFragment)
    fun inject(filterParamsModelsFragment: FilterParamsModelsFragment)
    fun inject(filterOptionModelsFragment: FilterOptionModelsFragment)
    fun inject(profileModelFragment: ProfileModelFragment)
    fun inject(profileOptionFragment: ProfileOptionFragment)
    fun inject(infoProfileOptionFragment: InfoProfileOptionFragment)
    fun inject(optionsModelFragment: OptionsModelFragment)
    fun inject(dialogFragment: DialogFragment)
    fun inject(glideModule: MyAppGlideModule)
    fun inject(mapFragment: MapFragment)
    fun inject(favoritesFragment: FavoritesFragment)
    fun inject(modelParamsFragment: ModelParamsFragment)
    fun inject(itineraryFragment: ItineraryFragment)
}