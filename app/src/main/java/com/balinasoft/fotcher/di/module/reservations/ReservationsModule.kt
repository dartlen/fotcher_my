package com.balinasoft.fotcher.di.module.reservations

import com.balinasoft.fotcher.model.interactor.reservations.ReservationsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.reservation.reservations.ReservationsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ReservationsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ReservationsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, reservationsInteractor:ReservationsInteractor, appSchedulers: AppSchedulers) = ReservationsPresenter(router, reservationsInteractor, appSchedulers)
}