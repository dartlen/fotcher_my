package com.balinasoft.fotcher.di.module.auth

import com.balinasoft.fotcher.model.interactor.recovery.RecoveryInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.auth.recovery.RecoveryPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class RecoveryModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = RecoveryInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, recoveryInteractor:RecoveryInteractor, appSchedulers: AppSchedulers) = RecoveryPresenter(router, recoveryInteractor, appSchedulers)
}