package com.balinasoft.fotcher.di.module.app

import android.content.Context
import android.content.SharedPreferences
import com.balinasoft.fotcher.model.JobExecutor
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.model.system.ThreadExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {
    lateinit var cont: Context
    @Provides
    fun application(): Context {
        cont = context
        return cont
    }

    @Provides
    @Singleton
    fun provideSharedPreference(): SharedPreferences = context.getSharedPreferences("shared pref", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun providerSchedulersProvider(): AppSchedulers {
        return AppSchedulers()
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }
}