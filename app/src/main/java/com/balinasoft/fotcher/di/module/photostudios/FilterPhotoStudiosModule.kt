package com.balinasoft.fotcher.di.module.photostudios

import com.balinasoft.fotcher.model.interactor.filterphotostudios.FilterPhotoStudiosInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.filterphotostudios.FilterPhotoStudiosPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterPhotoStudiosModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FilterPhotoStudiosInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, filterPhotoStudiosInteractor:FilterPhotoStudiosInteractor, appSchedulers: AppSchedulers) = FilterPhotoStudiosPresenter(router, filterPhotoStudiosInteractor, appSchedulers)
}