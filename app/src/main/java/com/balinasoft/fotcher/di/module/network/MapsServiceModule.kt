package com.balinasoft.fotcher.di.module.network

import com.balinasoft.fotcher.di.qualifier.Maps
import com.balinasoft.fotcher.model.data.server.MapsService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class MapsServiceModule {

    @Provides
    @Singleton
    @Maps
    fun provideApiInterfaceMaps(@Maps retrofit: Retrofit): MapsService {
        return retrofit.create(MapsService::class.java)
    }

}