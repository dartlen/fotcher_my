package com.balinasoft.fotcher.di.module.photostudios.favorites

import com.balinasoft.fotcher.model.interactor.favorites.FavoritesInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.favorites.FavoritesPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FavoritesModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FavoritesInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router: Router, favoritesInteractor:FavoritesInteractor, appSchedulers: AppSchedulers) = FavoritesPresenter(router, favoritesInteractor, appSchedulers)
}