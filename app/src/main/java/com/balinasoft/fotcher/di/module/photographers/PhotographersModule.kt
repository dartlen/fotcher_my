package com.balinasoft.fotcher.di.module.photographers

import com.balinasoft.fotcher.model.interactor.photographers.PhotographersInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photographer.photographers.PhotographersPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class PhotographersModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = PhotographersInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, photographersInteractor:PhotographersInteractor, appSchedulers: AppSchedulers) = PhotographersPresenter(router, photographersInteractor, appSchedulers)
}