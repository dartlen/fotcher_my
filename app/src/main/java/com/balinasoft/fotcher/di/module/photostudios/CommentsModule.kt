package com.balinasoft.fotcher.di.module.photostudios

import com.balinasoft.fotcher.model.interactor.comments.CommentsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.comments.CommentsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class CommentsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = CommentsInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router:Router, commentsInteractor:CommentsInteractor, appSchedulers: AppSchedulers) = CommentsPresenter(router, commentsInteractor, appSchedulers)
}