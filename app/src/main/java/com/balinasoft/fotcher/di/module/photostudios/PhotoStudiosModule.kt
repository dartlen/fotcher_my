package com.balinasoft.fotcher.di.module.photostudios

import com.balinasoft.fotcher.model.interactor.photostudios.PhotoStudiosInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.photostudios.PhotoStudiosPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class PhotoStudiosModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = PhotoStudiosInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, photoStudiosInteractor:PhotoStudiosInteractor, appSchedulers: AppSchedulers) = PhotoStudiosPresenter(router, photoStudiosInteractor, appSchedulers)
}