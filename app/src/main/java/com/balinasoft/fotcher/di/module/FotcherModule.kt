package com.balinasoft.fotcher.di.module

import com.balinasoft.fotcher.model.interactor.activity.MainActivityInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.activity.MainActivityPresenter
import com.balinasoft.fotcher.mvp.photostudio.favorites.FavoritesFragment
import com.balinasoft.fotcher.mvp.model.filtermodels.FilterModelsFragment
import com.balinasoft.fotcher.mvp.filterparams.FilterParamsFragment
import com.balinasoft.fotcher.mvp.photographer.filterphotographers.FilterPhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.filterphotostudios.FilterPhotoStudiosFragment
import com.balinasoft.fotcher.mvp.dialogs.messages.MessagesFragment
import com.balinasoft.fotcher.mvp.model.models.ModelsFragment
import com.balinasoft.fotcher.mvp.photographer.photographers.PhotographersFragment
import com.balinasoft.fotcher.mvp.photostudio.photostudios.PhotoStudiosFragment
import com.balinasoft.fotcher.mvp.profile.profileoptions.ProfileOptionFragment
import com.balinasoft.fotcher.mvp.profile.profilephotographer.ProfilePhotographerFragment
import com.balinasoft.fotcher.mvp.auth.recovery.RecoveryFragment
import com.balinasoft.fotcher.mvp.reservation.reservations.ReservationsFragment
import com.balinasoft.fotcher.mvp.auth.signin.SignInFragment
import com.balinasoft.fotcher.mvp.auth.signup.SignUpFragment
import com.balinasoft.fotcher.mvp.auth.verification.VerificationFragment
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FotcherModule {

    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = MainActivityInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router: Router, commentsInteractor:MainActivityInteractor, appSchedulers: AppSchedulers) = MainActivityPresenter(router, commentsInteractor, appSchedulers)

    @Provides
    fun provideSignInFragment() = SignInFragment()

    @Provides
    fun provideSignUpFragment() = SignUpFragment()

    @Provides
    fun provideRecoveryFragment() = RecoveryFragment()

    @Provides
    fun providePhotoStudiosFragment() = PhotoStudiosFragment()

    @Provides
    fun provideReservationsFragment() = ReservationsFragment()

    @Provides
    fun provideProfileFragment() = ProfilePhotographerFragment()

    @Provides
    fun provideMessagesFragment() = MessagesFragment()

    @Provides
    fun provideModelsFragment() = ModelsFragment()

    @Provides
    fun providePhotographersFragment() = PhotographersFragment()

    @Provides
    fun provideFilterPhotoStudiosFragment() = FilterPhotoStudiosFragment()

    @Provides
    fun provideVerificationFragment() = VerificationFragment()

    @Provides
    fun provideFilterPhotographersFragment() = FilterPhotographersFragment()

    @Provides
    fun provideFilterParamsFragment() = FilterParamsFragment()

    @Provides
    fun provideFilterModelsFragment() = FilterModelsFragment()

    @Provides
    fun providePofileOptionsFragment() = ProfileOptionFragment()

    @Provides
    fun provideFavoritesFragment() = FavoritesFragment()
}