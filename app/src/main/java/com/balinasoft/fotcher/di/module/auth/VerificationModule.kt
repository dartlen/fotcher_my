package com.balinasoft.fotcher.di.module.auth

import com.balinasoft.fotcher.model.interactor.verification.VerificationInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.auth.verification.VerificationPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class VerificationModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = VerificationInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, verificationInteractor:VerificationInteractor, appSchedulers: AppSchedulers) = VerificationPresenter(router, verificationInteractor, appSchedulers)
}