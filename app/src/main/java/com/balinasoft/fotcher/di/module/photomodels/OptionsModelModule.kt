package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.optionsmodel.OptionsModelInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.optionsmodel.OptionsModelPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class OptionsModelModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = OptionsModelInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, optionsModelInteractor:OptionsModelInteractor, appSchedulers: AppSchedulers) = OptionsModelPresenter(router, optionsModelInteractor, appSchedulers)
}