package com.balinasoft.fotcher.di.module.photostudios

import com.balinasoft.fotcher.model.interactor.infophotostudio.InfoPhotostudioInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photostudio.infophotostudio.InfoPhotostudioPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class InfoPhotostudioModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = InfoPhotostudioInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, infoPhotostudioInteractor:InfoPhotostudioInteractor, appSchedulers: AppSchedulers) = InfoPhotostudioPresenter(router, infoPhotostudioInteractor, appSchedulers)
}