package com.balinasoft.fotcher.di

import android.content.Context
import com.balinasoft.fotcher.di.component.AppComponent
import com.balinasoft.fotcher.di.component.DaggerAppComponent
import com.balinasoft.fotcher.di.component.FotcherComponent
import com.balinasoft.fotcher.di.module.app.AppModule
import com.balinasoft.fotcher.di.module.FotcherModule
import com.balinasoft.fotcher.di.module.network.OkHttpModule
import com.balinasoft.fotcher.di.module.network.RetrofitModule

class ComponentManager(context: Context) {
    val cont = context
    @get:JvmName("appContext_")
    lateinit var appComponent: AppComponent

    fun getAppComponent(): AppComponent {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(cont))
                .okHttpModule(OkHttpModule())
                .retrofitModule(RetrofitModule())
                .build()
        return appComponent
    }

    fun fotcher(): FotcherComponent {
        return appComponent.plusFotcherComponent(FotcherModule())
    }

}