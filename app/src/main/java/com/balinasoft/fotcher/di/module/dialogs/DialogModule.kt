package com.balinasoft.fotcher.di.module.dialogs

import com.balinasoft.fotcher.model.interactor.dialog.DialogInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.dialogs.dialog.DialogPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class DialogModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = DialogInteractor(repository, appSchedulers)

    @Provides
    fun providesPresenter(router: Router, dialogInteractor:DialogInteractor, appSchedulers: AppSchedulers) = DialogPresenter(router, dialogInteractor, appSchedulers)
}