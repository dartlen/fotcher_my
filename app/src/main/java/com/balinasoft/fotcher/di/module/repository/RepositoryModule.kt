package com.balinasoft.fotcher.di.module.repository

import android.content.SharedPreferences
import com.balinasoft.fotcher.di.qualifier.Fotcher
import com.balinasoft.fotcher.di.qualifier.Maps
import com.balinasoft.fotcher.model.cache.Cache
import com.balinasoft.fotcher.model.data.server.FotcherService
import com.balinasoft.fotcher.model.data.server.MapsService
import com.balinasoft.fotcher.model.mappers.FilterModelsEntityMapper
import com.balinasoft.fotcher.model.mappers.FilterPhotographersEntityMapper
import com.balinasoft.fotcher.model.mappers.FilterPhotostudiosEntityMapper
import com.balinasoft.fotcher.model.mappers.ProfilePhotographersEntityMapper
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.repository.local.LocalData
import com.balinasoft.fotcher.model.repository.remote.RemoteData
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideRepository(localData: LocalData, remoteData: RemoteData,
                          mapper: FilterPhotographersEntityMapper,
                          userEntityMapper: ProfilePhotographersEntityMapper,
                          filterPhotostudiosEntityMapper: FilterPhotostudiosEntityMapper,
                          filterModelsEntityMapper: FilterModelsEntityMapper): Repository {
        return Repository(localData, remoteData, mapper, userEntityMapper, filterPhotostudiosEntityMapper,
                filterModelsEntityMapper)
    }

    @Provides
    @Singleton
    fun providesLocalData(sharedPreferences: SharedPreferences, cache: Cache): LocalData {
        return LocalData(sharedPreferences, cache)
    }

    @Provides
    @Singleton
    fun providesRemoteData(@Fotcher fotcherService: FotcherService, @Maps mapsService: MapsService, @Fotcher retrofit: Retrofit): RemoteData {
        return RemoteData(fotcherService, mapsService, retrofit)
    }
}