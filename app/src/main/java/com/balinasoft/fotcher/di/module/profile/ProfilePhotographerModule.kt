package com.balinasoft.fotcher.di.module.profile

import com.balinasoft.fotcher.model.interactor.profilephotographer.ProfilePhotographerInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.profile.profilephotographer.ProfilePhotographerPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ProfilePhotographerModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ProfilePhotographerInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, profilePhotographerInteractor:ProfilePhotographerInteractor, appSchedulers: AppSchedulers) = ProfilePhotographerPresenter(router, profilePhotographerInteractor, appSchedulers)
}