package com.balinasoft.fotcher.di.module.network

import android.content.Context
import com.balinasoft.fotcher.BuildConfig
import com.balinasoft.fotcher.model.data.server.AuthHeaderInterceptor
import com.balinasoft.fotcher.model.repository.local.LocalData
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class OkHttpModule {
    @Provides
    @Singleton
    fun providesLogginInterceptor(localData: LocalData): Interceptor {
        return AuthHeaderInterceptor(localData)
    }

    @Provides
    @Singleton
    fun providesOkHttp(loggingInterceptor: Interceptor, context: Context): OkHttpClient {
        if (BuildConfig.DEBUG)
            return OkHttpClient.Builder()
                    .cache(Cache(context.cacheDir, 20 * 1024))
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(HttpLoggingInterceptor()
                            .setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build()
        else
            return OkHttpClient.Builder()
                    .cache(Cache(context.cacheDir, 20 * 1024))
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .build()
    }
}