package com.balinasoft.fotcher.di.module.dialogs

import com.balinasoft.fotcher.model.interactor.messages.MessagesInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.dialogs.messages.MessagesPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class MessagesModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = MessagesInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, messagesInteractor:MessagesInteractor, appSchedulers: AppSchedulers) = MessagesPresenter(router, messagesInteractor, appSchedulers)
}