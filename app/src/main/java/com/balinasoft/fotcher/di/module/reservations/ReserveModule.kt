package com.balinasoft.fotcher.di.module.reservations

import com.balinasoft.fotcher.model.interactor.reserve.ReserveInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.reservation.reserve.ReservePresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ReserveModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ReserveInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, reserveInteractor:ReserveInteractor, appSchedulers: AppSchedulers) = ReservePresenter(router, reserveInteractor, appSchedulers)
}