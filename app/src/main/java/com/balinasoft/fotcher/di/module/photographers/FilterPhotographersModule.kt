package com.balinasoft.fotcher.di.module.photographers

import com.balinasoft.fotcher.model.interactor.filterphotographers.FilterPhotographersInteractor
import com.balinasoft.fotcher.model.mappers.FilterPhotographersEntityMapper
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.photographer.filterphotographers.FilterPhotoStudiosPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterPhotographersModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers, mapper: FilterPhotographersEntityMapper) = FilterPhotographersInteractor(repository, appSchedulers, mapper)
    @Provides
    fun providesPresenter(router: Router, filterPhotographersInteractor:FilterPhotographersInteractor, appSchedulers: AppSchedulers) = FilterPhotoStudiosPresenter(router, filterPhotographersInteractor, appSchedulers)
}