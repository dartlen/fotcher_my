package com.balinasoft.fotcher.di.module.auth

import com.balinasoft.fotcher.model.interactor.signin.SignInInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.auth.signin.SignInPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class SignInModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = SignInInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, signInInteractor:SignInInteractor, appSchedulers: AppSchedulers) = SignInPresenter(router, signInInteractor, appSchedulers)
}