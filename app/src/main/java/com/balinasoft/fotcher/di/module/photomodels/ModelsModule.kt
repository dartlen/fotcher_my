package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.models.ModelsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.models.ModelsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ModelsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ModelsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, modelsInteractor:ModelsInteractor, appSchedulers: AppSchedulers) = ModelsPresenter(router, modelsInteractor, appSchedulers)
}