package com.balinasoft.fotcher.di.module.profile

import com.balinasoft.fotcher.model.interactor.profilephotostudio.ProfilePhotostudioInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.profile.profilephotostudio.ProfilePhotostudioPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ProfilePhotostudioModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = ProfilePhotostudioInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, profilePhotostudioInteractor: ProfilePhotostudioInteractor, appSchedulers: AppSchedulers) = ProfilePhotostudioPresenter(router, profilePhotostudioInteractor, appSchedulers)
}