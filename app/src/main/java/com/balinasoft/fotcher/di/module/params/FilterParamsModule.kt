package com.balinasoft.fotcher.di.module.params

import com.balinasoft.fotcher.model.interactor.filterparams.FilterParamsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.filterparams.FilterParamsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterParamsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FilterParamsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, filterParamsInteractor:FilterParamsInteractor, appSchedulers: AppSchedulers) = FilterParamsPresenter(router, filterParamsInteractor, appSchedulers)
}