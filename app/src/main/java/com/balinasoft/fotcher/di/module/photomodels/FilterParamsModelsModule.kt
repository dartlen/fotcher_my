package com.balinasoft.fotcher.di.module.photomodels

import com.balinasoft.fotcher.model.interactor.filterparamsmodels.FilterParamsModelsInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.model.filterparamsmodel.FilterParamsModelsPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class FilterParamsModelsModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = FilterParamsModelsInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, filterParamsModelsInteractor:FilterParamsModelsInteractor, appSchedulers: AppSchedulers) = FilterParamsModelsPresenter(router, filterParamsModelsInteractor, appSchedulers)
}