package com.balinasoft.fotcher.di.module.profile

import com.balinasoft.fotcher.model.interactor.infoprofileoption.InfoProfileOptionInteractor
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import com.balinasoft.fotcher.mvp.infoprofileoption.InfoProfileOptionPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class InfoProfileOptionModule {
    @Provides
    fun providerInteractor(repository: Repository, appSchedulers: AppSchedulers) = InfoProfileOptionInteractor(repository, appSchedulers)
    @Provides
    fun providesPresenter(router: Router, infoProfileOptionInteractor:InfoProfileOptionInteractor, appSchedulers: AppSchedulers) = InfoProfileOptionPresenter(router, infoProfileOptionInteractor, appSchedulers)
}