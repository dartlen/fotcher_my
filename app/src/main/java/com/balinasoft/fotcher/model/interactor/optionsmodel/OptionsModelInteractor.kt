package com.balinasoft.fotcher.model.interactor.optionsmodel

import com.balinasoft.fotcher.entity.profileoptions.pojo.ElementsItem

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class OptionsModelInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun saveOption(list: MutableList<ElementsItem>) = repository.getProfileOptions()!!
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t ->
                for (i in 0 until t.params!!.size) {
                    if (t.params[i].caption == "Параметры") {
                        t.params[i].elements!!
                                .forEachIndexed { index, elementsItem ->
                                    if (list[index].name.equals(elementsItem!!.name))
                                        t.params[i].elements!![index] = list[index]
                                }

                        break
                    }
                }
                repository.setProfileOptions(t)
                t
            }
}

