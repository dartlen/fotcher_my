package com.balinasoft.fotcher.model.data.server

import com.balinasoft.fotcher.model.repository.local.LocalData
import okhttp3.Interceptor
import okhttp3.Response

class AuthHeaderInterceptor(private val localData: LocalData) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        localData.Token.let {
            if (localData.Token != "")
                request = request.newBuilder().addHeader("Access-Token", "" + it)
                        .addHeader("accept", "*/*").build()
        }
        return chain.proceed(request)
    }
}
