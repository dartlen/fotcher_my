package com.balinasoft.fotcher.model.interactor.infophotographer

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class InfoPhotographerInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

}