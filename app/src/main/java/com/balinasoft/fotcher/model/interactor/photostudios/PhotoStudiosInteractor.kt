package com.balinasoft.fotcher.model.interactor.photostudios

import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.photostudios.ParamsItem
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

class PhotoStudiosInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart() = repository.getFilterPhotostudios()
            .flatMapSingle { t ->
                repository.searchPhotostudios(
                        PhotoStudiosRequest(page = 0,
                                params = getPramsForSearch(t)))
            }!!

    fun loadNextPage(page: Int, query: String) = repository.getFilterPhotostudios()
            .flatMapSingle { t ->
                repository.searchPhotostudios(
                        PhotoStudiosRequest(page = page,
                                params = getPramsForSearch(t), q = if (query.length > 2) query else ""))
            }!!

    fun onStartSearch(text: String) = repository.getFilterPhotostudios()
            .flatMapSingle { t ->
                repository.searchPhotostudios(
                        PhotoStudiosRequest(page = 0,
                                params = getPramsForSearch(t), q = text))
            }!!


    private fun getPramsForSearch(data: FilterPhotostudiosEntity): List<ParamsItem?>? {
        var listResult = mutableListOf<ParamsItem>()
        for (i in 0 until data.params!!.size) {
            if (i >= 4) {
                var listValues = arrayListOf<String>()
                for (k in 0 until data.params[i]!!.options!!.size) {
                    if (data.params[i]!!.options!![k]!!.check == true) {
                        listValues.add(data.params[i]!!.options!![k]!!.name!!)
                    }
                }
                if (listValues.size != 0)
                    listResult.add(ParamsItem(name = data.params[i]!!.name,
                            value = listValues))
            } else if (i < 4 && i != 1) {
                if (data.params[i]!!.borders != null)
                    if (data.params[i]!!.borders!![0] == data.params[i]!!.maxInt &&
                            data.params[i]!!.borders!![1] == data.params[i]!!.minInt)
                    else
                        listResult.add(ParamsItem(data.params[i]!!.name,
                                arrayListOf(data.params[i]!!.borders!![1], data.params[i]!!.borders!![0])))
            } else if (i == 1) {
                for (k in 0 until data.params[i]!!.options!!.size) {
                    if (data.params[i]!!.options!![k]!!.check == true)
                        listResult.add(ParamsItem(name = data.params[i]!!.name,
                                value = data.params[i]!!.options!![k]!!.name!!))
                }
            }
        }
        return listResult.toList()
    }

    fun likePhotostudio(id: Int, likeState: Boolean): Single<Response<ResponseBody>> {
        return if (likeState)
            repository.dislikePhotostudio(id)
        else
            repository.likePhotostudio(id)
    }

    fun getFlagReload() = repository.getFlagForReloadPhotostudios()
    fun setFlagReload() = repository.setFlagForReloadPhotostudios(false)

    fun getPhotostudioFilter(): Observable<FilterPhotostudiosEntity> {
        return repository.getFilterPhotostudios()
    }

}