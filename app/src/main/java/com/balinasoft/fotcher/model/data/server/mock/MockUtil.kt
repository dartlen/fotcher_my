package com.balinasoft.fotcher.model.data.server.mock

import com.balinasoft.fotcher.entity.messages.LastMessage
import com.balinasoft.fotcher.entity.messages.User
import com.balinasoft.fotcher.entity.photostudios.ContentItem

class MockUtil {

    fun getPagePhotostudio(int: Int): MutableList<ContentItem> {
        if (int % 2 == 0)
            return firstPage()
        else
            return secondPage()
    }

    fun getPageMessages(int: Int): MutableList<com.balinasoft.fotcher.entity.messages.ContentItem> {
        if (int % 2 == 0) {
            return firstPageMessages()
        } else {
            return secondpageMessages()
        }
    }

    fun getPagePhotographers(int: Int): MutableList<com.balinasoft.fotcher.entity.photographers.ContentItem> {
        if (int % 2 == 0) {
            return secondPhotographers()
        } else {
            return firstPhotographers()
        }
    }


    fun getPageModels(int: Int): MutableList<com.balinasoft.fotcher.entity.models.pojo.ContentItem> {
        if (int % 2 == 0) {
            return secondPageModels()
        } else {
            return firstPageModels()
        }
    }

    private fun firstPhotographers() = mutableListOf(
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Иванов",
                    name = "Иван"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Самойлов",
                    name = "Антон"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Поцелуёнок",
                    name = "Вероника"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Соболев",
                    name = "Никита"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Тиханович",
                    name = "Раиса"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Курносова",
                    name = "Ирина"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Шинелько",
                    name = "Александр"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Конюшевский",
                    name = "Артур"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Сушко",
                    name = "Юлия"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Минск",
                    surname = "Мазаловский",
                    name = "Николай"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Воронеж",
                    surname = "Шимановский",
                    name = "Сергей"),
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Москва",
                    surname = "Леганькова",
                    name = "Екатерина")
    )

    private fun secondPhotographers() = mutableListOf(
            com.balinasoft.fotcher.entity.photographers.ContentItem(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/onfZoq9fwHzPhkoz3UjV.jpg",
                    leftStatus = "Таджикистан",
                    surname = "Шармрович",
                    name = "Сергей")
    )


    private fun firstPageMessages() = mutableListOf(com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
            user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")))

    private fun secondpageMessages() = mutableListOf(com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
            user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Павел", surname = "Шишаев")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "Привет, как дела? Заеду за тобой после обеда, ok да?"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Николай", surname = "Иванов")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "От улыбки хмурый день светлей,\n" +
                    "От улыбки в небе радуга проснется,\n" +
                    "Поделись улыбкою своей,\n" +
                    "И она к тебе не раз еще вернется!"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Николай", surname = "Иванов")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "От улыбки хмурый день светлей,\n" +
                    "От улыбки в небе радуга проснется,\n" +
                    "Поделись улыбкою своей,\n" +
                    "И она к тебе не раз еще вернется!"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Николай", surname = "Иванов")),
            com.balinasoft.fotcher.entity.messages.ContentItem(LastMessage(readed = true, date = "2018-08-02T08:56:18.617Z", text = "От улыбки хмурый день светлей,\n" +
                    "От улыбки в небе радуга проснется,\n" +
                    "Поделись улыбкою своей,\n" +
                    "И она к тебе не раз еще вернется!"),
                    user = User(smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", name = "Николай", surname = "Иванов")))

    private fun firstPage() = mutableListOf(
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/KcOw3R2KADTq9giLA34l.jpg", id = 17, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Зимний сад", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    address = "г. Алушта Грибоедова 12 а", rating = mapOf("value" to 3.0, "count" to 10))
    )

    private fun secondPage() = mutableListOf<ContentItem>(ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
            lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
            address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1)),
            ContentItem(bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg", id = 13, favorite = false, lat = 49.786790111685235,
                    lng = 49.6923828125, name = "Студия 204 метра в квадрате", priceMax = 3000, priceMin = 2000, smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/gmfA6lriM5PzuyCpmFWu.jpg",
                    address = "г. Пенза Пенза, Ленина 1", rating = mapOf("value" to 4.0, "count" to 1))
    )

    private fun firstPageModels() = mutableListOf(
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            )

    )

    private fun secondPageModels() = mutableListOf(
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg",
                    leftStatus = "Ростов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg",
                    leftStatus = "Ростов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg",
                    leftStatus = "Ростов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg",
                    leftStatus = "Ростов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/6i08gGxiTOGquNIoF2tB.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            ),
            com.balinasoft.fotcher.entity.models.pojo.ContentItem(id = 17, name = "Гадя",
                    smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/6/rPNvDXIF8s3jqZpuDsPu.jpg",
                    leftStatus = "Борисов", surname = "Бузова"
            )

    )
}