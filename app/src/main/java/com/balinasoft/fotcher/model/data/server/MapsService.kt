package com.balinasoft.fotcher.model.data.server

import com.balinasoft.fotcher.entity.itinerary.ItineraryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MapsService {
    @GET("/maps/api/directions/json")
    fun getItinerary(
            @Query(value = "origin") position: String,
            @Query(value = "destination") destination: String): Single<ItineraryResponse>/*,
            @Query("sensor") sensor: Boolean,
            @Query("language") language: String)*/
}