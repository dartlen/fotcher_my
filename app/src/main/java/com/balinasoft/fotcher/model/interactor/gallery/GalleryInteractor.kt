package com.balinasoft.fotcher.model.interactor.gallery

import com.balinasoft.fotcher.entity.gallery.pojoimage.UploadGalleryResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class GalleryInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart(id: Int, page: Int) = repository.getPhoto(id, page)

    fun uploadImage(file: File): Single<UploadGalleryResponse?>? {
        val requestFile = RequestBody.create(MediaType.parse("jpg"), file)
        val body = MultipartBody.Part.createFormData("image", "1052405402.jpg", requestFile)
        return repository.uploadImageGallery(body)
    }
}