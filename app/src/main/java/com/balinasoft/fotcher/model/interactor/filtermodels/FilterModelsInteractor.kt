package com.balinasoft.fotcher.model.interactor.filtermodels

import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class FilterModelsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart() = repository.getFilterModels()


    fun saveFilters(data: FilterModelsEntity) {
        repository.setFilterModels(data)
    }

    fun setFlagForReloadModels() {
        repository.setFlagForReloadModels(true)
    }
}