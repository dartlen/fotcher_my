package com.balinasoft.fotcher.model.cache

import android.content.Context
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.model.system.ThreadExecutor
import io.reactivex.Observable
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Cache
/**
 * Constructor of the class [Cache].
 *
 * @param context A
 * @param serializer [Serializer] for object serialization.
 * @param fileManager [FileManager] for saving serialized objects to the file system.
 */
@Inject
internal constructor(context: Context?, private val serializer: Serializer?,
                     private val fileManager: FileManager?, private val threadExecutor: ThreadExecutor?) {

    private val context: Context
    private val cacheDir: File

    val isExpired: Boolean
        get() {
            val currentTime = System.currentTimeMillis()
            val lastUpdateTime = this.lastCacheUpdateTimeMillis

            val expired = currentTime - lastUpdateTime > EXPIRATION_TIME

            if (expired) {
                this.evictAll()
            }

            return expired
        }

    /**
     * Get in millis, the last time the cache was accessed.
     */
    private val lastCacheUpdateTimeMillis: Long
        get() = this.fileManager!!.getFromPreferences(this.context, SETTINGS_FILE_NAME,
                SETTINGS_KEY_LAST_CACHE_UPDATE)

    init {
        if (context == null || serializer == null || fileManager == null || threadExecutor == null) {
            throw IllegalArgumentException("Invalid null parameter")
        }
        this.context = context.applicationContext
        this.cacheDir = this.context.cacheDir
    }

    fun getParams(): Observable<FilterParamsEntity> {
        return Observable.create { emitter ->
            val entityFile = this@Cache.buildFile("filterParamsEntity")
            val fileContent = this@Cache.fileManager!!.readFileContent(entityFile)
            val userEntity = this@Cache.serializer!!.deserialize(fileContent, FilterParamsEntity::class.java)

            emitter.onNext(userEntity)
            emitter.onComplete()
        }
    }

    fun putParams(filterParamsEntity: FilterParamsEntity?, nameFile: String): FilterParamsEntity {
        if (filterParamsEntity != null) {
            val userEntityFile = this.buildFile(nameFile)
            val jsonString = this.serializer!!.serialize(filterParamsEntity, FilterParamsEntity::class.java)
            this.executeAsynchronously(CacheWriter(this.fileManager!!, userEntityFile, jsonString))
            setLastCacheUpdateTimeMillis()
        }
        return filterParamsEntity!!
    }

    fun getPhotostudios(): Observable<FilterPhotostudiosEntity> {
        return Observable.create { emitter ->
            val entityFile = this@Cache.buildFile("filterPhotostudiosEntity")
            val fileContent = this@Cache.fileManager!!.readFileContent(entityFile)
            val userEntity = this@Cache.serializer!!.deserialize(fileContent, FilterPhotostudiosEntity::class.java)

            emitter.onNext(userEntity)
            emitter.onComplete()
        }
    }

    fun putPhotostudios(filterPhotostudiosEntity: FilterPhotostudiosEntity?, nameFile: String): FilterPhotostudiosEntity {
        if (filterPhotostudiosEntity != null) {
            val userEntityFile = this.buildFile(nameFile)
            val jsonString = this.serializer!!.serialize(filterPhotostudiosEntity, FilterPhotostudiosEntity::class.java)
            this.executeAsynchronously(CacheWriter(this.fileManager!!, userEntityFile, jsonString))
            setLastCacheUpdateTimeMillis()
        }
        return filterPhotostudiosEntity!!
    }

    fun getModels(): Observable<FilterModelsEntity> {
        return Observable.create { emitter ->
            val entityFile = this@Cache.buildFile("filterModelsEntity")
            val fileContent = this@Cache.fileManager!!.readFileContent(entityFile)
            val userEntity = this@Cache.serializer!!.deserialize(fileContent, FilterModelsEntity::class.java)

            emitter.onNext(userEntity)
            emitter.onComplete()
        }
    }

    fun setProfileOptions(profileOptionsResponse: ProfileOptionsResponse, nameFile: String): ProfileOptionsResponse {

        val userEntityFile = this.buildFile(nameFile)
        val jsonString = this.serializer!!.serialize(profileOptionsResponse, ProfileOptionsResponse::class.java)
        this.executeAsynchronously(CacheWriter(this.fileManager!!, userEntityFile, jsonString))
        setLastCacheUpdateTimeMillis()

        return profileOptionsResponse
    }

    fun getProfileOptions(): Observable<ProfileOptionsResponse?>? {
        return Observable.create { emitter ->
            val entityFile = this@Cache.buildFile("ProfileOptions")
            val fileContent = this@Cache.fileManager!!.readFileContent(entityFile)
            val entity = this@Cache.serializer!!.deserialize(fileContent, ProfileOptionsResponse::class.java)

            emitter.onNext(entity)
            emitter.onComplete()
        }
    }

    fun putModels(filterModelsEntity: FilterModelsEntity?, nameFile: String): FilterModelsEntity {
        if (filterModelsEntity != null) {
            val userEntityFile = this.buildFile(nameFile)
            val jsonString = this.serializer!!.serialize(filterModelsEntity, FilterModelsEntity::class.java)
            this.executeAsynchronously(CacheWriter(this.fileManager!!, userEntityFile, jsonString))
            setLastCacheUpdateTimeMillis()
        }
        return filterModelsEntity!!
    }

    fun isCached(nameFile: String): Boolean {
        val EntityFile = this.buildFile(nameFile)
        return this.fileManager!!.exists(EntityFile)
    }

    fun evictAll() {
        this.executeAsynchronously(CacheEvictor(this.fileManager!!, this.cacheDir))
    }

    /**
     * Build a file, used to be inserted in the disk cache.
     *
     * @param userId The id user to build the file.
     * @return A valid file.
     */
    private fun buildFile(nameFile: String): File {
        val fileNameBuilder = StringBuilder()
        fileNameBuilder.append(this.cacheDir.path)
        fileNameBuilder.append(File.separator)
        //fileNameBuilder.append(DEFAULT_FILE_NAME)
        fileNameBuilder.append(nameFile)

        return File(fileNameBuilder.toString())
    }

    /**
     * Set in millis, the last time the cache was accessed.
     */
    private fun setLastCacheUpdateTimeMillis() {
        val currentMillis = System.currentTimeMillis()
        this.fileManager!!.writeToPreferences(this.context, SETTINGS_FILE_NAME,
                SETTINGS_KEY_LAST_CACHE_UPDATE, currentMillis)
    }

    /**
     * Executes a [Runnable] in another Thread.
     *
     * @param runnable [Runnable] to execute
     */
    private fun executeAsynchronously(runnable: Runnable) {
        this.threadExecutor!!.execute(runnable)
    }

    /**
     * [Runnable] class for writing to disk.
     */
    private class CacheWriter internal constructor(private val fileManager: FileManager,
                                                   private val fileToWrite: File,
                                                   private val fileContent: String) : Runnable {

        override fun run() {
            this.fileManager.writeToFile(fileToWrite, fileContent)
        }
    }

    /**
     * [Runnable] class for evicting all the cached files
     */
    private class CacheEvictor internal constructor(private val fileManager: FileManager,
                                                    private val cacheDir: File) : Runnable {

        override fun run() {
            this.fileManager.clearDirectory(this.cacheDir)
        }
    }

    companion object {

        private val SETTINGS_FILE_NAME = "com.fernandocejas.android10.SETTINGS"
        private val SETTINGS_KEY_LAST_CACHE_UPDATE = "last_cache_update"

        private val DEFAULT_FILE_NAME = "user_"
        private val EXPIRATION_TIME = (60 * 10 * 1000).toLong()
    }

    fun removeCache(name: String) {
        if (isCached(name)) {
            val file = this.buildFile(name)
            file.delete()
        }

    }
}

