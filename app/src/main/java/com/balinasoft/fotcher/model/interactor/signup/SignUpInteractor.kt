package com.balinasoft.fotcher.model.interactor.signup

import com.balinasoft.fotcher.entity.signup.SignUpRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class SignUpInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun signup(email: String, password: String, userType: String) = repository.signup(SignUpRequest(password, userType, email))

}