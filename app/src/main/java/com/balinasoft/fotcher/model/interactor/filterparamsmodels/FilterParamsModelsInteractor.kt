package com.balinasoft.fotcher.model.interactor.filterparamsmodels

import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class FilterParamsModelsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart() = repository.getFilterPhotographers()

    fun saveOptionsEntity(modelsEntity: ParamsEntity) = repository.getFilterModels()
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t: FilterModelsEntity? ->
                for (i in 0 until t!!.params.size) {
                    if (t.params[i].name == modelsEntity.name) {
                        t.params[i] = modelsEntity
                        break
                    }
                }
                repository.setFilterModels(t)
                t
            }


    fun saveOptionsPhotostudiosEntity(modelsEntity: ParamsEntity) = repository.getFilterModels()
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t: FilterModelsEntity? ->
                for (i in 0 until t!!.params.size) {
                    if (t.params[i].name == modelsEntity.name) {
                        t.params[i] = modelsEntity
                        break
                    }
                }
                repository.setFilterModels(t)
                t
            }


}