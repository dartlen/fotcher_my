package com.balinasoft.fotcher.model.interactor.messages

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class MessagesInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun getMessages(page: Int) = repository.getMessages(page)

    fun loadNextPage(page: Int) = repository.getMessages(page)
}