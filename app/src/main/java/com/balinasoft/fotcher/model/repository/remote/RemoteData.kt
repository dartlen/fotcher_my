package com.balinasoft.fotcher.model.repository.remote

import com.balinasoft.fotcher.BuildConfig
import com.balinasoft.fotcher.entity.comments.pojo.CommentRequest
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageRequest
import com.balinasoft.fotcher.entity.messages.MessagesResponse
import com.balinasoft.fotcher.entity.models.pojo.ModelsRequest
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse
import com.balinasoft.fotcher.entity.photographers.PhotographersRequest
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionRequest
import com.balinasoft.fotcher.entity.recovery.ChangePasswordRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryCodeRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryRequest
import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.entity.signin.SignInRequest
import com.balinasoft.fotcher.entity.signup.SignUpRequest
import com.balinasoft.fotcher.entity.user.pojo.UserResponse
import com.balinasoft.fotcher.entity.verification.VerificationRequest
import com.balinasoft.fotcher.model.data.server.FotcherService
import com.balinasoft.fotcher.model.data.server.MapsService
import com.balinasoft.fotcher.model.data.server.Utils
import com.balinasoft.fotcher.model.data.server.mock.MockAdapter
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.Retrofit

class RemoteData(private var fotcherService: FotcherService, private var mapsService: MapsService, private var retrofit: Retrofit) {


    fun signin(signInRequest: SignInRequest) = fotcherService.signin(signInRequest).map {
        if (it.isSuccessful) {
            it
        } else {
            val error = Utils.parseError(retrofit, it)
            throw error
        }
    }!!

    fun signup(signUpRequest: SignUpRequest) = fotcherService.signUp(signUpRequest).map {
        if (it.isSuccessful) {
            it
        } else {
            val error = Utils.parseError(retrofit, it)
            throw error
        }
    }!!

    fun searchPhotostudios(photoStudiosRequest: PhotoStudiosRequest): Single<PhotostudiosResponse> {
        return if (!BuildConfig.DEBUG)
            MockAdapter()
                    .swapretrofit(retrofit).searchPhotostudios(photoStudiosRequest)
        else
            fotcherService.searchPhotostudios(photoStudiosRequest)


    }

    fun verificateAccount(request: VerificationRequest) = fotcherService.verificateAccount(request)
            .map {
                if (it.isSuccessful) {
                    it
                } else {
                    val error = Utils.parseError(retrofit, it)
                    throw error
                }
            }!!

    fun recoveryAccount(recoveryRequest: RecoveryRequest) = fotcherService
            .recoveryAccount(recoveryRequest).map {
                if (it.isSuccessful) {
                    it
                } else {
                    val error = Utils.parseError(retrofit, it)
                    throw error
                }

            }!!

    fun likePhotostudio(id: Int) = fotcherService.likePhotostudio(id).map {
        if (it.isSuccessful) {
            it
        } else {
            val error = Utils.parseError(retrofit, it)
            throw error
        }
    }!!

    fun dislikePhotostudio(id: Int) = fotcherService.disLikePhotostudio(id).map {
        if (it.isSuccessful) {
            it
        } else {
            val error = Utils.parseError(retrofit, it)
            throw error
        }
    }!!

    fun getMessages(page: Int): Single<MessagesResponse> {
        return if (!BuildConfig.DEBUG)
            MockAdapter()
                    .swapretrofit(retrofit).getMessages(page)
        else
            fotcherService.getMessages(page)

    }

    fun searchPhotographers(photographersRequest: PhotographersRequest): Single<PhotographersResponse> {
        return if (!BuildConfig.DEBUG)
            MockAdapter()
                    .swapretrofit(retrofit).searchPhotographers(photographersRequest)
        else
            fotcherService.searchPhotographers(photographersRequest)
    }

    fun getFilterPhotographers() = fotcherService.getFilterPhotographers().map {
        if (it.isSuccessful) {
            it.body()
        } else {
            val error = Utils.parseError(retrofit, it)
            throw error
        }
    }!!

    fun getUser(userId: Int): Single<Response<UserResponse>> {
        return if (!BuildConfig.DEBUG)
            MockAdapter()
                    .swapretrofit(retrofit).getUser(userId)
        else
            fotcherService.getUser(userId)/*.map {
            if (it.isSuccessful) {
                    it.body()
                } else {
                    val error = Utils.parseError(retrofit, it)
                    throw error
                }
            }*/

    }

    fun getCurrentUser() =
            fotcherService.saveCurrentUser().map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun getFilterPhotostudios() =
            fotcherService.getFilterPhotostudios().map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun getPhotostudio(id: Int) = fotcherService.getPhotostudio(id).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun getComments(id: String) = fotcherService.getComments(id).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }

    fun postComment(id: String, data: CommentRequest) = fotcherService.postComment(id, data).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }

    fun searchModels(modelsRequest: ModelsRequest): Single<ModelsResponse> {
        return if (!BuildConfig.DEBUG)
            MockAdapter()
                    .swapretrofit(retrofit).searchModels(modelsRequest)
        else
            fotcherService.searchModels(modelsRequest)
    }


    fun getFilterModels() =
            fotcherService.getFilterModels().map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun getProfileOptions() =
            fotcherService.getParamsOptions().map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun uploadImage(image: MultipartBody.Part, smallX: Int, smallY: Int, smallSize: Int) = fotcherService.uploadImage(image, smallX, smallY, smallSize).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun uploadImageGallery(body: MultipartBody.Part) = fotcherService.uploadImageGallery(body)
            .map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun sendRecoveryCode(request: RecoveryCodeRequest) = fotcherService.sendRecoveryCode(request)
            .map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun changePassword(request: ChangePasswordRequest) = fotcherService.changePassword(request)
            .map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun getPhoto(id: Int, page: Int) = fotcherService.getPhoto(id, page)
            .map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun getReserveFreeTime(id: Int) = fotcherService.getReserveFreeTime(id)

    fun getDialog(id: Int, lastId: Int) = fotcherService.getDialog(id, lastId).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun sendMessage(request: SendMessageRequest) = fotcherService.sendMessage(request).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun sendMessageImage(body: MultipartBody.Part) =
            fotcherService.sendMessageImage(body).map {
                if (it.isSuccessful) {
                    it.body()
                } else {
                    throw Utils.parseError(retrofit, it)
                }
            }!!

    fun readMessage(id: Int) = fotcherService.readMessage(id)

    fun setProfile(request: ProfileOptionRequest) = fotcherService.setProfile(request)

    fun getItinerary(position: String, destination: String) = mapsService.getItinerary(position, destination)

    fun getUnreadedDialogs() = fotcherService.getUnreadedDialogs().map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun reserve(request: ReserveStudio) = fotcherService.reserveStudio(request).map {
        if (it.isSuccessful) {
            it.body()
        } else {
            throw Utils.parseError(retrofit, it)
        }
    }!!

    fun getReservations() = fotcherService.getReservations()
}