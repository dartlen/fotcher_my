package com.balinasoft.fotcher.model.interactor.map

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject


class MapInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

}