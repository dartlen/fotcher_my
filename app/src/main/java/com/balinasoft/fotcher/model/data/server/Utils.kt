package com.balinasoft.fotcher.model.data.server

import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

object Utils {

    fun parseError(retrofit: Retrofit, response: Response<*>): ServerError {
        val converter = retrofit
                .responseBodyConverter<ServerError>(ServerError::class.java, arrayOfNulls(0))

        val error: ServerError

        try {
            error = converter.convert(response.errorBody())
        } catch (e: IOException) {
            return ServerError()
        }

        return error
    }
}