package com.balinasoft.fotcher.model.interactor.models

import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.models.pojo.ModelsRequest
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse

import com.balinasoft.fotcher.entity.models.pojo.ParamsItem
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import javax.inject.Inject

class ModelsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart(): Observable<ModelsResponse>? {

        return repository.getFilterModels().flatMapSingle { t ->
            repository.searchModels(
                    ModelsRequest(page = 0,
                            params = getPramsForSearch(t),
                            Q = ""))
        }
    }

    fun onStartSearch(text: String) = repository.getFilterModels()
            .flatMapSingle { t ->
                repository.searchModels(
                        ModelsRequest(page = 0, params = getPramsForSearch(t), Q = text))
            }

    fun loadNextPage(page: Int, query: String) = repository.getFilterModels()
            .flatMapSingle { t ->
                repository.searchModels(
                        ModelsRequest(page = page, params = getPramsForSearch(t), Q = query))
            }!!

    private fun getPramsForSearch(data: FilterModelsEntity): List<ParamsItem?>? {
        var listResult = mutableListOf<ParamsItem>()
        for (i in 0 until data.params.size) {
            if (i < 4) {
                for (k in 0 until data.params[i].options!!.size) {
                    if (data.params[i].options!![k]!!.check == true)
                        listResult.add(ParamsItem(name = data.params[i].name, value = data.params[i].options!![k]!!.name!!))
                }
            } else if (i == 4) {
                for (k in 0 until data.params[i].elements.size) {
                    if (data.params[i].elements[k]!!.type == "INT_RANGE") {
                        if (data.params[i].elements[k]!!.borders != null) {
                            listResult.add(ParamsItem(name = data.params[i].elements[k]!!.name!!, value = arrayListOf(data.params[i].elements[k]!!.borders!![0], data.params[i].elements[k]!!.borders!![1])))
                        }
                    } else {
                        for (v in 0 until data.params[i].elements[k]!!.options!!.size) {
                            if (data.params[i].elements[k]!!.options!![v]!!.check == true) {
                                listResult.add(ParamsItem(name = data.params[i].elements[k]!!.name, value = data.params[i].elements[k]!!.options!![v]!!.name))
                            }
                        }
                    }
                    //if (data.params[i].elements[k]!! == true)
                    //    listResult.add(ParamsItem(name = data.params[i].name, value = data.params[i].options!![k]!!.name!!))
                }
            } else if (i > 4) {
                var listValues = arrayListOf<String>()
                for (k in 0 until data.params[i].options!!.size) {
                    if (data.params[i].options!![k]!!.check == true) {
                        listValues.add(data.params[i].options!![k]!!.name!!)
                    }
                }
                if (listValues.size != 0)
                    listResult.add(ParamsItem(name = data.params[i].name,
                            value = listValues))
            }
        }
        if (listResult.size == 0)
            listResult.add(ParamsItem(name = "", value = ""))
        return listResult.toList()
    }

    fun getFlagReload() = repository.getFlagForReloadModels()
    fun setFlagReload() = repository.setFlagForReloadModels(false)
}