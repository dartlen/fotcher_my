package com.balinasoft.fotcher.model.interactor.profilephotostudio

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

class ProfilePhotostudioInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart(id: Int) = repository.getPhotostudio(id)

    fun likePhotostudio(id: Int, likeState: Boolean): Single<Response<ResponseBody>> {
        return if (!likeState)
            repository.dislikePhotostudio(id)
        else
            repository.likePhotostudio(id)
    }
}