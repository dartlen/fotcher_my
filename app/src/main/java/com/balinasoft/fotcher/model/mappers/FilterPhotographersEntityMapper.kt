package com.balinasoft.fotcher.model.mappers

import com.balinasoft.fotcher.commons.Mapper
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotographers.OptionsEntity
import com.balinasoft.fotcher.entity.filterphotographers.ParamsEntity
import com.balinasoft.fotcher.entity.filterphotographers.pojo.FilterPhotographersResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FilterPhotographersEntityMapper @Inject constructor() : Mapper<FilterPhotographersResponse,
        FilterParamsEntity>() {

    override fun mapFrom(from: FilterPhotographersResponse): FilterParamsEntity {

        var filterParamsEntity: FilterParamsEntity =
                FilterParamsEntity(params = mutableListOf())

        for (i in 0 until from.params!!.size) {
            var listOption = mutableListOf<OptionsEntity?>()
            for (k in 0 until from.params[i]!!.options!!.size)
                listOption.add(OptionsEntity(name = from.params[i]!!.options!![k]!!.name,
                        caption = from.params[i]!!.options!![k]!!.caption, check = false))

            filterParamsEntity.params!!.add(ParamsEntity(name = from.params[i]!!.name,
                    caption = from.params[i]!!.caption, options = listOption))
        }
        return filterParamsEntity
    }
}