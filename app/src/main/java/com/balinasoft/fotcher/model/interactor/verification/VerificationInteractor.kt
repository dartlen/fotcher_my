package com.balinasoft.fotcher.model.interactor.verification

import com.balinasoft.fotcher.entity.verification.VerificationRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class VerificationInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun verificateAccount(code: String, email: String) = repository
            .verificateAccount(VerificationRequest(code, email))

    fun saveToken(token: String?) = repository.saveToken(token)
    fun setUserName(name: String) = repository.setUserName(name)
    fun saveUserId(id: Int) = repository.saveId(id)
    fun getCurrentUser() = repository.getCurrentUser()
}