package com.balinasoft.fotcher.model.interactor.filteroptionsmodels

import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import javax.inject.Inject

class FilterOptionsModelsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun saveFilters(data: ParamsEntity): Observable<FilterModelsEntity?>? {
        return repository.getFilterModels()
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.ui())
                .map { t: FilterModelsEntity? ->
                    for (i in 0 until t!!.params.size) {
                        if (t.params[i].name == data.name) {
                            t.params[i] = data
                            break
                        }
                    }
                    repository.setFilterModels(t)
                    t
                }
    }
}