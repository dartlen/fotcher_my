package com.balinasoft.fotcher.model.system

import java.util.concurrent.Executor

interface ThreadExecutor : Executor
