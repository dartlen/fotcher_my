package com.balinasoft.fotcher.model.interactor.profilephotographer

import com.balinasoft.fotcher.entity.messages.User
import com.balinasoft.fotcher.entity.user.UserEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import javax.inject.Inject

class ProfilePhotographerInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart(userId: Int): Single<UserEntity>? {
        //repository.saveId(userId)
        return repository.getUser(userId)
    }

    fun getIdPhotographer() = repository.getId()

    fun getUser(id: Int) = repository.getUser(id).map { t -> User(leftStatus = t.leftStatus, surname = t.surname, smallAvatar = t.smallAvatar, name = t.name, id = t.id, type = t.type) }
}