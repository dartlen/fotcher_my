package com.balinasoft.fotcher.model.interactor.filterparams

import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotographers.ParamsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FilterParamsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    var compositeDisposable = CompositeDisposable()

    fun onStart() = repository.getFilterPhotographers()

    fun saveOptionsEntity(paramsEntity: ParamsEntity) = repository.getFilterParams()
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t: FilterParamsEntity? ->
                for (i in 0 until t!!.params!!.size) {
                    if (t.params!![i]!!.name == paramsEntity.name) {
                        t.params[i] = paramsEntity
                        break
                    }
                }
                repository.setFilterParams(t)
                t
            }


    fun saveOptionsPhotostudiosEntity(photostudiosEntity: com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity) = repository.getFilterPhotostudios()
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t: FilterPhotostudiosEntity? ->
                for (i in 0 until t!!.params!!.size) {
                    if (t.params!![i]!!.name == photostudiosEntity.name) {
                        t.params[i] = photostudiosEntity
                        break
                    }
                }
                repository.setFilterPhotostudios(t)
                t
            }!!

    fun onDestroy() {
        compositeDisposable.clear()
    }
}