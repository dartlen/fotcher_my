package com.balinasoft.fotcher.model.data.server

class ServerError(val statusCode: Int = 0, override val message: String? = null) : RuntimeException()