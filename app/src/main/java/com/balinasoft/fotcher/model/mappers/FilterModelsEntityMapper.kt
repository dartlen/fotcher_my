package com.balinasoft.fotcher.model.mappers

import com.balinasoft.fotcher.commons.Mapper
import com.balinasoft.fotcher.entity.filtermodels.ElementsEntity
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filtermodels.OptionsEntity
import com.balinasoft.fotcher.entity.filtermodels.ParamsEntity
import com.balinasoft.fotcher.entity.filtermodels.pojo.FilterModelsResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FilterModelsEntityMapper @Inject constructor() : Mapper<FilterModelsResponse,
        FilterModelsEntity>() {

    override fun mapFrom(from: FilterModelsResponse): FilterModelsEntity {

        var filterModelsEntity: FilterModelsEntity =
                FilterModelsEntity(params = mutableListOf())

        for (i in 0 until from.params!!.size) {
            var listOption = mutableListOf<OptionsEntity?>()
            if (i == 4) {
                var list = mutableListOf<ElementsEntity?>()
                for (k in 0 until from.params[i]!!.elements!!.size) {
                    if (k > 5) {
                        var options = mutableListOf<OptionsEntity?>()
                        for (j in 0 until from.params[i]!!.elements!![k]!!.options!!.size) {
                            options.add(OptionsEntity(
                                    name = from.params[i]!!.elements!![k]!!.options!![j]!!.name,
                                    caption = from.params[i]!!.elements!![k]!!.options!![j]!!.caption,
                                    check = false
                            ))
                        }
                        list.add(ElementsEntity(
                                name = from.params[i]!!.elements!![k]!!.name,
                                caption = from.params[i]!!.elements!![k]!!.caption,
                                type = from.params[i]!!.elements!![k]!!.type,
                                value = from.params[i]!!.elements!![k]!!.value as? String,
                                required = from.params[i]!!.elements!![k]!!.required,
                                minInt = from.params[i]!!.elements!![k]!!.minInt,
                                maxInt = from.params[i]!!.elements!![k]!!.maxInt,
                                options = options,
                                borders = null
                        ))
                    } else {
                        list.add(ElementsEntity(
                                name = from.params[i]!!.elements!![k]!!.name,
                                caption = from.params[i]!!.elements!![k]!!.caption,
                                type = from.params[i]!!.elements!![k]!!.type,
                                value = from.params[i]!!.elements!![k]!!.value as? String,
                                required = from.params[i]!!.elements!![k]!!.required,
                                minInt = from.params[i]!!.elements!![k]!!.minInt,
                                maxInt = from.params[i]!!.elements!![k]!!.maxInt,
                                borders = null
                        ))
                    }

                }
                filterModelsEntity.params.add(ParamsEntity(name = from.params[i]!!.name,
                        caption = from.params[i]!!.caption, elements = list))

            } else {
                for (k in 0 until from.params[i]!!.options!!.size)
                    listOption.add(OptionsEntity(name = from.params[i]!!.options!![k]!!.name,
                            caption = from.params[i]!!.options!![k]!!.caption, check = false))

                filterModelsEntity.params.add(ParamsEntity(name = from.params[i]!!.name,
                        caption = from.params[i]!!.caption, options = listOption, elements = mutableListOf()))
            }
        }
        return filterModelsEntity
    }
}