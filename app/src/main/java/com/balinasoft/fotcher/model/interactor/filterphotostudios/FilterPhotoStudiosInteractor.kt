package com.balinasoft.fotcher.model.interactor.filterphotostudios

import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class FilterPhotoStudiosInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart() = repository.getFilterPhotostudios()
    fun saveFilters(filterPhotostudiosEntity: FilterPhotostudiosEntity) = repository.setFilterPhotostudios(filterPhotostudiosEntity)
    fun setFlagForReloadPhotostudios() {}
}