package com.balinasoft.fotcher.model.mappers

import com.balinasoft.fotcher.commons.Mapper
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.filterphotostudios.OptionsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.ParamsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.pojo.FilterPhotostudiosResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FilterPhotostudiosEntityMapper @Inject constructor() : Mapper<FilterPhotostudiosResponse,
        FilterPhotostudiosEntity>() {
    override fun mapFrom(from: FilterPhotostudiosResponse): FilterPhotostudiosEntity {

        var filterParamsEntity: FilterPhotostudiosEntity =
                FilterPhotostudiosEntity(params = mutableListOf())

        for (i in 0 until from.params!!.size) {
            if (i > 3 || i == 1) {
                val listOption = mutableListOf<OptionsEntity?>()
                for (k in 0 until from.params[i]!!.options!!.size)
                    listOption.add(OptionsEntity(name = from.params[i]!!.options!![k]!!.name,
                            caption = from.params[i]!!.options!![k]!!.caption))
                filterParamsEntity.params!!.add(ParamsEntity(name = from.params[i]!!.name,
                        caption = from.params[i]!!.caption, options = listOption))
            } else {
                filterParamsEntity.params!!.add(ParamsEntity(
                        name = from.params[i]!!.name,
                        caption = from.params[i]!!.caption,
                        options = null,
                        maxInt = from.params[i]!!.maxInt,
                        minInt = from.params[i]!!.minInt,
                        type = from.params[i]!!.type))
            }
        }
        return filterParamsEntity
    }
}