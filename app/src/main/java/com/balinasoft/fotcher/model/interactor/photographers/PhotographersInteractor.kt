package com.balinasoft.fotcher.model.interactor.photographers

import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.photographers.ParamsItem
import com.balinasoft.fotcher.entity.photographers.PhotographersRequest
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import javax.inject.Inject

class PhotographersInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart(): Observable<PhotographersResponse>? {

        return repository.getFilterPhotographers().flatMapSingle { t ->
            repository.searchPhotographers(
                    PhotographersRequest(page = 0,
                            params = getPramsForSearch(t),
                            Q = ""))
        }
    }

    fun onStartSearch(text: String) = repository.getFilterPhotographers()
            .flatMapSingle { t ->
                repository.searchPhotographers(
                        PhotographersRequest(page = 0, params = getPramsForSearch(t), Q = text))
            }

    fun loadNextPage(page: Int, query: String) = repository.getFilterPhotographers()
            .flatMapSingle { t ->
                repository.searchPhotographers(
                        PhotographersRequest(page = page, params = getPramsForSearch(t), Q = query))
            }!!

    private fun getPramsForSearch(data: FilterParamsEntity): List<ParamsItem?>? {
        var listResult = mutableListOf<ParamsItem>()
        for (i in 0 until data.params!!.size) {
            if (i < 3) {
                for (k in 0 until data.params[i]!!.options!!.size) {
                    if (data.params[i]!!.options!![k]!!.check == true)
                        listResult.add(ParamsItem(name = data.params[i]!!.name, value = data.params[i]!!.options!![k]!!.name!!))
                }
            } else {
                var listValues = arrayListOf<String>()
                for (k in 0 until data.params[i]!!.options!!.size) {
                    if (data.params[i]!!.options!![k]!!.check == true) {
                        listValues.add(data.params[i]!!.options!![k]!!.name!!)
                    }
                }
                if (listValues.size != 0)
                    listResult.add(ParamsItem(name = data.params[i]!!.name,
                            value = listValues))
            }
        }
        if (listResult.size == 0)
            listResult.add(ParamsItem(name = "", value = ""))
        return listResult.toList()
    }

    fun getFlagReload() = repository.getFlagForReloadPhotographers()
    fun setFlagReload() = repository.setFlagForReloadPhotographers(false)
}