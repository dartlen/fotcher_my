package com.balinasoft.fotcher.model.data.server.mock

import com.balinasoft.fotcher.entity.UnreadDialogsResponse
import com.balinasoft.fotcher.entity.comments.pojo.CommentRequest
import com.balinasoft.fotcher.entity.comments.pojo.CommentSendResponse
import com.balinasoft.fotcher.entity.comments.pojo.CommentsResponse
import com.balinasoft.fotcher.entity.currentuser.pojo.CurrentUserResponse
import com.balinasoft.fotcher.entity.dialog.pojo.AttachmentResponse
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageRequest
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse
import com.balinasoft.fotcher.entity.filtermodels.pojo.FilterModelsResponse
import com.balinasoft.fotcher.entity.filterphotographers.pojo.FilterPhotographersResponse
import com.balinasoft.fotcher.entity.filterphotostudios.pojo.FilterPhotostudiosResponse
import com.balinasoft.fotcher.entity.gallery.pojo.GalleryResponse
import com.balinasoft.fotcher.entity.gallery.pojoimage.UploadGalleryResponse
import com.balinasoft.fotcher.entity.messages.MessagesResponse
import com.balinasoft.fotcher.entity.models.pojo.ModelsRequest
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse
import com.balinasoft.fotcher.entity.photographers.PhotographersRequest
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.entity.photostudio.pojo.PhotostudioResponse
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionRequest
import com.balinasoft.fotcher.entity.profileoptions.pojo.upload.UploadResponse
import com.balinasoft.fotcher.entity.recovery.ChangePasswordRequest
import com.balinasoft.fotcher.entity.recovery.ChangePasswordResponse
import com.balinasoft.fotcher.entity.recovery.RecoveryCodeRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryRequest
import com.balinasoft.fotcher.entity.reservations.ReservationsResponse
import com.balinasoft.fotcher.entity.reserve.pojo.FreeTimeResponse
import com.balinasoft.fotcher.entity.reserve.pojo.request.ReserveResponse
import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.entity.signin.SignInRequest
import com.balinasoft.fotcher.entity.signin.SignInResponse
import com.balinasoft.fotcher.entity.signup.SignUpRequest
import com.balinasoft.fotcher.entity.signup.SignUpResponse
import com.balinasoft.fotcher.entity.user.pojo.LastPhotosItem
import com.balinasoft.fotcher.entity.user.pojo.ParamsItem
import com.balinasoft.fotcher.entity.user.pojo.UserResponse
import com.balinasoft.fotcher.entity.user.pojo.ValueItem
import com.balinasoft.fotcher.entity.verification.VerificationRequest
import com.balinasoft.fotcher.entity.verification.VerificationResponse
import com.balinasoft.fotcher.model.data.server.FotcherService
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls

class MockService(private val delegate: BehaviorDelegate<FotcherService>) : FotcherService {

    override fun searchPhotostudios(data: PhotoStudiosRequest): Single<PhotostudiosResponse> {
        val dataResponse: PhotostudiosResponse = PhotostudiosResponse(content = MockUtil()
                .getPagePhotostudio(data.page!!), totalPages = 10, page = data.page, pageSize = 20)
        return delegate.returning<Any>(Calls.response<Any>(dataResponse)).searchPhotostudios(data)
    }


    override fun signin(data: SignInRequest): Single<Response<SignInResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun signUp(data: SignUpRequest): Single<Response<SignUpResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun verificateAccount(data: VerificationRequest): Single<Response<VerificationResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun recoveryAccount(data: RecoveryRequest): Single<Response<ResponseBody>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun likePhotostudio(id: Int): Single<Response<ResponseBody>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun disLikePhotostudio(id: Int): Single<Response<ResponseBody>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun getMessages(page: Int): Single<MessagesResponse> {
        val dataResponse: MessagesResponse = MessagesResponse(totalPages = 2, content = MockUtil()
                .getPageMessages(page))
        return delegate.returning<Any>(Calls.response<Any>(dataResponse)).getMessages(page)
    }

    override fun searchPhotographers(data: PhotographersRequest): Single<PhotographersResponse> {
        val dataResponse: PhotographersResponse = PhotographersResponse(content = MockUtil()
                .getPagePhotographers(data.page!!), totalPages = 3)
        return delegate.returning<Any>(Calls.response<Any>(dataResponse)).searchPhotographers(data)
    }

    override fun getFilterPhotographers(): Single<Response<FilterPhotographersResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUser(userId: Int): Single<Response<UserResponse>> {
        val dataResponse: UserResponse = UserResponse(
                leftStatus = "Минск",
                surname = "Шишаев",
                name = "Павел",
                smallAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/7/j3WPSyoQrBjAqLfbsRPQ.jpg",
                type = "PHOTOMODEL",
                bigAvatar = "http://fotcher-temp.balinasoft.com/images/avatar/2018/7/Az5jWGrQy0CdqoPes3Gj.jpg",
                phone = null,
                params = arrayListOf(ParamsItem(name = "", caption = "Рост", value = mutableListOf(ValueItem(name = "", caption = "191")), type = ""),
                        ParamsItem(name = "", caption = "вес", value = mutableListOf(ValueItem(name = "", caption = "87")), type = ""),
                        ParamsItem(name = "", caption = "Объём", value = mutableListOf(ValueItem(name = "", caption = "110-80-100")), type = "")),
                infoColumns = null,
                rightStatus = "Нет опыта",
                lastPhotos = arrayListOf(LastPhotosItem(id = 13, small = "http://fotcher-temp.balinasoft.com/images/photo/2018/7/G8SMneIED1odoSf8NdWS.jpg",
                        big = "http://fotcher-temp.balinasoft.com/images/photo/2018/7/FHYIlmHS3XqH7MR2fqxk.jpg"),
                        LastPhotosItem(id = 12, small = "http://fotcher-temp.balinasoft.com/images/photo/2018/7/g3TgOqqeFU26Gr1Wp1rq.jpg",
                                big = "http://fotcher-temp.balinasoft.com/images/photo/2018/7/3bLQOz5MAXVKoBMgSSva.jpg"))
        )

        return delegate.returning<Any>(Calls.response<Any>(dataResponse)).getUser(userId)
    }

    override fun saveCurrentUser(): Single<Response<CurrentUserResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFilterPhotostudios(): Single<Response<FilterPhotostudiosResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPhotostudio(id: Int): Single<Response<PhotostudioResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getComments(id: String): Single<Response<CommentsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun postComment(id: String, data: CommentRequest): Single<Response<CommentSendResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun searchModels(data: ModelsRequest): Single<ModelsResponse> {
        val dataResponse: ModelsResponse = ModelsResponse(content = MockUtil()
                .getPageModels(data.page!!), totalPages = 3)
        return delegate.returning<Any>(Calls.response<Any>(dataResponse)).searchModels(data)
    }

    override fun getFilterModels(): Single<Response<FilterModelsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getParamsOptions(): Single<Response<ProfileOptionsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun uploadImage(image: MultipartBody.Part, sizeX: Int, sizeY: Int, smallSize: Int): Single<Response<UploadResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changePassword(changePasswordRequest: ChangePasswordRequest): Single<Response<ChangePasswordResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendRecoveryCode(request: RecoveryCodeRequest): Single<Response<ResponseBody>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPhoto(id: Int, page: Int): Single<Response<GalleryResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun uploadImageGallery(image: MultipartBody.Part): Single<Response<UploadGalleryResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getReserveFreeTime(id: Int): Single<Response<FreeTimeResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDialog(id: Int, lastId: Int): Single<Response<List<DialogResponse>>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendMessage(request: SendMessageRequest): Single<Response<SendMessageResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendMessageImage(body: MultipartBody.Part): Single<Response<AttachmentResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun readMessage(id: Int): Single<Response<ResponseBody>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setProfile(request: ProfileOptionRequest): Single<Response<com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUnreadedDialogs(): Single<Response<UnreadDialogsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reserveStudio(request: ReserveStudio): Single<Response<ReserveResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getReservations(): Single<Response<ReservationsResponse>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
