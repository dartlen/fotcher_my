package com.balinasoft.fotcher.model.interactor.signin

import com.balinasoft.fotcher.entity.signin.SignInRequest
import com.balinasoft.fotcher.entity.signin.SignInResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class SignInInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun signin(email: String, password: String): Single<Response<SignInResponse>> {
        return repository.signin(SignInRequest(password, email))
    }

    fun saveToken(token: String?) = repository.saveToken(token)

    fun getCurrentUser() = repository.getCurrentUser()

    fun saveUserId(id: Int) = repository.saveId(id)
    fun saveType(type: String) = repository.saveType(type)
    fun saveImage(url: String) = repository.setImage(url)
    fun setUserName(name: String) = repository.setUserName(name)
}


