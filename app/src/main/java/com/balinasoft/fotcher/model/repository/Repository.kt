package com.balinasoft.fotcher.model.repository

import com.balinasoft.fotcher.entity.comments.pojo.CommentRequest
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageRequest
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.models.pojo.ModelsRequest
import com.balinasoft.fotcher.entity.photographers.PhotographersRequest
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionRequest
import com.balinasoft.fotcher.entity.recovery.ChangePasswordRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryCodeRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryRequest
import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.entity.signin.SignInRequest
import com.balinasoft.fotcher.entity.signup.SignUpRequest
import com.balinasoft.fotcher.entity.verification.VerificationRequest
import com.balinasoft.fotcher.model.mappers.FilterModelsEntityMapper
import com.balinasoft.fotcher.model.mappers.FilterPhotographersEntityMapper
import com.balinasoft.fotcher.model.mappers.FilterPhotostudiosEntityMapper
import com.balinasoft.fotcher.model.mappers.ProfilePhotographersEntityMapper
import com.balinasoft.fotcher.model.repository.local.LocalData
import com.balinasoft.fotcher.model.repository.remote.RemoteData
import io.reactivex.Observable
import okhttp3.MultipartBody

class Repository(private var local: LocalData, private var remote: RemoteData,
                 private var mapperFilterPhotographers: FilterPhotographersEntityMapper,
                 private val userEntityMapper: ProfilePhotographersEntityMapper,
                 private val mapperFilterPhotostudios: FilterPhotostudiosEntityMapper,
                 private val mapperFilterModels: FilterModelsEntityMapper) {

    fun signin(signInRequest: SignInRequest) = remote.signin(signInRequest)

    fun saveToken(token: String?) = local.saveToken(token)

    fun signup(signUp: SignUpRequest) = remote.signup(signUp)

    fun searchPhotostudios(photoStudiosRequest: PhotoStudiosRequest) = remote.searchPhotostudios(photoStudiosRequest)

    fun verificateAccount(request: VerificationRequest) = remote.verificateAccount(request)

    fun recoveryAccount(recoveryRequest: RecoveryRequest) = remote.recoveryAccount(recoveryRequest)

    fun likePhotostudio(id: Int) = remote.likePhotostudio(id)

    fun dislikePhotostudio(id: Int) = remote.dislikePhotostudio(id)

    fun getMessages(page: Int) = remote.getMessages(page)

    fun searchPhotographers(photographersRequest: PhotographersRequest) = remote.searchPhotographers(photographersRequest)

    fun getFilterPhotographers(): Observable<FilterParamsEntity> {
        return if (local.checkFilterParams()) {
            local.getFilterParamsEntity()
        } else {
            remote.getFilterPhotographers().map { t -> mapperFilterPhotographers.mapFrom(t) }.map { t -> setFilterParams(t) }.toObservable()
        }
    }

    fun setFilterParams(filterParamsEntity: FilterParamsEntity) = local.setFilterParamsEntity(filterParamsEntity)

    fun getFilterParams() = local.getFilterParamsEntity()

    fun getFlagForReloadPhotographers() = local.FlagPhotographers


    fun setFlagForReloadPhotographers(boolean: Boolean) {
        local.FlagPhotographers = boolean
    }

    fun getUser(userId: Int) = remote.getUser(userId).map { t -> userEntityMapper.mapFrom(t.body()!!) }!!

    fun saveId(userId: Int) = local.saveId(userId)

    fun getId() = local.id

    fun saveType(type: String) {
        local.type = type
    }

    fun getType() = local.type

    fun getCurrentUser() = remote.getCurrentUser()

    fun getFilterPhotostudios(): Observable<FilterPhotostudiosEntity> {
        return if (local.checkFilterPhotostudios()) {
            local.getFilterPhotostudiosEntity()
        } else {
            remote.getFilterPhotostudios().map { t -> mapperFilterPhotostudios.mapFrom(t) }.map { t -> setFilterPhotostudios(t) }.toObservable()
        }
    }

    fun setFilterPhotostudios(filterPhotostudiosEntity: FilterPhotostudiosEntity) = local.setFilterPhotostudiosEntity(filterPhotostudiosEntity)

    fun getFlagForReloadPhotostudios() = local.FlagPhotostudios


    fun setFlagForReloadPhotostudios(boolean: Boolean) {
        local.FlagPhotostudios = boolean
    }

    fun getPhotostudio(id: Int) = remote.getPhotostudio(id)

    fun getComments(id: String) = remote.getComments(id)

    fun postComment(id: String, data: CommentRequest) = remote.postComment(id, data)

    fun getFlagForReloadModels() = local.FlagModels

    fun setFlagForReloadModels(boolean: Boolean) {
        local.FlagModels = boolean
    }

    fun searchModels(modelsRequest: ModelsRequest) = remote.searchModels(modelsRequest)

    fun getFilterModels(): Observable<FilterModelsEntity> {
        return if (local.checkFilterModels()) {
            local.getFilterModelsEntity()
        } else {
            remote.getFilterModels().map { t -> mapperFilterModels.mapFrom(t) }.map { t -> setFilterModels(t) }.toObservable()
        }
    }

    fun setFilterModels(filterModelsEntity: FilterModelsEntity) = local.setFilterModelsEntity(filterModelsEntity)

    fun getProfileOptions():
            Observable<ProfileOptionsResponse?>? {
        return if (local.checkProfileOptions()) {
            local.getProfileOptions()
        } else {
            remote.getProfileOptions().map { t -> setProfileOptions(t) }.toObservable()
        }
    }

    fun setProfileOptions(profileOptionsResponse: ProfileOptionsResponse) = local.setProfileOptions(profileOptionsResponse)

    fun getImage() = local.image

    fun setImage(url: String) {
        local.image = url
    }

    fun uploadImage(image: MultipartBody.Part, smallX: Int, smallY: Int, smallSize: Int) = remote.uploadImage(image, smallX, smallY, smallSize)

    fun clearProfileOptionsCache() {
        local.clearProfileOptionsCache()
    }

    fun sendRecoveryCode(request: RecoveryCodeRequest) = remote.sendRecoveryCode(request)

    fun changePassword(request: ChangePasswordRequest) = remote.changePassword(request)

    fun getPhoto(id: Int, page: Int) = remote.getPhoto(id, page)

    fun uploadImageGallery(body: MultipartBody.Part) = remote.uploadImageGallery(body)

    fun getReserveFreeTime(id: Int) = remote.getReserveFreeTime(id)

    fun getDialog(id: Int, lastId: Int) = remote.getDialog(id, lastId)

    fun sendMessage(request: SendMessageRequest) = remote.sendMessage(request)

    fun sendMessageImage(body: MultipartBody.Part) = remote.sendMessageImage(body)

    fun readMessage(id: Int) = remote.readMessage(id)

    fun setProfile(request: ProfileOptionRequest) = remote.setProfile(request)

    fun getUserName() = local.userName
    fun setUserName(name: String) {
        local.userName = name
    }

    fun getItinerary(position: String, destination: String) = remote.getItinerary(position, destination)

    fun getUnreadedDialogs() = remote.getUnreadedDialogs()

    fun reserve(request: ReserveStudio) = remote.reserve(request)

    fun getReservations() = remote.getReservations()
}