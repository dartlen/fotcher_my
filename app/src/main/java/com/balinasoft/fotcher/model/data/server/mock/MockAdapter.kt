package com.balinasoft.fotcher.model.data.server.mock

import com.balinasoft.fotcher.model.data.server.FotcherService
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

class MockAdapter {
    private var mockRetrofit: MockRetrofit? = null


    fun swapretrofit(ret: Retrofit): FotcherService {
        val behavior = NetworkBehavior.create()

        mockRetrofit = MockRetrofit.Builder(ret)
                .networkBehavior(behavior)
                .build()

        val delegate = mockRetrofit!!.create(FotcherService::class.java)
        val mockUserService = MockService(delegate)

        return mockUserService
    }
}