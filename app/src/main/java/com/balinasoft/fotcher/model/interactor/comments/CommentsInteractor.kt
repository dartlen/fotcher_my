package com.balinasoft.fotcher.model.interactor.comments

import com.balinasoft.fotcher.entity.comments.pojo.CommentRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class CommentsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart(id: String) = repository.getComments(id)

    fun postComment(id: String, stars: Float, comment: String) = repository.postComment(id, CommentRequest(stars, comment))
}