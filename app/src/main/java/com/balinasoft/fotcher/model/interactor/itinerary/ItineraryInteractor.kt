package com.balinasoft.fotcher.model.interactor.itinerary

import android.location.Location
import com.balinasoft.fotcher.entity.itinerary.ItineraryResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import javax.inject.Inject

class ItineraryInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun getItinerary(getItinerary: Location?, photostudioLocation: String): Single<ItineraryResponse> {
        return repository.getItinerary(getItinerary!!.latitude.toString() + " " + getItinerary.longitude.toString(), photostudioLocation)
    }

}