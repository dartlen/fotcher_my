package com.balinasoft.fotcher.model.interactor.profileoptions

import com.balinasoft.fotcher.entity.profileoptions.pojo.OptionsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionRequest
import com.balinasoft.fotcher.entity.profileoptions.pojo.upload.UploadResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class ProfileOptionsInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart() = repository.getProfileOptions()

    fun getAvatar() = repository.getImage()

    fun uploadImage(file: File): Single<UploadResponse?>? {
        val requestFile = RequestBody.create(MediaType.parse("jpg"), file)
        val body = MultipartBody.Part.createFormData("image", "1052405402.jpg", requestFile)
        return repository.uploadImage(body, 200, 200, 300)
    }

    fun setAvatar(url: String) {
        repository.setImage(url)
    }

    fun saveOptions(params: MutableList<ParamsItem>) {
        repository.setProfileOptions(ProfileOptionsResponse(params = params))
    }

    fun onBack(paramsEntity: MutableList<ParamsItem>): Single<Response<com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionsResponse>> {
        var tmp = mutableListOf<com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem>()

        paramsEntity.forEach { t: ParamsItem? ->
            when {
                t!!.type == "SELECT" -> t.options!!.forEach {
                    if (it!!.check!!)
                        tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(name = t.name, value = it.name))
                }
                t.type == "STRING" -> tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(name = t.name, value = t.value as String?))
                t.type == "MULTI_SELECT" -> {
                    var array = arrayListOf<String>()
                    t.options!!.forEach {
                        if (it!!.check!!)
                            array.add(it.name!!)
                    }
                    tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(name = t.name, value = array))
                }
                t.type == "INT_RANGE" -> tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(name = t.name, value = (t.border as ArrayList)[0]))
                t.type == "GROUP" -> t.elements!!.forEach {
                    if (it!!.type == "INT_RANGE") {
                        if (it.border != null)
                            tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(
                                    name = it.name,
                                    value = (it.border as ArrayList)[0]))
                    } else if (it.type == "SELECT") {
                        it.options!!.forEach { value: OptionsItem? ->
                            if (value!!.check!!) {
                                tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(
                                        name = it.name,
                                        value = value.name))
                            }
                        }
                    } else if (it.type == "DATE") {
                        tmp.add(com.balinasoft.fotcher.entity.profileoptions.pojo.post.ParamsItem(
                                name = it.name,
                                value = it.value))
                    }
                }
            }
        }

        repository.getProfileOptions()
        repository.clearProfileOptionsCache()
        return repository.setProfile(ProfileOptionRequest(params = tmp))

    }

    fun setUserName(name: String) = repository.setUserName(name)


}




