package com.balinasoft.fotcher.model.interactor.recovery

import com.balinasoft.fotcher.entity.recovery.ChangePasswordRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryCodeRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class RecoveryInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun recoveryAccount(email: String) = repository.recoveryAccount(RecoveryRequest(email))

    fun sendRecoveryCode(code: String, email: String) =
            repository.sendRecoveryCode(RecoveryCodeRequest(email = email, code = code))

    fun changePassword(email: String, code: String, password: String) =
            repository.changePassword(ChangePasswordRequest(code = code, email = email, password = password))

    fun setToken(token: String) {
        repository.saveToken(token)
    }
}