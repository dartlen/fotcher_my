package com.balinasoft.fotcher.model.mappers

import com.balinasoft.fotcher.commons.Mapper
import com.balinasoft.fotcher.entity.user.*
import com.balinasoft.fotcher.entity.user.pojo.UserResponse
import com.google.gson.internal.LinkedTreeMap
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProfilePhotographersEntityMapper @Inject constructor() : Mapper<UserResponse,
        UserEntity>() {

    override fun mapFrom(from: UserResponse): UserEntity {
        val listParams = mutableListOf<ParamsEntity?>()
        val elements = mutableListOf<Element>()

        from.params!!.forEachIndexed { _, paramsItem ->
            when {
                paramsItem!!.type == "GROUP" -> {
                    loop@ paramsItem.elements!!.forEach lit@{
                        elements.add(Element(
                                name = (it as LinkedTreeMap<*, *>)["name"] as String,
                                caption = (it)["caption"] as String,
                                value = if ((it)["value"]!! is LinkedTreeMap<*, *>) (((it)["value"]!! as LinkedTreeMap<*, *>)["name"] as String).toInt() else ((it)["value"]!! as Double).toInt(),
                                type = (it)["type"] as String))
                    }
                    listParams.add(ParamsEntity(name = paramsItem.name,
                            type = paramsItem.type,
                            caption = paramsItem.caption,
                            elements = elements))
                }
                paramsItem.type == "SELECT" -> {
                    val listValues = mutableListOf<ValueEntity?>()
                    listValues.add(ValueEntity(name = ((paramsItem.value!! as LinkedTreeMap<*, *>)["name"] as String?),
                            caption = (paramsItem.value as LinkedTreeMap<*, *>)["caption"] as String?))
                    listParams.add(ParamsEntity(name = paramsItem.name,
                            type = paramsItem.type,
                            caption = paramsItem.caption,
                            value = listValues))
                }
                paramsItem.type == "MULTI_SELECT" -> {
                    val listValues = mutableListOf<ValueEntity?>()
                    if (paramsItem.value!! is LinkedTreeMap<*, *>)
                        listValues.add(ValueEntity(name = ((paramsItem.value as LinkedTreeMap<*, *>)["name"] as String?),
                                caption = paramsItem.value["caption"] as String?))
                    else {
                        for (k in 0 until (((paramsItem.value as ArrayList<*>)).size))
                            listValues.add(ValueEntity(name = ((paramsItem.value)[k] as LinkedTreeMap<*, *>)["name"] as String?,
                                    caption = (paramsItem.value[k] as LinkedTreeMap<*, *>)["caption"] as String?))
                    }
                    listParams.add(ParamsEntity(name = paramsItem.name,
                            type = paramsItem.type,
                            caption = paramsItem.caption,
                            value = listValues))
                }
            }

        }

        val listLastPhotos = mutableListOf<LastPhotosEntity>()
        for (i in 0 until from.lastPhotos!!.size)
            listLastPhotos.add(LastPhotosEntity(small = from.lastPhotos[i]!!.small,
                    big = from.lastPhotos[i]!!.big,
                    id = from.lastPhotos[i]!!.id))

        return UserEntity(params = listParams,
                phone = from.phone,
                bigAvatar = from.bigAvatar,
                surname = from.surname,
                name = from.name,
                rightStatus = from.rightStatus,
                leftStatus = from.leftStatus,
                lastPhotos = listLastPhotos,
                infoColumns = from.infoColumns,
                id = from.id,
                type = from.type,
                smallAvatar = from.smallAvatar)
    }
}

