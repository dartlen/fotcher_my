package com.balinasoft.fotcher.model.interactor.activity

import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class MainActivityInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun checkToken() = repository.getCurrentUser()

    fun saveUserId(id: Int) = repository.saveId(id)
    fun getId() = repository.getId()
    fun saveType(type: String) = repository.saveType(type)
    fun getType() = repository.getType()
    fun saveImage(url: String) = repository.setImage(url)
    fun getAvatar() = repository.getImage()
    fun getUserName() = repository.getUserName()
    fun setUserName(name: String) = repository.setUserName(name)
    fun getUnreadedDialogs() = repository.getUnreadedDialogs()
}