package com.balinasoft.fotcher.model.interactor.filterphotographers

import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.model.mappers.FilterPhotographersEntityMapper
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import javax.inject.Inject


class FilterPhotographersInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers,
        private val mapper: FilterPhotographersEntityMapper
) {

    fun onStart(): Observable<FilterParamsEntity> {

        return repository.getFilterPhotographers()
    }

    fun saveFilters(data: FilterParamsEntity) {
        repository.setFilterParams(data)
    }

    fun setFlagForReloadPhotographers() {
        repository.setFlagForReloadPhotographers(true)
    }
}

