package com.balinasoft.fotcher.model.interactor.infoprofileoption

import com.balinasoft.fotcher.entity.profileoptions.pojo.ParamsItem
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class InfoProfileOptionInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun saveOption(params: ParamsItem) = repository.getProfileOptions()!!
            .subscribeOn(appSchedulers.io())
            .observeOn(appSchedulers.io())
            .map { t ->
                for (i in 0 until t.params!!.size) {
                    if (t.params[i].name == params.name) {
                        t.params[i] = params
                        //t.params[i].value= LinkedTreeMap<String,String>()
                        break
                    }
                }
                repository.setProfileOptions(t)
                t
            }
}


