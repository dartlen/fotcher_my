package com.balinasoft.fotcher.model.interactor.favorites

import com.balinasoft.fotcher.entity.photostudios.ParamsItem
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

class FavoritesInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {

    fun onStart() = repository.getFilterPhotostudios()
            .flatMapSingle { _ ->
                repository.searchPhotostudios(
                        PhotoStudiosRequest(page = 0,
                                params = mutableListOf<ParamsItem>(), favorite = true))
            }!!

    fun loadNextPage(page: Int, query: String) = repository.getFilterPhotostudios()
            .flatMapSingle { _ ->
                repository.searchPhotostudios(
                        PhotoStudiosRequest(page = page, favorite = true,
                                params = mutableListOf<ParamsItem>(), q = if (query.length > 2) query else ""))
            }!!

    fun likePhotostudio(id: Int, likeState: Boolean): Single<Response<ResponseBody>> {
        return if (likeState)
            repository.dislikePhotostudio(id)
        else
            repository.likePhotostudio(id)
    }

    fun getFlagReload() = repository.getFlagForReloadPhotostudios()
    fun setFlagReload() = repository.setFlagForReloadPhotostudios(false)

}