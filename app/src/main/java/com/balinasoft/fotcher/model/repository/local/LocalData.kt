package com.balinasoft.fotcher.model.repository.local

import android.content.SharedPreferences
import com.balinasoft.fotcher.entity.filtermodels.FilterModelsEntity
import com.balinasoft.fotcher.entity.filterphotographers.FilterParamsEntity
import com.balinasoft.fotcher.entity.filterphotostudios.FilterPhotostudiosEntity
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.model.cache.Cache
import io.reactivex.Maybe
import io.reactivex.Observable

class LocalData(private var sharedPreferences: SharedPreferences, private var cache: Cache) {
    private val DEVICE_TOKEN = "data.source.prefs.DEVICE_TOKEN"
    private val PHOTOGRAPHERS_KEY = "data.source.prefs.PHOTOGRAPHERS_KEY"
    private val PHOTOGRAPHER_ID = "data.source.prefs.PHOTOGRAPHER_ID"
    private val PHOTOSTUDIOS_KEY = "data.source.prefs.PHOTOGRAPHERS_KEY"
    private val MODELS_KEY = "data.source.prefs.MODELS_KEY"
    private val USER_TYPE = "data.source.prefs.USER_TYPE"
    private val IMAGE_URL = "data.source.prefs.IMAGE_URL"
    private val USER_NAME = "data.source.prefs.USERNAME"

    fun saveToken(token: String?): Maybe<Boolean> {
        Token = token
        return Maybe.just(true)
    }

    var Token: String?
        get() = sharedPreferences.getString(DEVICE_TOKEN, "")
        set(value) {
            sharedPreferences.edit().putString(DEVICE_TOKEN, value).apply()
        }

    fun getFilterParamsEntity(): Observable<FilterParamsEntity> = cache.getParams()

    fun setFilterParamsEntity(filterParamsEntity: FilterParamsEntity) =
            cache.putParams(filterParamsEntity, "filterParamsEntity")

    fun setProfileOptions(profileOptionsResponse: ProfileOptionsResponse) =
            cache.setProfileOptions(profileOptionsResponse, "ProfileOptions")

    fun checkFilterParams(): Boolean {
        return cache.isCached("filterParamsEntity")
    }


    fun getFilterPhotostudiosEntity(): Observable<FilterPhotostudiosEntity> = cache.getPhotostudios()

    fun setFilterPhotostudiosEntity(filterPhotostudiosEntity: FilterPhotostudiosEntity) =
            cache.putPhotostudios(filterPhotostudiosEntity, "filterPhotostudiosEntity")

    fun checkFilterPhotostudios(): Boolean {
        return cache.isCached("filterPhotostudiosEntity")
    }

    fun getFilterModelsEntity(): Observable<FilterModelsEntity> = cache.getModels()

    fun getProfileOptions() = cache.getProfileOptions()

    fun setFilterModelsEntity(filterModelsEntity: FilterModelsEntity) =
            cache.putModels(filterModelsEntity, "filterModelsEntity")

    fun checkFilterModels(): Boolean {
        return cache.isCached("filterModelsEntity")
    }

    fun checkProfileOptions(): Boolean {
        return cache.isCached("ProfileOptions")
    }

    var FlagPhotographers: Boolean
        get() = sharedPreferences.getBoolean(PHOTOGRAPHERS_KEY, true)
        set(value) {
            sharedPreferences.edit().putBoolean(PHOTOGRAPHERS_KEY, value).apply()
        }

    var FlagPhotostudios: Boolean
        get() = sharedPreferences.getBoolean(PHOTOSTUDIOS_KEY, true)
        set(value) {
            sharedPreferences.edit().putBoolean(PHOTOSTUDIOS_KEY, value).apply()
        }

    fun saveId(userId: Int) {
        id = userId
    }

    var id: Int
        get() = sharedPreferences.getInt(PHOTOGRAPHER_ID, 0)
        set(value) {
            sharedPreferences.edit().putInt(PHOTOGRAPHER_ID, value).apply()
        }

    var FlagModels: Boolean
        get() = sharedPreferences.getBoolean(MODELS_KEY, true)
        set(value) {
            sharedPreferences.edit().putBoolean(MODELS_KEY, value).apply()
        }

    var type: String
        get() = sharedPreferences.getString(USER_TYPE, "")
        set(value) {
            sharedPreferences.edit().putString(USER_TYPE, value).apply()
        }

    var image: String
        get() = sharedPreferences.getString(IMAGE_URL, "")
        set(value) {
            sharedPreferences.edit().putString(IMAGE_URL, value).apply()
        }

    fun clearProfileOptionsCache() {
        cache.removeCache("ProfileOptions")
    }

    var userName: String
        get() = sharedPreferences.getString(USER_NAME, "")
        set(value) {
            sharedPreferences.edit().putString(USER_NAME, value).apply()
        }
}