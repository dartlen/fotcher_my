package com.balinasoft.fotcher.model.data.server

import com.balinasoft.fotcher.entity.UnreadDialogsResponse
import com.balinasoft.fotcher.entity.comments.pojo.CommentRequest
import com.balinasoft.fotcher.entity.comments.pojo.CommentSendResponse
import com.balinasoft.fotcher.entity.comments.pojo.CommentsResponse
import com.balinasoft.fotcher.entity.currentuser.pojo.CurrentUserResponse
import com.balinasoft.fotcher.entity.dialog.pojo.AttachmentResponse
import com.balinasoft.fotcher.entity.dialog.pojo.DialogResponse
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageRequest
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse
import com.balinasoft.fotcher.entity.filtermodels.pojo.FilterModelsResponse
import com.balinasoft.fotcher.entity.filterphotographers.pojo.FilterPhotographersResponse
import com.balinasoft.fotcher.entity.filterphotostudios.pojo.FilterPhotostudiosResponse
import com.balinasoft.fotcher.entity.gallery.pojo.GalleryResponse
import com.balinasoft.fotcher.entity.gallery.pojoimage.UploadGalleryResponse
import com.balinasoft.fotcher.entity.messages.MessagesResponse
import com.balinasoft.fotcher.entity.models.pojo.ModelsRequest
import com.balinasoft.fotcher.entity.models.pojo.ModelsResponse
import com.balinasoft.fotcher.entity.photographers.PhotographersRequest
import com.balinasoft.fotcher.entity.photographers.PhotographersResponse
import com.balinasoft.fotcher.entity.photostudio.pojo.PhotostudioResponse
import com.balinasoft.fotcher.entity.photostudios.PhotoStudiosRequest
import com.balinasoft.fotcher.entity.photostudios.PhotostudiosResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.ProfileOptionsResponse
import com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionRequest
import com.balinasoft.fotcher.entity.profileoptions.pojo.upload.UploadResponse
import com.balinasoft.fotcher.entity.recovery.ChangePasswordRequest
import com.balinasoft.fotcher.entity.recovery.ChangePasswordResponse
import com.balinasoft.fotcher.entity.recovery.RecoveryCodeRequest
import com.balinasoft.fotcher.entity.recovery.RecoveryRequest
import com.balinasoft.fotcher.entity.reservations.ReservationsResponse
import com.balinasoft.fotcher.entity.reserve.pojo.FreeTimeResponse
import com.balinasoft.fotcher.entity.reserve.pojo.request.ReserveResponse
import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.entity.signin.SignInRequest
import com.balinasoft.fotcher.entity.signin.SignInResponse
import com.balinasoft.fotcher.entity.signup.SignUpRequest
import com.balinasoft.fotcher.entity.signup.SignUpResponse
import com.balinasoft.fotcher.entity.user.pojo.UserResponse
import com.balinasoft.fotcher.entity.verification.VerificationRequest
import com.balinasoft.fotcher.entity.verification.VerificationResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface FotcherService {
    //Account
    @Headers("Content-Type: application/json")
    @POST("/api/v1/account/signin")
    fun signin(@Body data: SignInRequest): Single<Response<SignInResponse>>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/account/signup")
    fun signUp(@Body data: SignUpRequest): Single<Response<SignUpResponse>>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/photostudio/search")
    fun searchPhotostudios(@Body data: PhotoStudiosRequest): Single<PhotostudiosResponse>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/account/signup/confirm")
    fun verificateAccount(@Body data: VerificationRequest): Single<Response<VerificationResponse>>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/account/forgot/send")
    fun recoveryAccount(@Body data: RecoveryRequest): Single<Response<ResponseBody>>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/photostudio/{photostudioId}/favorite")
    fun likePhotostudio(@Path("photostudioId") id: Int): Single<Response<ResponseBody>>

    @Headers("Content-Type: application/json")
    @DELETE("/api/v1/photostudio/{photostudioId}/favorite")
    fun disLikePhotostudio(@Path("photostudioId") id: Int): Single<Response<ResponseBody>>

    @GET("/api/v1/chat/dialog")
    fun getMessages(@Query("page") page: Int): Single<MessagesResponse>

    @Headers("Content-Type: application/json")
    @POST("/api/v1/photographer/search")
    fun searchPhotographers(@Body data: PhotographersRequest): Single<PhotographersResponse>

    @GET("/api/v1/photographer/search")
    fun getFilterPhotographers(): Single<Response<FilterPhotographersResponse>>

    @GET("/api/v1/user/{userId}")
    fun getUser(@Path("userId") userId: Int): Single<Response<UserResponse>>

    @GET("/api/v1/user/current")
    fun saveCurrentUser(): Single<Response<CurrentUserResponse>>

    @GET("/api/v1/photostudio/search")
    fun getFilterPhotostudios(): Single<Response<FilterPhotostudiosResponse>>

    @GET("/api/v1/photostudio/{photostudioId}")
    fun getPhotostudio(@Path("photostudioId") id: Int): Single<Response<PhotostudioResponse>>

    @GET("/api/v1/photostudio/{photostudioId}/review")
    fun getComments(@Path("photostudioId") id: String): Single<Response<CommentsResponse>>

    @POST("/api/v1/photostudio/{photostudioId}/review")
    fun postComment(@Path("photostudioId") id: String, @Body data: CommentRequest): Single<Response<CommentSendResponse>>

    @POST("/api/v1/photomodel/search")
    fun searchModels(@Body data: ModelsRequest): Single<ModelsResponse>

    @GET("/api/v1/photomodel/search")
    fun getFilterModels(): Single<Response<FilterModelsResponse>>

    @GET("/api/v1/user/param")
    fun getParamsOptions(): Single<Response<ProfileOptionsResponse>>

    @Multipart
    @Headers("Accept:*/*")
    @POST("/api/v1/user/avatar")
    fun uploadImage(@Part image: MultipartBody.Part,
                    @Query("smallX") sizeX: Int,
                    @Query("smallY") sizeY: Int,
                    @Query("smallSize") smallSize: Int): Single<Response<UploadResponse>>

    @POST("/api/v1/account/forgot/check")
    fun sendRecoveryCode(@Body request: RecoveryCodeRequest): Single<Response<ResponseBody>>

    @POST("/api/v1/account/forgot/restore")
    fun changePassword(@Body changePasswordRequest: ChangePasswordRequest): Single<Response<ChangePasswordResponse>>

    @GET("/api/v1/photo/user/{userId}")
    fun getPhoto(@Path("userId") id: Int, @Query("page") page: Int): Single<Response<GalleryResponse>>

    @Multipart
    @Headers("Accept:*/*")
    @POST("/api/v1/photo")
    fun uploadImageGallery(@Part image: MultipartBody.Part): Single<Response<UploadGalleryResponse>>

    @GET("/api/v1/reservation/free-time")
    fun getReserveFreeTime(@Query("photostudioId") id: Int): Single<Response<FreeTimeResponse>>

    @GET("/api/v1/chat/message")
    fun getDialog(@Query("userId") id: Int, @Query("lastId") lastId: Int): Single<Response<List<DialogResponse>>>

    @POST("/api/v1/chat/message")
    fun sendMessage(@Body request: SendMessageRequest): Single<Response<SendMessageResponse>>

    @Multipart
    @Headers("accept:*/*")
    @POST("/api/v1/chat/attachment?type=IMAGE")
    fun sendMessageImage(@Part image: MultipartBody.Part): Single<Response<AttachmentResponse>>

    @POST("/api/v1/chat/message/{messageId}/read")
    fun readMessage(@Path("messageId") id: Int): Single<Response<ResponseBody>>

    @POST("/api/v1/user/param")
    fun setProfile(@Body request: ProfileOptionRequest): Single<Response<com.balinasoft.fotcher.entity.profileoptions.pojo.post.ProfileOptionsResponse>>

    @GET("/api/v1/chat/unread-count")
    fun getUnreadedDialogs(): Single<Response<UnreadDialogsResponse>>

    @POST("/api/v1/reservation/reserve")
    fun reserveStudio(@Body request: ReserveStudio): Single<Response<ReserveResponse>>

    @GET("/api/v1/reservation")
    fun getReservations(): Single<Response<ReservationsResponse>>
}