package com.balinasoft.fotcher.model.interactor.dialog

import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageRequest
import com.balinasoft.fotcher.entity.dialog.pojo.SendMessageResponse
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class DialogInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun getDialog(id: Int) = repository.getDialog(id, 0).flatMapObservable { t -> Observable.fromIterable(t) }.map { t ->
        if (!t.readed!! && !t.my!!) {
            repository.readMessage(t.id!!).subscribe()
        }
        t
    }.toList()

    fun sendMessage(message: String, id: Int) =
            repository.sendMessage(SendMessageRequest(text = message, toId = id))

    fun sendImage(file: File, id: Int): Single<SendMessageResponse?>? {
        val requestFile = RequestBody.create(MediaType.parse("jpg"), file)
        val body = MultipartBody.Part.createFormData("file", "1052405402.jpg", requestFile)
        return repository.sendMessageImage(body).flatMap { t -> repository.sendMessage(SendMessageRequest(toId = id, attachmentIds = arrayListOf(t.id), text = "")) }
    }

    fun loadNextPage(id: Int, lastMessage: Int) = repository.getDialog(id, lastMessage)
            .flatMapObservable { t -> Observable.fromIterable(t) }.map { t ->
                if (!t.readed!! && !t.my!!) {
                    repository.readMessage(t.id!!).subscribe()

                }
                t
            }.toList()
}