package com.balinasoft.fotcher.model.interactor.reserve

import com.balinasoft.fotcher.entity.reserve.pojo.response.ReserveStudio
import com.balinasoft.fotcher.model.repository.Repository
import com.balinasoft.fotcher.model.system.AppSchedulers
import javax.inject.Inject

class ReserveInteractor @Inject constructor(
        private val repository: Repository,
        private val appSchedulers: AppSchedulers
) {
    fun onStart(id: Int) = repository.getReserveFreeTime(id)
    fun onReserve(request: ReserveStudio) = repository.reserve(request)
}